#include "pch.h"
#include <iostream>

#define IS(EXPECTED, GOT) \
if (! ( (EXPECTED) == (GOT) )) \
{ \
	std::cerr << '\n' << __FUNCTION__ << " Expected '" << (EXPECTED) << "' Got '" << (GOT) << "'"; \
	*success = false; \
	return; \
}

#define IS_NOT(TO_AVOID, GOT) \
if (( (TO_AVOID) == (GOT) )) \
{ \
	std::cerr << "\n" << __FUNCTION__ << " Do not want '" << (TO_AVOID) << "'"; \
	*success = false; \
	return; \
}

// include .tst file(s)
$ for test in TEST_INCLUDES
#include "$test$"
$ endfor

int main(void)
{
	std::cout << "Test Suite " << UNIT_NAME;

	bool success;
	u32 numFails = 0;

$ for test in TESTS
	{
		success = true;
		$test$(&success);
		if (!success)
			numFails++;
	}
$ endfor

	if (numFails == 0)
		std::cout << "... No failures";
	else
		std::cerr << "\n\t" << numFails << " failure" << (numFails > 1 ? "s" : "");

	std::cout << std::endl;
	return numFails;
}
