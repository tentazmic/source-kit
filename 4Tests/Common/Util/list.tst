#include "Util/list.hpp"

$ test list access
{
	arr::sttc_list<u32, 5> a;
	a.push(1);
	a.push(2);
	a.push(3);

	IS(a[1], 2)
	IS_NOT(a[2], 0)
}
