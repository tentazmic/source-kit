# Road Map

## To keep in mind

Write tests whenever encountering a bug
Document code more

## Tasks

##### New Project Structure

Rather than having new projects just continue from the head of engine, they should include
it in their project folder (e.g. as a submodule) and use it like that.  
We have a new Python script `if.py` that acts as a front end for the various Python
scripts in 0Tools. Rather than `python install-framework/0Tools/base.py ...` we do
`python if.py base ...`.  
The platform main files should be templated and generated for the game repo.  
The Base and Installs folders should be in the game repo.

Enable the project to have multiple Base (or Launcher) folders.  
9Editor in the engine repo contains a bunch of editor logic, then we can have an
equivalent folder in the game repo for actually implementing the editor.  
A use of these base folders would be a base for the actual game, then a second one for the
headless server.
Perhaps the editor should also be one.

*Unfinished Thought: These platform files should also be promoted to be better
paired with the files that implement the game functions (launcher files).*  
No, the point of the launcher files is that would be able to be used for any OS platform,
you would just need to make that OS' platform file.

Build tool:

- print the names of dirty files
- some sort of build config file, so we can type `python if.py config --chime-never` it will be assumed for all future builds
- flag to print all commands executed on the command line
- print success or failure + reason at the end of compilation

---

Move the namespace app implementation into a shared ipp file.

Perform the asset path resolution done by the `AssetPath` macro using the ppprocessor.  
Ths will make it easier to switch between using the file system and asset bundles.  
Going to need to define an asset type, that takes a const char* in filesystem builds and
then some number ID in asset bundle builds.

Replace the OGL_CHECK stuff with OpenGL debug functions: `glDebugMessageCallback` and
`glDebugMessageControl`.

Formal colour type:

- the ability to interface with both floating values and integers
- creation of C++ literals so we can define a colour like `"FFFFFFFF"_rgba

##### Refactor Data Structures

Move the data structures into a new folder called `Data/` and a `dst` namespace.  
Rename the prefixes to be more concise:

- sttc_ to c (compile time)
- dync_ to d (dynamic)

Create a new type f (fixed) which are like clists but not modifiable.  
For the dynamic structures, enable them to use allocator policies for their memory:

- ` d..`: Uses malloc and free
- `ad..`: Uses an arena
- `sd..`: Uses alloca

An example policy:
```
__Alloc_Stack:
    void * alloc(size_t s) => return alloca(s)
    void free(void *ptr) => void
```

Enable these lists to use initialiser lists. For the dynamic structures, have them also
take a capacity/additional field so that we can pad the capacity if needed.  
Enable usage of slices.  
Make the `get()` function more descriptive and create an operator overload for it (^?).

Port over the bitset from Spansion.

Add a reserve function for dynamic structures.  
`_list_funcs.ipp:203` When we have a dynamic structure that is uninitialised, COUNT would
be null data so we would get a seg fault at this line.

Port over the testing stuff from Spansion.

Pool array, free list.

##### Memory Model

Equip the engine to use a new memory model using predeclared memory buffers.  
The platform allocates a large chunk of memory and splits them up into two major
categories: persistent memory and per-frame memory.  
The arena behind per-frame memory would just be a linear allocator, that is reset at the
end of each frame.  
Persistent memory would be used for things like level data, stuff that lives for longer
than a frame.  
No code outside of the platform should use malloc or free and ideally the platform should
use the respective platform's syscall functions for memory allocation.

Move the generations stuff into `Data/` and rename the type to `tracker`, equip the
structure with a bitset to tell which slots are still active and a function to deactivate
an active slot. Give them IDs and when the head loops around send a message.

##### Renderer Overhaul

We don't have dedicated, large renderers.

We instead have several types of render commands, some absolutely necessary ones:

- sqr::cmd_quad
- sqr::cmd_circle
- sqr::cmd_line
- sqr::cmd_text
- cbe::cmd_static_mesh
- cbe::cmd_skinned_mesh
- cbe::cmd_text
- cbe::cmd_billboard

- rdr::cmd_set_framebuffer
- rdr::cmd_clear_colour
- rdr::cmd_resize_screen
- rdr::cmd_write_uniform_buffer_object

They should be some mechanism to parse the shader source to know the vertex attributes
the shader expects rather than defining it ourselves in code.

These render commands can only have one material/shader. They should take a pointer to
a data buffer or a mesh, so we can easily use the command to draw both one element and
do batched renderering.

We have a RenderQueue struct that takes these render commands.  
It is essentially a glorified queue structure. We can call `Draw` and it will execute the
commands in the order they were added.  
If necessary, we'll equip the RenderQueue with the functionality of sorting the commands,
e.g. transparent objects then opaque ones.

Place the 2D stuff in the namespace `sqr` and 3D stuff in `cbe`.
Use `rdr` for stuff that's non specific to either.

---

Move all the formatters stuff to their relevant files.  
Add delete functions to graphics libraries.

Make the released state for inputs the default, ie make its backing value of 0.

Enable the mesh buffer creator to create a mesh buffer without an index buffer.  

Remove the custom math::vec* structs and just use the glm ones.

Copy over the testing stuff from Spansion  

Proper audio engine fundamentals:

- play looping audio
- play single clips
- proper mixing 

Rather than having the game run in a standalone window then having a floating ImGUI window
it may be beneficial to have a mini editor. We have a panel to the right that can display
details about objects or the scene, on the bottom we have a mini console and in the
top left is the game view, which we force into different aspect ratios.

Create an ImGui backend that makes use of the renderer.


##### Order

## Engine Tasks

Fix style according to `STYLE.md`.

Add in full 3D to the renderer + lights  
Asset Pipeline:
- Blender exporter for 3D models, as blender addon and command line script, export everything in an 'Export' collection
- Texture importer

Give the installs the ability to upload their own verticies to the renderers  

3D skinned meshes  
Shader system:
- Custom, C like language for shaders that is transpiled into the correct language
- allows for you to import/include other shaders so you can use their standalone functions and shader (main) functions
- allows you to write various types of shader in one file

Game launcher window  

Deployment system, both command line and in editor:
- compiling all the assets into asset packs
- creating a release mode asset manager, that uses a virtual file system to read assets from the packs, the filepath strings are hashed/replaced with an integer index (use constexpr functions)
- removing any human readable string from the executables and asset packs (strings command)
- executable icon
- move all the files to a completely separate directory and it runs as expected

## Tool Tasks

Figure out PCH
- no needless compilation (mainly the pch)
- The PCH only works when pch.h.gch is in the _Includes directory, want it to work from 00PreCompiled
- pch.h needs to be compiled with the same compiler options as the cpp files.

### Maybes

Progressive success chime to be played at each success stage of a multi 
stage compilation, e.g. compiling Base for Installs  
Move the tools to its own repo, add the tools to the path, so that the same
tools aren't present in every project, perhaps do this when the engine is much
further along so that it can be clear which tools are generic

## Completed

~~Create editor project~~  
~~Rename OpenGL files and classes to have the ogl prefix~~  
~~Remove workbench~~  
~~Replace with standalone build scripts~~  
~~multiprocessed compilation~~  
~~Remove config files, keep name field in project.conf~~  
~~Have the settings be defaults in the scripts, create a build profile class~~
~~Remove the concurrent flag and replace with an option for number of threads~~  
~~Port over code generation stuff~~  
~~Rework macros: for project name, for the Core and Editor roots~~  
~~Create more loggers of differing detail~~  
~~Remove singletons~~  
~~Make a template of the new wing beater files~~  
~~New folder /AllSource~~  
- ~~noticed that Core was getting a lot of stuff that we don't want the wings to be involved with~~
- ~~new AllSource will contain "engine" code, then Core will contain stuff~~
~~specific to Core e.g. main.cpp~~

~~Merge into master~~  
~~Switch to linux to test compatability~~  
~~Seperate the data of the vertex array and buffer from functionality,~~
~~so that the modules don't need to compile glad~~  
~~Do the above for Shaders and textures~~

~~Use namespaces in different sections of the code + rename mixins~~  
~~Move to favour simple structs over classes~~  
~~typedefed replacements for unsigned int, int, etc (i32, u32)~~  

~~Framebuffers + Scene Rendering in the editor~~  
~~Events: make it so you bind a callback_sink to a sink_invoker, rather than get a sink from~~
~~an event. Then we can create different sink invokers, e.g. one that reacts to application~~
~~events being marked as handled.~~  
~~Create a new invoker for application events~~  

~~Implement array list~~
~~ECS; reimplement everything under Components/ using new ECS~~  

~~Blueprint Source Files:~~  
~~Found that the template stuff used for the wings will be needed elsewhere.~~
~~`1Templates` should have much more generic stuff: Wing project, Component + System files.~~  
~~The wing templates should be moved to use blueprint files, which use the extensions end~~
~~in the target file extension followed by `.b`.~~  
~~These will use a more sofisticated meta language than templates and be located within~~
~~the source directories. When compiling they will be resolved, saved to a location in~~
~~00Precompiled and included in the build.~~

~~Change the ECS components implementation to be runtime generic, so that wings can add their own~~
~~components without the component being visible in core.~~

~~Scenes~~  
~~Asset Pipeline: Actual Asset system (not just macros), dev/debug version i.e. loading from files not a bundle~~

~~Integrate the blueprints with the wings build system.~~  
~~New scenes from a wing~~  
~~Redo the wings so that they no longer compile anything in Framework/Platform/~~  

~~Change it so the viewport panel makes use of a layer. This way we can have a panel order through~~
~~which the events go through, i.e. the focused one first.~~  

~~General ImGUI render of component data, so that we don't have to define them for each component~~  

~~Inspector System~~
- ~~view for the scene - done~~
- ~~view for an entity - done~~
- ~~multiple, ordered inspector panels - done~~

~~Redo lists~~  

~~Update blueprints so that error messages print out the correct line from the blueprint file~~  

~~Editor Filesystem View~~  
~~Convert file explorer functionality to a pure function in UI folder so both the~~
~~file explorer and inspector panel can use it~~  
~~Opening scenes by selecting them in the file explorer, inspecting files from the file explorer~~
~~in the inspector~~

~~Play audio~~  
~~Load and play a song from disk~~  

~~Platform Restructure~~  


~~Look in `_Includes/core`, remove the need for the STRINGIZE_VALUE_OF stuff, look~~
~~at INSTALL_EXT in UNIVERSAL for example~~  
~~Move externals build system to use the build system provided by the package,~~
~~(it'll be better for them to maintain it rather than me)~~  

~~Renaming Core => Base, Wing => Install~~  

~~Redo reflection system:~~
- ~~take a list of files and build a tree of who includes who, serialise it~~
- ~~if a file's path changes, mark it as dirty~~
- ~~if a file changes, mark as dirty~~
- ~~if a file's parent is dirty, mark it as dirty~~

- ~~get the ecs component information~~

~~Redo blueprints:~~
- ~~every file goes through the system~~

~~Build:~~
- ~~Only recompile non-dirty files~~

~~PPProcess:~~
- ~~enable GDB to recognise the original file names (should still load the ppprocessed file)~~
- ~~note the line num from the original in the ppprocessed file~~

~~Currently thinking of prefixing the line with /* line num */, but that presents a problem~~
~~when the original file has a multi line comment. The closing comment blocks ends the~~
~~original, proper multiline block~~  

*Fixed by transforming all multiline comment blocks by wrapping each line between /* and */*

~~Tackle TODOs~~  
~~Create test framework~~  

~~Engine loop has frame times that vary from 2.5ms to 8ms on a scene with only 4 textured~~
~~quads, it shouldn't be above 1.5ms even though it's single threaded~~
- ~~get profiles of frames of different lengths~~
- ~~figure out what's causing the long duration and the variance, then quash them~~  

*This only occurs due to VSync, setting it off uncaps the frame rate, yield FPS > 4K*

~~Move the audio (and maybe windowing) to separate threads, fixes:~~
- ~~crash after window resize~~
- ~~crash after window move~~  

*only Windows audio*

~~GLFW Gamepad Input~~  
~~Fonts~~  
