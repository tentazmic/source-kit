#!/usr/bin/python
import sys
import config
import subprocess

# TODO Respect -Verbose switch, Respect -Timer switch

__USAGE = f"""\
{sys.argv[0]} <SUBCOMMAND> [OPTIONS]

===== SUBCOMMANDs =====

base        | b  <string[]>         Compile the provided base projects
installs    | i  <string[]>         Compile the provided installs
externals   | x  <string[]>         Compile the provided externals
test        | t  <glob>             Run the matched tests
template    | p  <string> <string>  Create a file/folder structure from a template

config      | c  <OPTIONS>          Save options to be assumed in future runs

=====   OPTIONS   =====

-Help | -h | ?   <string?>          Prints this help dialogue or a help dialogue about
                                    a given subcommand
                                    Use with a subcommand to print its help dialogue

/Processes | /p  <integer>          The max number of processes to use
                                        {config.SETTINGS['n-processes']}
-Timer     | -t                     Time the command
!Timer     | !t                     Turn off the timer
                                        {config.SETTINGS['timer']}
-Verbose   | -v                     Prints more information about whats going on
!Verbose   | !v
                                        {config.SETTINGS['verbose']}
/Chime     | /m  <always|a , failure|f , never|n>
                                    What situations the build chimes in
                                        {config.SETTINGS['chime']}

--- base ---
-Run       | -r                     Run the result of the build after
!Run       | !r
                                        {config.SETTINGS['run']}

--- base, installs, externals ---
/Clean     | /c  <none|n, shallow|s , deep|d>
                                    How we should use previously built objects
                                    - none   : Use them all
                                    - shallow: Rebuild the objects
                                    - deep   : Delete the relevant 00Source folder then build
                                        {config.SETTINGS['clean']}\
"""

__USAGE_TEMPLATE = f"""\
python {sys.argv[0]} template <TEMPLATE> <DESTINATION> [CONTEXT PAIRS]\

--- CONTEXT PAIRS ---
Either enter a pair like so 'foo=bar' or as two arguments 'foo bar'\
"""

__NAME = '$name$'
__INSTALL_EXTENSION = '$install_extension$'

def print_usage(subcommand = ''):
	match subcommand:

		case 'template' | 'p':
			print(__USAGE_TEMPLATE)
		case _:
			print(__USAGE)

def read_arguments():
	args = sys.argv[1:]
	if len(args) == 0:
		print(__USAGE)
		exit(1)

	subcom_name = args.pop(0)
	subcom = [ subcom_name ]
	match subcom_name:
		case 'base' | 'b' | 'installs' | 'i' | 'externals' | 'x':
			while len(args) > 0:
				if args[0].startswith('-'):
					break

				subcom.append(args.pop(0))
		case 'test' | 't':
			if len(args) != 0:
				subcom.append(args.pop(0))
		case 'template' | 'p':
			if len(args) < 2:
				print("ERROR template subcommand requires two inputs")
				exit(1)

			subcom.append(args.pop(0))
			subcom.append(args.pop(0))
		case 'config' | 'c':
			pass
		case '-Help' | '-h' | '?':
			if len(args) > 0:
				print_usage(args[0])
			else:
				print_usage()
			exit(0)
		case _:
			print(f"ERROR Unknown subcommand: {subcom}")
			exit(1)

	settings = config.SETTINGS.copy()
	if not config.read_and_update_settings(args, settings):
		exit(1)

	return subcom, settings



if __name__ == '__main__':
	subcommand, settings = read_arguments()

	match subcommand[0]:
		case 'base' | 'b':
			pass
		case 'installs' | 'i':
			pass
		case 'externals' | 'x':
			pass
		case 'test' | 't':
			pass
		case 'template' | 'p':
			args = sys.argv[3:]
			cmd = [ 'python', '$tools_folder$/templates.py' ]
			cmd.extend(subcommand[1:])
			cmd.extend(sys.argv[4:])
			subprocess.run(cmd)

		case 'config' | 'c':
			if len(sys.argv) < 3:
				print("ERROR Pass in at least one parameter to the config subcommand")
				exit(1)

			cmd = [ 'python', '$tools_folder$/templates.py', 'config.py', 'config.py' ]
			for k, v in settings.items():
				cmd.append(f'{k}={v}')
			subprocess.run(cmd)
