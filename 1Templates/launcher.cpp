#include "pch.h"

#include "app/application.hpp"

// We could take a launcher project and compile it into an install
// and have it be used by an 
extern "C" {

void GameStartup(const app::startup_config &config, snd::buffer buffer)
{

}

void GameShutdown()
{

}

void GameUpdate(tik::frame_stamp &fs)
{

}

void GameRender()
{

}

void GameStreamAudio(snd::buffer buf)
{

}

void GameFireEvents()
{

}

void GamePlatformEvent(evt::event e)
{

}

};

