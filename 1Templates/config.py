##########################################################
########## DEFAULTS

NUM_PROCESSES = $n-processes$
USE_TIMER     = $timer$
IS_VERBOSE    = $verbose$
CHIME_KIND    = '$chime$'
RUN_BUILD     = $run$
CLEAN_KIND    = '$clean$'


SETTINGS = {
	'n-processes': NUM_PROCESSES,
	'timer':       USE_TIMER,
	'verbose':     IS_VERBOSE,
	'chime':       CHIME_KIND,
	'run':         RUN_BUILD,
	'clean':       CLEAN_KIND
}

#########################################################
#########################################################

__CHIME_OPTS = ('always', 'a', 'failure', 'f', 'never', 'n')
__CLEAN_OPTS = ('none', 'n', 'shallow', 's', 'deep', 'd')

# Check if config is valid
# Could check to see if the argument exists but I think its fine to let
# Python crash
def check_validity(args):
	global __CHIME_OPTS
	global __CLEAN_OPTS

	valid = True

	if type(args['n-processes']) is not int or args['n-processes'] < 1:
		print(f'ERROR Invalid NUM_PROCESSES: {args["n-processes"]}')
		valid = False

	if type(args['timer']) is not bool:
		print(f'ERROR USE_TIMER is not a bool: {args["timer"]}')
		valid = False

	if type(args['verbose']) is not bool:
		print(f'ERROR VERBOSE is not a bool: {args["verbose"]}')
		valid = False

	if type(args['chime']) is not str or args['chime'] not in __CHIME_OPTS:
		print(f'ERROR chime flag unknown: {args["chime"]}')
		print('  Options are:\n    always, a\n    failure, f\n    never, n')
		valid = False

	if type(args['run']) is not bool:
		print(f'ERROR RUN is not a bool: {args["run"]}')
		valid = False

	if type(args['clean']) is not str or args['clean'] not in __CLEAN_OPTS:
		print(f'ERROR CLEAN flag unknown: {args["clean"]}')
		print('  Options are:\n    none, n\n    shallow, s\n    deep, d')
		valid = False

	return valid

def read_and_update_settings(args, settings) -> bool:
	global __CHIME_OPTS
	global __CLEAN_OPTS
	success = True

	while len(args) > 0:
		arg = args.pop(0)
		match arg:
			case '-Help' | '-h' | '?':
				settings['help'] = 0
			case '/Processes' | '/p':
				if len(args) == 0:
					print(f"ERROR Argument {arg} has no input")
					success = False

				raw = args.pop(0)
				try:
					num = int(raw)
				except ValueError:
					print(f"ERROR {arg} expects an integer: {raw}")
					success = False

				if num < 1:
					print(f"ERROR Processes must be greater than 0: {num}")
					success = False

				settings['n-processes'] = num
			case '-Timer' | '-t':
				settings['timer'] = True
			case '!Timer' | '!t':
				settings['timer'] = False
			case '-Verbose' | '-v':
				settings['verbose'] = True
			case '!Verbose' | '!v':
				settings['verbose'] = False
			case '/Chime' | '/m':
				if len(args) == 0:
					print(f"ERROR Argument {arg} has no input")
					success = False

				kind = args.pop(0)
				if kind in __CHIME_OPTS:
					settings['chime'] = kind
				else:
					print(f"ERROR {arg} expects one of {__CHIME_OPTS}:  {kind}")
					success = False
			case '-Run' | '-r':
				settings['run'] = True
			case '!Run' | '!r':
				settings['run'] = False
			case '/Clean' | '/c':
				if len(args) == 0:
					print(f"ERROR Argument {arg} has no input")
					success = False

				kind = args.pop(0)
				if kind in __CLEAN_OPTS:
					settings['clean'] = kind
				else:
					print(f"ERROR {arg} expects one of {__CLEAN_OPTS}:  {kind}")
					success = False

	return success

if not check_validity(SETTINGS):
	exit(1)

