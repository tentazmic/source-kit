#shader vertex
#version 330 core

layout(location = 0) in vec2  aPosition;
layout(location = 1) in vec4  aColour;
layout(location = 2) in vec2  aTexCoord;

uniform mat4 uViewProjection;

out vec4 vColour;
out vec2 vTexCoord;

void main()
{
	vColour = aColour;
	vTexCoord = aTexCoord;

	gl_Position = uViewProjection * vec4(aPosition, 0.0, 1.0);
}

#shader pixel
#version 330 core

layout(location = 0) out vec4 oColour;

uniform sampler2D uFontAtlas;

in vec4 vColour;
in vec2 vTexCoord;

// TODO Precompute offline
highp float screenPxRange()
{
	const float pxRange = 2.0; // TODO for the distance field's pixel range
	highp vec2 unitRange = vec2(pxRange) / vec2(textureSize(uFontAtlas, 0));
	highp vec2 screenTexSize = vec2(1.0) / fwidth(vTexCoord);
	return max(0.5 * dot(unitRange, screenTexSize), 1.0);
}

float median(float r, float g, float b)
{
	return max(min(r, g), min(max(r, g), b));
}

float lerp(float a, float b, float c)
{
	float diff = b - a;
	return a + (diff * c);
}

vec4 mix2(vec4 a, vec4 b, float s)
{
	return vec4(lerp(a.r, b.r, s), lerp(a.g, b.g, s), lerp(a.b, b.b, s), lerp(a.a, b.a, s));
}

void main()
{
	highp vec3 msd = texture(uFontAtlas, vTexCoord).rgb;
	highp float sd = median(msd.r, msd.g, msd.b);
	highp float screenPxDistance = screenPxRange() * (sd - 0.5);
	highp float opacity = clamp(screenPxDistance + 0.5, 0.0, 1.0);

	oColour = vec4(1, 1, 1, sd);
	// oColour = vec4(1, 1, 1, screenPxDistance);
	if (opacity == 0.0)
		discard;

	// oColour = mix2(vec4(0.0), vColour, opacity);
	oColour = mix2(vec4(0.0), vColour, opacity);
	if (oColour.a == 0.0)
		discard;
}
