#include "pch.h"

#include <imgui.h>
#include "app/application.hpp"
#include "Application/events.hpp"

// TODO Implement a custom backend that uses
// the engine functions
#define IMGUI_DISABLE_WIN32_FUNCTIONS
#define IMGUI_IMPL_OPENGL_LOADER_CUSTOM

#include <GLFW/glfw3.h>
#include <backends/imgui_impl_glfw.cpp>

#include <glad/glad.h>
#include <backends/imgui_impl_opengl3.cpp>

#include "imgui.hpp"

namespace gui {

namespace {
	// void OnWindowClose(evt::event& e);
	// void OnWindowLostFocus(evt::event& e);
	void OnWindowResized(evt::event& e);
	// void OnWindowMoved(evt::event& e);

	void OnKeyPressed(evt::event& e);
	void OnKeyReleased(evt::event& e);
	void OnKeyTyped(evt::event& e);

	void OnMouseMoved(evt::event& e);
	void OnMouseScroll(evt::event& e);
	void OnMouseButtonPressed(evt::event& e);
	void OnMouseButtonReleased(evt::event& e);
};

void Initialise(evt::dispatcher& dispatcher, arr::list<u32> configFlags)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();

	ImGuiIO& io = ImGui::GetIO(); (void)io;
	FOR (it, configFlags)
		io.ConfigFlags |= *it;
	// // io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	// // io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
	// io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
	// io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
	// // io.ConfigViewportsNoAutoMerge = true;
	// // io.ConfigViewportsNoTaskBarIcon = true;

	// ImGui::StyleColorsDark();
	ImGui::StyleColorsClassic();

	// When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
	ImGuiStyle& style = ImGui::GetStyle();
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
			style.WindowRounding = 0.0f;
			style.Colors[ImGuiCol_WindowBg].w = 1.0f;
	}

	ImGui_ImplGlfw_InitForOpenGL(RCAST(GLFWwindow *)(app::NativeWindow()), true);
	ImGui_ImplOpenGL3_Init("#version 410");

	dispatcher.OnWindowResized.AddDelegate(evt::signature::del::Create<&OnWindowResized>());
	dispatcher.OnKeyPressed.AddDelegate(evt::signature::del::Create<&OnKeyPressed>());
	dispatcher.OnKeyReleased.AddDelegate(evt::signature::del::Create<&OnKeyReleased>());
	dispatcher.OnKeyTyped.AddDelegate(evt::signature::del::Create<&OnKeyTyped>());
	dispatcher.OnMouseMoved.AddDelegate(evt::signature::del::Create<&OnMouseMoved>());
	dispatcher.OnMouseScrolled.AddDelegate(evt::signature::del::Create<&OnMouseScroll>());
	dispatcher.OnMouseButtonPressed.AddDelegate(evt::signature::del::Create<&OnMouseButtonPressed>());
	dispatcher.OnMouseButtonReleased.AddDelegate(evt::signature::del::Create<&OnMouseButtonReleased>());
}

void Shutdown()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}

void BeginFrame()
{
	ImGui::GetIO().DisplaySize = ImVec2((f32)app::WindowDimensions().x, (f32)app::WindowDimensions().y);
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void EndFrame()
{
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	ImGuiIO& io = ImGui::GetIO();
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		GLFWwindow* backup_current_context = glfwGetCurrentContext();
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		glfwMakeContextCurrent(backup_current_context);
	}
}

namespace {
	// void OnWindowClose(evt::event& e) { }
	// void OnWindowLostFocus(evt::event& e) { }
	void OnWindowResized(evt::event& e)
	{
		ImGuiIO& io = ImGui::GetIO();
		io.DisplaySize = ImVec2(e.size.x, e.size.y);
		io.DisplayFramebufferScale = ImVec2(1.0f, 1.0f);
	}
	// void OnWindowMoved(evt::event& e) { }

	void OnKeyPressed(evt::event& e)
	{
		ImGuiIO& io = ImGui::GetIO();
		io.KeysDown[e.key.value] = true;
		io.KeyCtrl = io.KeysDown[inp::EKey::LCONTROL.value] || io.KeysDown[inp::EKey::RCONTROL.value];
		io.KeyShift = io.KeysDown[inp::EKey::LSHIFT.value] || io.KeysDown[inp::EKey::RSHIFT.value];
		io.KeyAlt = io.KeysDown[inp::EKey::L_ALT.value] || io.KeysDown[inp::EKey::R_ALT.value];
		io.KeySuper = io.KeysDown[inp::EKey::LSUPER.value] || io.KeysDown[inp::EKey::LSUPER.value];
	}
	void OnKeyReleased(evt::event& e)
	{
		ImGuiIO& io = ImGui::GetIO();
		io.KeysDown[e.key.value] = false;
		io.KeyCtrl = io.KeysDown[inp::EKey::LCONTROL.value] || io.KeysDown[inp::EKey::RCONTROL.value];
		io.KeyCtrl = io.KeysDown[inp::EKey::LSHIFT.value] || io.KeysDown[inp::EKey::RSHIFT.value];
		io.KeyCtrl = io.KeysDown[inp::EKey::L_ALT.value] || io.KeysDown[inp::EKey::R_ALT.value];
		io.KeyCtrl = io.KeysDown[inp::EKey::LSUPER.value] || io.KeysDown[inp::EKey::RSUPER.value];
	}
	void OnKeyTyped(evt::event& e)
	{
		ImGuiIO& io = ImGui::GetIO();
		io.AddInputCharacter(e.key.value);
	}

	void OnMouseMoved(evt::event& e)
	{
		ImGuiIO& io = ImGui::GetIO();
		io.MousePos = ImVec2(e.position.x, e.position.y);
	}
	void OnMouseScroll(evt::event& e)
	{
		ImGuiIO& io = ImGui::GetIO();
		io.MouseWheel += e.offset.y;
		io.MouseWheelH += e.offset.x;
	}
	void OnMouseButtonPressed(evt::event& e)
	{
		ImGuiIO& io = ImGui::GetIO();
		io.MouseDown[e.button.value] = true;
	}
	void OnMouseButtonReleased(evt::event& e)
	{
		ImGuiIO& io = ImGui::GetIO();
		io.MouseDown[e.button.value] = false;
	}
};
};
