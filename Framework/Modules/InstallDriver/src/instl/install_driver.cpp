#include "pch.h"
#include "debug"
#include "core"

#include "install_driver.hpp"

namespace instl {

void install_driver::Startup(pointers& p) const
{
	for (size_t i = 0; i < _startups.count(); i++)
		_startups[i](p);
}

void install_driver::Shutdown() const
{
	for (size_t i = 0; i < _shutdowns.count(); i++)
		_shutdowns[i]();
}

void install_driver::LoadScene(gfx::_TextureLib *t, arr::list<game::sprite> s, arr::list<game::camera> c) const
{
	for (size_t i = 0; i < _loadScenes.count(); i++)
		_loadScenes[i](t, s, c);
}

#define INSTALL_VAR(TYPE, NAME)  const char *id_##NAME = "instl_" #NAME
#define INSTALL_FUNC(TYPE, NAME, ...) const char *id_##NAME = "instl_" #NAME ;
#include "instl/_symbols.ipp"

};
