#pragma once
#include "core"
#include "game_data.hpp"
#include "instl/install.hpp"
#include "Util/list.hpp"
#include "Graphics/texture.hpp"

// The install function calls are not ppprocessed
// as they are added too infrequently to justify it

namespace instl {

struct install_driver
{
	void Startup(pointers&) const;
	void Shutdown() const;
	void LoadScene(gfx::_TextureLib *, arr::list<game::sprite>, arr::list<game::camera>) const;

	friend void FindInstalls(install_driver&);

private:
	arr::dync_list<fn_Startup> _startups;
	arr::dync_list<fn_Shutdown> _shutdowns;
	arr::dync_list<fn_LoadScene> _loadScenes;
};

void FindInstalls(install_driver&);

void SetDriver(install_driver*);
};
