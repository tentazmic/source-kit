# Asset Handling

## Scene Serialisation

An entity is denoted by `ent` followed by its name.
Variables are defined be `IDENTIFIER : VALUE`.
Variables don't declare their types, the deserialiser will
know the appropriate type.

The entity ids don't correspond to the id within the world,
rather its a local id within the scene.

When referencing other assets use `SCOPE :: FILEPATH`. The
scope refers to what context the asset is in, leaving it blank
indicates the global context, i.e. at `Assets/` from the project
root, and a name refers to the identifier for that install, in which
case the path is relative to `INSTALL_PATH/Assets/`.
A value of `::::` shows that there is meant to be no asset loaded
there, done to show that it is intentional.

## Scenes vs Prefabs

A scene can have any number of root level entities. A prefab can only have one.
A prefab can have various other entities so long as they are children of the
root one.
This distinction will be enforced as part of the Editor and LoadPrefab will
print an error if given a scene.
This distinction serves mainly as manner of expression intention.

## Scene Loading

Scene Loading is done in two stages.

The first stage has the scene be loaded into the asset buffer. References to
other assets such as scenes, textures, audio. etc. remain as paths to files.  

The second stage is loading the data from the asset buffer into their runtime
data containers and resolving the asset references. Scene references are loaded
in the first stage.

## (New) Scene Scopes

As scenes are indexed by handles we need a to know when to free the data behind
the scenes. Scopes are the solution to this.  

A scope is a compile time constant. The current scope is set and then all scenes
are loaded as being a part of that scope. When that scope is closed all the memory
used by the scenes in that scope is freed.

There can be a scope for in level gameplay, a scope for menus, a scope fo particular
levels, etc.
