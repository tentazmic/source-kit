#include "pch.h"

#include "font.hpp"
#include "UI/font.hpp"

#include <msdf-atlas-gen.h>
#include <FontGeometry.h>
#include <GlyphGeometry.h>

#include "Graphics/texture.hpp"

#define DEFAULT_ANGLE_THRESHOLD 3.0
#define LCG_MULTIPLIER 6364136223846793005ull
#define LCG_INCREMENT 1442695040888963407ull
#define THREAD_COUNT 8

namespace ui {

struct font_data
{
	std::vector<msdf_atlas::GlyphGeometry> glyphs;
	msdf_atlas::FontGeometry geometry;
};

glyph Font::GetGlyph(u32 c) const
{
	const msdf_atlas::GlyphGeometry *geometry = _data->geometry.getGlyph(c);
	if (!geometry)
		return glyph{};

	glyph g{ .advance = SCAST(f32)(geometry->getAdvance()) };
	f64 left, right, bottom, top;

	geometry->getQuadAtlasBounds(left, bottom, right, top);
	g.atlasBounds = math::rect(SCAST(f32)(left), SCAST(f32)(bottom),
	                           SCAST(f32)(right), SCAST(f32)(top));

	geometry->getQuadPlaneBounds(left, bottom, right, top);
	g.planeBounds = math::rect(SCAST(f32)(left), SCAST(f32)(bottom),
	                           SCAST(f32)(right), SCAST(f32)(top));

	return g;
}

metrics Font::GetMetrics() const
{
	const msdfgen::FontMetrics &m = _data->geometry.getMetrics();
	return metrics{
		.ascenderY = SCAST(f32)(m.ascenderY),
		.descenderY = SCAST(f32)(m.descenderY),
		.lineHeight = SCAST(f32)(m.lineHeight)
	};
};

f32 Font::GetAdvance(u32 curr, u32 next) const
{
	f64 res;
	_data->geometry.getAdvance(res, curr, next);
	return SCAST(f32)(res);
}

};

namespace res {

namespace {
	template<typename PIXEL, typename SOURCE, i32 CHANNELS,
	         msdf_atlas::GeneratorFunction<SOURCE, CHANNELS> GEN_FUNC>
	gfx::texture CreateAtlasTexture(const std::vector<msdf_atlas::GlyphGeometry> &glyphs,
	                                u32 width, u32 height)
	{
		msdf_atlas::GeneratorAttributes attributes;
		attributes.config.overlapSupport = true;
		attributes.scanlinePass = true;

		msdf_atlas::ImmediateAtlasGenerator
		  <SOURCE, CHANNELS,	GEN_FUNC, msdf_atlas::BitmapAtlasStorage<PIXEL, CHANNELS>>
			generator(width, height);
		generator.setAttributes(attributes);
		generator.setThreadCount(8);
		generator.generate(glyphs.data(), SCAST(i32)(glyphs.size()));

		using BMCR = msdfgen::BitmapConstRef<PIXEL, CHANNELS>;
		BMCR bitmap = (BMCR)generator.atlasStorage();

		size_t imageSize = sizeof(PIXEL) * bitmap.width * bitmap.height * CHANNELS;
		void *image = malloc(imageSize);
		memcpy(image, bitmap.pixels, imageSize);

		return gfx::texture{
			.image = image,
			.dimensions = glm::uvec3(bitmap.width, bitmap.height, 1),
			.format = {
				.texel = gfx::ETextureTexel::NORMALISED_UINT8,
				.numChannels = CHANNELS
			}
		};
	}
};

FontLibrary::FontLibrary(gfx::_TextureLib *library, size_t capacity)
	: _library(library), _fonts(capacity), _generations(capacity)
{ }

FontLibrary::~FontLibrary()
{
	FOR (it, _fonts)
	{
		delete it->data;
		free(it->path);
	}
}

hdl_font FontLibrary::Load(const char *path, bool expensiveColouring /* = false */)
{
	FOR (it, _fonts)
	{
		if (strcmp(it->path, path) != 0)
			continue;

		AS_WRN("Already loaded font {}", path);
		hdl_font::TYPE index = it - _fonts.begin();
		return hdl_font{ index, _generations[index] };
	}

	msdfgen::FreetypeHandle *hdlFreetype = msdfgen::initializeFreetype();
	AS_ASSERT(hdlFreetype, "Could not initialise Freetype");

	msdfgen::FontHandle *hdlFont = msdfgen::loadFont(hdlFreetype, path);
	if (!hdlFont)
	{
		AS_ERR("Failed to load font: {}", path);
		return hdl_font{};
	}

	// From imgui_draw.cpp:2809
	const u32 glyphRanges[] = { 0x0020, 0x00FF, 0 };

	msdf_atlas::Charset charset;
	for (size_t i = 0; i < (sizeof(glyphRanges) / sizeof(glyphRanges[0])); i++)
	{
		if (glyphRanges[i] == 0)
			break;

		for (size_t c = glyphRanges[i]; c <= glyphRanges[i + 1]; c++)
			charset.add(c);
	}

	font font;
	font.path = SCAST(char *)(malloc(strlen(path) + 1));
	strcpy(font.path, path);
	font.data = new ui::font_data;
	font.data->geometry = msdf_atlas::FontGeometry(&font.data->glyphs);

	double scale = 1.0;
	i32 glyphsLoaded = font.data->geometry.loadCharset(hdlFont, scale, charset);
	AS_INF("{} / {} Glyphs Loaded", glyphsLoaded, charset.size());

	double emSize = 40.0;
	msdf_atlas::TightAtlasPacker atlasPacker;
	atlasPacker.setPixelRange(2.0);
	atlasPacker.setMiterLimit(1.0);
	atlasPacker.setInnerPixelPadding(0);
	atlasPacker.setOuterPixelPadding(0);
	atlasPacker.setScale(emSize);

	i32 remaining = atlasPacker.pack(font.data->glyphs.data(),
	                                 SCAST(i32)(font.data->glyphs.size()));
	AS_ASSERT(remaining == 0, "Font {} Could not pack {} glyphs", path, remaining);

	i32 width, height;
	// Takes in references
	atlasPacker.getDimensions(width, height);
	emSize = atlasPacker.getScale();

	u64 colouringSeed = 0;
	if (expensiveColouring)
	{
		msdf_atlas::Workload(
			[&glyphs = font.data->glyphs, &colouringSeed](i32 i, i32 threadNo) -> bool {
				u64 glyphSeed = (LCG_MULTIPLIER * (colouringSeed ^ i) + LCG_INCREMENT)
				                * !!colouringSeed;
				glyphs[i].edgeColoring(msdfgen::edgeColoringInkTrap,
				                       DEFAULT_ANGLE_THRESHOLD, glyphSeed);
				return true;
			},
			font.data->glyphs.size())
			.finish(THREAD_COUNT);
	}
	else
	{
		u64 glyphSeed = colouringSeed;
		for (msdf_atlas::GlyphGeometry &glyph : font.data->glyphs)
		{
			glyphSeed *= LCG_MULTIPLIER;
			glyph.edgeColoring(msdfgen::edgeColoringInkTrap, DEFAULT_ANGLE_THRESHOLD, glyphSeed);
		}
	}

	gfx::texture atlas = CreateAtlasTexture<u8, f32, 3, msdf_atlas::msdfGenerator>
		(font.data->glyphs, width, height);

	msdfgen::destroyFont(hdlFont);
	msdfgen::deinitializeFreetype(hdlFreetype);

	font.texture = _library->Load(atlas);
	free(atlas.image);
	hdl_font::TYPE index = _fonts.step(font);
	return hdl_font{ index, _generations.tick(index) };
}

};
