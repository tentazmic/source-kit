#pragma once
#include "core"

namespace res {

struct parser
{
	parser(const char *name, std::istream&);

	char next();
	char peek();
	void back();
	char until(char);

	void skip_whitespace();

	inline size_t stream_pos() const { return _stream.tellg(); }
	inline size_t line_number() const { return _lineNumber; }
	inline size_t line_pos() const { return _linePos; }
	void seek(size_t);

	bool read_until_whitespace(char *, size_t);
	bool read_length(size_t length, char *, size_t buffSize);

	bool at_end() const;

	bool is_next_string(const char*);

	inline const char *name() const { return _name; }

private:
	const char *_name;
	std::istream& _stream;
	// Line Number and Pos refer to the next character in the stream
	size_t _lineNumber;
	size_t _linePos;
	size_t _streamMax;
};

};
