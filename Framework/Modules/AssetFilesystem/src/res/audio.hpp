#pragma once
#include "Audio/audio.hpp"

namespace res {

// As audio deals with file streaming, it needs
// to stay here
struct audio_stream
{
	std::istream *stream;
	size_t end;
	f32 scale = 1.f;

	inline size_t pos() const { return stream->tellg(); }
};

// TODO Complete with the proper audio system
// Only reference audio streams with handles
using hdl_stream = mem::handle<u32, 22, 10>;

struct AudioLibrary
{

};

audio_stream LoadAudioClip(const char *path);
void StreamAudioClip(snd::buffer &, audio_stream &);
};
