#include "pch.h"
#include "shader.hpp"

#include "core"
#include "debug"

#include "parser.hpp"

namespace res {

namespace {

const char * ReadShader(parser &, size_t start, size_t length);

};

bool ParseShader(const char *path, shader_source &ss)
{
	ss = shader_source{ 0, 0 };
	std::ifstream file(path, std::ios::in | std::ios::binary);
	if (!file)
	{
		AS_ERR("Could not read Shader {}", path);
		return false;
	}

	/*
	file.seekg(0, std::ios::end);
	size_t size = file.tellg();
	if (size == MAX(size_t))
	{
		AS_ERR("FAIL Could not read Shader {}", path);
		return false;
	}

	file.seekg(0, std::ios::beg);
	*/
	const char *tkn_DeclareShader = "shader";
	parser src("Shader Parser", file);

	const size_t lenBuff = 14;
	char buffer[lenBuff];
	struct shader_index { size_t start = 0, length = 0; } vertex, pixel;
	shader_index *current = nullptr;
	while (!src.at_end())
	{
		char hash = src.until('#');
		if (hash == 0)
			break;

		size_t potentialShaderEnd = src.stream_pos() - 1;
		if (!src.is_next_string(tkn_DeclareShader))
			continue;

		src.skip_whitespace();
		src.read_until_whitespace(buffer, lenBuff);

		if (current)
			current->length = potentialShaderEnd - current->start;

		if (strcmp(buffer, "vertex") == 0)
		{
			vertex.start = src.stream_pos();
			current = &vertex;
			continue;
		}
		else if (strcmp(buffer, "pixel") == 0)
		{
			pixel.start = src.stream_pos();
			current = &pixel;
			continue;
		}

		AS_ERR("{} {}:{} Unknown shader type '{}'",
		       path, src.line_number(), src.line_pos(), buffer);
		AS_INF("Options: vertex, pixel");
		return false;
	}

	if (current && current->length == 0)
		current->length = src.stream_pos() - current->start;

	if (vertex.start != 0)
		ss.vertex = ReadShader(src, vertex.start, vertex.length);

	if (pixel.start != 0)
		ss.pixel = ReadShader(src, pixel.start, pixel.length);

	// Using && as these two are necessary
	return ss.vertex && ss.pixel;
}

namespace {

const char * ReadShader(parser &p, size_t start, size_t length)
{
	p.seek(start);
	char *buffer = (char *)malloc(length + 1);
	p.read_length(length, buffer, length + 1);
	return buffer;
}
};
};
