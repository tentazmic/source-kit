#pragma once

#include "Graphics/shader.hpp"
#include "Util/wrap_list.hpp"
#include "Memory/generations.hpp"

#include "res/shader.hpp"

namespace gfx {

class OGLShader : public _Shader
{
public:
	OGLShader() : _ID(0), _path(nullptr), _uniformMap()
#ifdef ENABLE_ASSERTS
	              , _valid(false)
#endif
		{ }
	OGLShader(res::shader_source &, const char *);
	~OGLShader() override;

	void Bind() const override;
	void UnBind() const override;

	void UploadInt1(const char *uniform, i32 value) override;
	void UploadFloat1(const char *uniform, f32 value) override;
	void UploadFloat2(const char *uniform, const math::vec2& value) override;
	void UploadFloat3(const char *uniform, const math::vec3& value) override;
	void UploadFloat4(const char *uniform, const math::vec4& value) override;
	void UploadMat3(const char *uniform, const glm::mat3& matrix) override;
	void UploadMat4(const char *uniform, const glm::mat4& matrix) override;

	void UploadInt1Array(const char *, i32*, u32) override;

	const char * GetPath() const override { return _path; }

#ifdef ENABLE_ASSERTS
	inline bool is_valid() const { return _valid; }
#endif

private:
	i32 GetUniformLocation(const char *uniform);

	u32 _ID;
	const char *_path;
	std::unordered_map<std::string, i32> _uniformMap;

#ifdef ENABLE_ASSERTS
	bool _valid;
#endif
};

class OGLShaderLib : public _ShaderLib
{
public:
	OGLShaderLib(size_t capacity);
	~OGLShaderLib() override;

	hdl_shader Load(const char *) override;
	_Shader * Get(hdl_shader handle) override;
	bool IsValid(hdl_shader handle) const override;

private:
	arr::dync_wrap_list<OGLShader> _shaders;
	mem::dync_generations_tracker<hdl_shader::TYPE> _generations;
};
};
