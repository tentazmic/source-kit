#include "pch.h"

#include "_ogl.hpp"
#include "ogl_render_api.hpp"
#include "debug"

#include "ogl_mesh_buffer.hpp"
#include "ogl_shader.hpp"
#include "ogl_texture.hpp"
#include "ogl_frame_buffer.hpp"

namespace rdr {

OGLRenderAPI::OGLRenderAPI()
{
	OGL_CHECK(glEnable(GL_BLEND))
	// OGL_CHECK(glEnable(GL_DEPTH_TEST)) Was causing bad drawing order will figure it out later
	OGL_CHECK(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA))
	RD_INF(
		"OpenGL Renderer: {} - {} - {}",
		glGetString(GL_VENDOR),glGetString(GL_RENDERER), glGetString(GL_VERSION)
	);
}

void OGLRenderAPI::SetClearColour(const glm::vec4& colour)
{
	OGL_CHECK(glClearColor(colour.r, colour.g, colour.b, colour.a))
}

void OGLRenderAPI::SetViewport(u32 x, u32 y, u32 width, u32 height)
{
	OGL_CHECK(glViewport(x, y, width, height))
}

void OGLRenderAPI::Clear()
{
	OGL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT))
}

void OGLRenderAPI::DrawIndexed(gfx::_MeshBuffer *mesh, u32 count /* = 0 */)
{
	mesh->Bind();
	u32 num = count != 0 ? mesh->indexes() : count;
	OGL_CHECK(glDrawElements(GL_TRIANGLES, num, GL_UNSIGNED_INT, nullptr))
}

gfx::_MeshBufferLib * OGLRenderAPI::CreateMeshBufferLibrary(size_t capacity) const
	{ return new gfx::OGLMeshBufferLib(capacity); }
gfx::_ShaderLib * OGLRenderAPI::CreateShaderLibrary(size_t capacity) const
	{ return new gfx::OGLShaderLib(capacity); }
gfx::_TextureLib * OGLRenderAPI::CreateTextureLibrary(size_t capacity) const
	{ return new gfx::OGLTextureLib(capacity); }
gfx::_FrameBufferLib * OGLRenderAPI::CreateFrameBufferLibrary(size_t capacity) const
	{ return new gfx::OGLFrameBufferLib(capacity); }

void OGLRenderAPI::FreeMeshBufferLibrary(gfx::_MeshBufferLib *lib) const { delete lib; }
void OGLRenderAPI::FreeShaderLibrary(gfx::_ShaderLib *lib) const { delete lib; }
void OGLRenderAPI::FreeTextureLibrary(gfx::_TextureLib *lib) const { delete lib; }
void OGLRenderAPI::FreeFrameBufferLibrary(gfx::_FrameBufferLib *lib) const { delete lib; }
};
