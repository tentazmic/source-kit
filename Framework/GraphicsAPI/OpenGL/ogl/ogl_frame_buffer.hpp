#pragma once
#include "core"
#include "Graphics/frame_buffer.hpp"
#include "Util/list.hpp"
#include "Util/wrap_list.hpp"

namespace gfx {

class OGLFrameBuffer : public _FrameBuffer
{
public:
	OGLFrameBuffer() : _data{}, _ID(0), _att_Colour(0), _att_Depth(0) { }
	OGLFrameBuffer(const frame_buffer& frame_buffer);
	~OGLFrameBuffer() override;

	void Recreate();
	void Bind() override;
	void UnBind() override;
	void Resize(u32 width, u32 height) override;
	glm::uvec2 GetSize() const override { return glm::uvec2(_data.width, _data.height); }
	u32 GetColourAttachment() const override { return _att_Colour; }

	const frame_buffer& GetSpec() const override { return _data; }
private:
	frame_buffer _data;
	u32 _ID;
	u32 _att_Colour;
	u32 _att_Depth;
};

class OGLFrameBufferLib : public _FrameBufferLib
{
public:
	OGLFrameBufferLib(size_t capacity);
	~OGLFrameBufferLib() override;
	hdl_frame_bfr Create(const frame_buffer& frameBuffer) override;
	_FrameBuffer * Get(hdl_frame_bfr handle) override;
	bool IsValid(hdl_frame_bfr handle) const override;

private:
	arr::dync_wrap_list<OGLFrameBuffer> _frameBuffers;
	mem::dync_generations_tracker<hdl_frame_bfr::TYPE> _generations;
};
};
