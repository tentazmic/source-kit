#pragma once
#include "math"

namespace math
{
	void PrintMatrix(const glm::mat4& mat);
};
