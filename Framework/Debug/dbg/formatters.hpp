#pragma once
#include "core"
#include <spdlog/fmt/fmt.h>

#include "Memory/generations.hpp"

#include "math"

// #######################
// GLM
// #######################

template<>
struct fmt::formatter<glm::vec2> : fmt::formatter<std::string>
{
	auto format(glm::vec2 v2, format_context &ctx) -> decltype(ctx.out())
	{
		return format_to(ctx.out(), "{{ {}, {} }}", v2.x, v2.y);
	}
};

template<>
struct fmt::formatter<glm::mat4> : fmt::formatter<std::string>
{
	auto format(glm::mat4 mat, format_context &ctx) -> decltype(ctx.out())
	{
		return format_to(ctx.out(), "Matrix");
	}
};

// #######################
// Math
// #######################

template<>
struct fmt::formatter<math::vec2> : fmt::formatter<std::string>
{
	auto format(math::vec2 v2, format_context &ctx) -> decltype(ctx.out())
	{
		return format_to(ctx.out(), "{{ {}, {} }}", v2.x, v2.y);
	}
};

template<>
struct fmt::formatter<math::uvec2> : fmt::formatter<std::string>
{
	auto format(math::uvec2 v2, format_context &ctx) -> decltype(ctx.out())
	{
		return format_to(ctx.out(), "{{ {}, {} }}", v2.x, v2.y);
	}
};

template<>
struct fmt::formatter<math::vec3> : fmt::formatter<std::string>
{
	auto format(math::vec3 v3, format_context &ctx) -> decltype(ctx.out())
	{
		return format_to(ctx.out(), "{{ {}, {}, {} }}", v3.x, v3.y, v3.z);
	}
};

template<>
struct fmt::formatter<math::vec4> : fmt::formatter<std::string>
{
	auto format(math::vec4 v4, format_context &ctx) -> decltype(ctx.out())
	{
		return format_to(ctx.out(), "{{ {}, {}, {}, {} }}", v4.x, v4.y, v4.z, v4.w);
	}
};

template<>
struct fmt::formatter<math::rect> : fmt::formatter<std::string>
{
	auto format(math::rect r, format_context &ctx) -> decltype(ctx.out())
	{
		return format_to(ctx.out(), "[{} {}]", r.min, r.max);
	}
};

template<typename T, size_t I, size_t G>
struct fmt::formatter<mem::handle<T, I, G>> : fmt::formatter<std::string>
{
	auto format(mem::handle<T, I, G> h, format_context &ctx) -> decltype(ctx.out())
	{
		T index = h.index;
		T generation = h.generation;
		return format_to(ctx.out(), "[ {}: {} ]", index, generation);
	}
};
