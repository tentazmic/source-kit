#include "pch.h"
#include "log.hpp"
#include <spdlog/sinks/stdout_color_sinks.h>

#include "core"

#ifdef BLD_INSTALL

#define SETUP_LOGGER(NAME, LOGGER, FORMAT) { \
auto logger = spdlog::get("INSTL-" NAME);    \
if (!logger) {                               \
	logger = spdlog::stdout_color_mt(NAME);    \
	logger->set_pattern("itl " FORMAT); }      \
LOGGER = logger.get(); }

#else

#define SETUP_LOGGER(NAME, LOGGER, FORMAT)    \
LOGGER = spdlog::stdout_color_mt(NAME).get(); \
LOGGER->set_pattern(FORMAT);

#endif

namespace dbg {

spdlog::logger *LogGeneral;
spdlog::logger *LogPlain;

spdlog::logger *LogAssets;
spdlog::logger *LogAudio;
spdlog::logger *LogRender;
spdlog::logger *LogPlatform;

#ifdef BLD_EDITOR
spdlog::logger *LogEditor;
#endif

void Init()
{
	spdlog::set_level(spdlog::level::trace);

	SETUP_LOGGER("PLAIN", LogPlain, "%^%v%$")
	// [FILENAME:LINE_NUM] String
	SETUP_LOGGER("GENERAL", LogGeneral, "[%s:%#] %^%v%$")

	SETUP_LOGGER("ASSETS", LogAssets, "%n [%s:%#] %^%v%$")
	SETUP_LOGGER("AUDIO", LogAudio, "%n [%s:%#] %^%v%$")
	SETUP_LOGGER("RENDER", LogRender, "%n [%s:%#] %^%v%$")
	SETUP_LOGGER("PLATFORM", LogPlatform, "%n [%s:%#] %^%v%$")

#ifdef BLD_EDITOR
	SETUP_LOGGER("EDITOR", LogEditor, "%n [%s:%#] %^%v%$")
#endif
}

void Shutdown()
{
	LogPlain->info("Down");
}

void __debugbreak()
{
	i32 x = 4;
	x = x *x;
}
};
