#pragma once

#include <mutex>
#include "core"

// Data Format
// {"pid":59904,"tid":6,"ts":9026289822,"ph":"E","cat":"v8","name":"V8.Execute","args":{"runtime-call-stats":{}},"tts":81317},
// {"pid":59904,"tid":6,"ts":9026323208,"ph":"B","cat":"v8","name":"V8.Execute","args":{},"tts":81443},

// TODOMAYBE Profiler Refactor
// The current system requires you to manually end and begin new sessions
// when having sessions cover blocks of frames, having #ifdef's in areas
// where there shouldn't be
// Have a map of session names to session objects so that we can
// switch sessions without the session object being in scope
// Have it be more aware of frame blocks

namespace dbg::profile {
using micros = std::chrono::microseconds;
using clock = std::chrono::high_resolution_clock;

using time_point = std::chrono::time_point<clock, micros>;

struct session
{
	~session()
	{
		if (output.is_open())
		{
			output << "]}";
			output.flush();
			output.close();
		}
	}

	char name[32];
	u32 numWrites;
	u32 flushFrequency;
	std::ofstream output;
	std::mutex lock;
};

struct profile_result
{
	char name[128];
	u64 threadID;
	i64 start, end;
};

void NewSession(session *, const char *name);
void SetSession(session *);

void StartSession(session *, const char *filepath, u32 flushFreq = 20);
void EndSession(session *);
void WriteProfile(const profile_result& result);

struct timer
{
public:
	timer(const char *name)
		: _result{ "", 0, 0, 0 }, _stopped{ false }
	{
		strcpy(_result.name, name);
		_result.threadID = std::hash<std::thread::id>{}(std::this_thread::get_id());
		time_point now = std::chrono::time_point_cast<micros>(clock::now());
		_result.start = now.time_since_epoch().count();
	}

	~timer()
	{
		if (!_stopped) stop();
	}

	void stop()
	{
		time_point now = std::chrono::time_point_cast<micros>(clock::now());
		_result.end = now.time_since_epoch().count();
		_stopped = true;
		WriteProfile(_result);
	}

private:
	profile_result _result;
	bool _stopped;
};
};
