#pragma once

#include <GLFW/glfw3.h>

#include "Input/codes.hpp"
#include "Input/input.hpp"

namespace inp {

const EKey EKey::SPACE      { 32 };
const EKey EKey::APOSTROPHE { 39 };  /* ' */
const EKey EKey::COMMA      { 44 };  /* , */
const EKey EKey::MINUS      { 45 };  /* - */
const EKey EKey::PERIOD     { 46 };  /* . */
const EKey EKey::FSLASH     { 47 };  /* / */
const EKey EKey::NM_0       { 48 };
const EKey EKey::NM_1       { 49 };
const EKey EKey::NM_2       { 50 };
const EKey EKey::NM_3       { 51 };
const EKey EKey::NM_4       { 52 };
const EKey EKey::NM_5       { 53 };
const EKey EKey::NM_6       { 54 };
const EKey EKey::NM_7       { 55 };
const EKey EKey::NM_8       { 56 };
const EKey EKey::NM_9       { 57 };
const EKey EKey::SEMICOLON  { 59 };  /* ; */
const EKey EKey::EQUAL      { 61 };  /* = */
const EKey EKey::A          { 65 };
const EKey EKey::B          { 66 };
const EKey EKey::C          { 67 };
const EKey EKey::D          { 68 };
const EKey EKey::E          { 69 };
const EKey EKey::F          { 70 };
const EKey EKey::G          { 71 };
const EKey EKey::H          { 72 };
const EKey EKey::I          { 73 };
const EKey EKey::J          { 74 };
const EKey EKey::K          { 75 };
const EKey EKey::L          { 76 };
const EKey EKey::M          { 77 };
const EKey EKey::N          { 78 };
const EKey EKey::O          { 79 };
const EKey EKey::P          { 80 };
const EKey EKey::Q          { 81 };
const EKey EKey::R          { 82 };
const EKey EKey::S          { 83 };
const EKey EKey::T          { 84 };
const EKey EKey::U          { 85 };
const EKey EKey::V          { 86 };
const EKey EKey::W          { 87 };
const EKey EKey::X          { 88 };
const EKey EKey::Y          { 89 };
const EKey EKey::Z          { 90 };
const EKey EKey::LBRACKET   { 91 };  /* [ */
const EKey EKey::BSLASH     { 92 };  /* \ */
const EKey EKey::RBRACKET   { 93 };  /* ] */
const EKey EKey::GRAVE      { 96 };  /* ` */
const EKey EKey::WORLD_1    { 61 }; /* non-US #1 */
const EKey EKey::WORLD_2    { 62 }; /* non-US #2 */

// Function keys
const EKey EKey::ESC           { 256 };
const EKey EKey::ENTER         { 257 };
const EKey EKey::TAB           { 258 };
const EKey EKey::BACKSPACE     { 259 };
const EKey EKey::INSERT        { 260 };
const EKey EKey::DELETE        { 261 };
const EKey EKey::RIGHT         { 262 };
const EKey EKey::LEFT          { 263 };
const EKey EKey::DOWN          { 264 };
const EKey EKey::UP            { 265 };
const EKey EKey::PAGE_UP       { 266 };
const EKey EKey::PAGE_DOWN     { 267 };
const EKey EKey::HOME          { 268 };
const EKey EKey::END           { 269 };
const EKey EKey::CAPS          { 280 };
const EKey EKey::SCROLL        { 281 };
const EKey EKey::NUM_LOCK      { 282 };
const EKey EKey::PRINT_SCREEN  { 283 };
const EKey EKey::PAUSE         { 284 };
const EKey EKey::F1            { 290 };
const EKey EKey::F2            { 291 };
const EKey EKey::F3            { 292 };
const EKey EKey::F4            { 293 };
const EKey EKey::F5            { 294 };
const EKey EKey::F6            { 295 };
const EKey EKey::F7            { 296 };
const EKey EKey::F8            { 297 };
const EKey EKey::F9            { 298 };
const EKey EKey::F10           { 299 };
const EKey EKey::F11           { 300 };
const EKey EKey::F12           { 301 };
const EKey EKey::F13           { 302 };
const EKey EKey::F14           { 303 };
const EKey EKey::F15           { 304 };
const EKey EKey::F16           { 305 };
const EKey EKey::F17           { 306 };
const EKey EKey::F18           { 307 };
const EKey EKey::F19           { 308 };
const EKey EKey::F20           { 309 };
const EKey EKey::F21           { 310 };
const EKey EKey::F22           { 311 };
const EKey EKey::F23           { 312 };
const EKey EKey::F24           { 313 };
const EKey EKey::F25           { 314 };
const EKey EKey::KP_0          { 320 };
const EKey EKey::KP_1          { 321 };
const EKey EKey::KP_2          { 322 };
const EKey EKey::KP_3          { 323 };
const EKey EKey::KP_4          { 324 };
const EKey EKey::KP_5          { 325 };
const EKey EKey::KP_6          { 326 };
const EKey EKey::KP_7          { 327 };
const EKey EKey::KP_8          { 328 };
const EKey EKey::KP_9          { 329 };
const EKey EKey::KP_DECIMAL    { 330 };
const EKey EKey::KP_DIVIDE     { 331 };
const EKey EKey::KP_MULTIPLY   { 332 };
const EKey EKey::KP_SUBTRACT   { 333 };
const EKey EKey::KP_ADD        { 334 };
const EKey EKey::KP_ENTER      { 335 };
const EKey EKey::KP_EQUAL      { 336 };
const EKey EKey::LSHIFT        { 340 };
const EKey EKey::LCONTROL      { 341 };
const EKey EKey::L_ALT         { 342 };
const EKey EKey::LSUPER        { 343 };
const EKey EKey::RSHIFT        { 344 };
const EKey EKey::RCONTROL      { 345 };
const EKey EKey::R_ALT         { 346 };
const EKey EKey::RSUPER        { 347 };
const EKey EKey::MENU          { 348 };

const EMouseButton EMouseButton::BTN_1  { 0 };
const EMouseButton EMouseButton::BTN_2  { 1 };
const EMouseButton EMouseButton::BTN_3  { 2 };
const EMouseButton EMouseButton::BTN_4  { 3 };
const EMouseButton EMouseButton::BTN_5  { 4 };
const EMouseButton EMouseButton::BTN_6  { 5 };
const EMouseButton EMouseButton::BTN_7  { 6 };
const EMouseButton EMouseButton::BTN_8  { 7 };
const EMouseButton EMouseButton::LEFT   = EMouseButton::BTN_1;
const EMouseButton EMouseButton::RIGHT  = EMouseButton::BTN_2;
const EMouseButton EMouseButton::MIDDLE = EMouseButton::BTN_3;

const EGamepadButton EGamepadButton::FACE_N  { GLFW_GAMEPAD_BUTTON_Y };
const EGamepadButton EGamepadButton::FACE_S  { GLFW_GAMEPAD_BUTTON_A };
const EGamepadButton EGamepadButton::FACE_W  { GLFW_GAMEPAD_BUTTON_X };
const EGamepadButton EGamepadButton::FACE_E  { GLFW_GAMEPAD_BUTTON_B };
const EGamepadButton EGamepadButton::DPAD_U  { GLFW_GAMEPAD_BUTTON_DPAD_UP };
const EGamepadButton EGamepadButton::DPAD_D  { GLFW_GAMEPAD_BUTTON_DPAD_DOWN };
const EGamepadButton EGamepadButton::DPAD_L  { GLFW_GAMEPAD_BUTTON_DPAD_LEFT };
const EGamepadButton EGamepadButton::DPAD_R  { GLFW_GAMEPAD_BUTTON_DPAD_RIGHT };
const EGamepadButton EGamepadButton::L_BUMP  { GLFW_GAMEPAD_BUTTON_LEFT_BUMPER };
const EGamepadButton EGamepadButton::R_BUMP  { GLFW_GAMEPAD_BUTTON_RIGHT_BUMPER };
const EGamepadButton EGamepadButton::START   { GLFW_GAMEPAD_BUTTON_START };
const EGamepadButton EGamepadButton::SELECT  { GLFW_GAMEPAD_BUTTON_BACK };
const EGamepadButton EGamepadButton::HOME    { GLFW_GAMEPAD_BUTTON_GUIDE };
const EGamepadButton EGamepadButton::L_STICK { GLFW_GAMEPAD_BUTTON_LEFT_THUMB };
const EGamepadButton EGamepadButton::R_STICK { GLFW_GAMEPAD_BUTTON_RIGHT_THUMB };

const EGamepadAxis EGamepadAxis::LEFT_X    { GLFW_GAMEPAD_AXIS_LEFT_X };
const EGamepadAxis EGamepadAxis::LEFT_Y    { GLFW_GAMEPAD_AXIS_LEFT_Y };
const EGamepadAxis EGamepadAxis::RIGHT_X   { GLFW_GAMEPAD_AXIS_RIGHT_X };
const EGamepadAxis EGamepadAxis::RIGHT_Y   { GLFW_GAMEPAD_AXIS_RIGHT_Y };
const EGamepadAxis EGamepadAxis::L_TRIGGER { GLFW_GAMEPAD_AXIS_LEFT_TRIGGER };
const EGamepadAxis EGamepadAxis::R_TRIGGER { GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER };

#ifndef BLD_INSTALL
void PopulateControllers()
{
	for (u8 ID = GLFW_JOYSTICK_1; ID <= GLFW_JOYSTICK_4; ID++)
	{
		if (glfwJoystickIsGamepad(ID))
			s_controllers.push(ID);
	}
}

void KeyboardUpdate(keyboard& kb)
{
	for (auto& key : kb.keys)
	{
		i32 state = glfwGetKey(s_window, key.first.value);
		key.second.Update(state == GLFW_PRESS || state == GLFW_REPEAT);
	}
}

void MouseUpdate(mouse& m)
{
	f64 x = 0, y = 0;
	glfwGetCursorPos(s_window, &x, &y);
	m.position = { (f32)x, (f32)y };

	for (auto& button : m.buttons)
	{
		i32 state = glfwGetMouseButton(s_window, button.first.value);
		button.second.Update(state == GLFW_PRESS || state == GLFW_REPEAT);
	}
}

bool GamepadUpdate(gamepad &gp)
{
	GLFWgamepadstate state;
	if (! (glfwJoystickIsGamepad(gp.ID) && glfwGetGamepadState(gp.ID, &state)))
	{
		LG_WRN("Gamepad {} does not exist", gp.ID);
		return false;
	}

	const f32 deadzone = 0.1f;
	gp.control.x = ApplyDeadzone(state.axes[GLFW_GAMEPAD_AXIS_LEFT_X], deadzone);
	// For some reason the y axis is inverted
	gp.control.y = - ApplyDeadzone(state.axes[GLFW_GAMEPAD_AXIS_LEFT_Y], deadzone);
	gp.A.Update(state.buttons[GLFW_GAMEPAD_BUTTON_A] == GLFW_PRESS);
	gp.start.Update(state.buttons[EGamepadButton::START.value] == GLFW_PRESS);

	return true;
}
#endif
};
