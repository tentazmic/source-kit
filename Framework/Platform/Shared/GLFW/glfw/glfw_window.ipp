#pragma once

#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include "debug"

namespace glfw {

void SetVSync(bool enabled)
{
	glfwSwapInterval(enabled ? 1 : 0);
	s_vSync = enabled;
}

void SetGraphicsAPI(app::EGraphicsAPI api)
{
	glfwMakeContextCurrent(s_window);
	i32 success = 0;
	switch (api)
	{
		case app::EGraphicsAPI::OPENGL:
			success = gladLoadGLLoader(RCAST(GLADloadproc)(glfwGetProcAddress));
			break;
	}

	PF_ASSERT(success, "Could not set the graphics API");
}

void GLFWErrorCallback(i32 error, const char *description)
{
	PF_ERR("GLFW Error ({}): {}", error, description);
}

void CreateWindow(const std::map<i32, i32>& windowHints = std::map<i32, i32>{})
{
	// Create the window
	PF_INF("Creating window {} [{}x{}]", s_title, s_width, s_height);

	PF_ASSERT(glfwInit(), "Could not initialise GLFW");

	glfwSetErrorCallback(GLFWErrorCallback);

	for (auto const& it : windowHints)
		glfwWindowHint(it.first, it.second);

	s_window = glfwCreateWindow((i32)s_width, (i32)s_height, s_title, nullptr, nullptr);
	PF_ASSERT(s_window, "Could not create a GLFW window");
	SetGraphicsAPI(s_graphicsAPI);
	SetVSync(s_vSync);

	// Set GLFW Callbacks
	glfwSetWindowSizeCallback(s_window, [](GLFWwindow *win, i32 width, i32 height)
	{
		s_width = width;
		s_height = height;
		GamePlatformEvent(evt::NewWindowResized(glm::uvec2((u32)width, (u32)height)));
	});

	glfwSetWindowCloseCallback(s_window,
		[](GLFWwindow *window) { GamePlatformEvent(evt::NewWindowClose()); });

	glfwSetKeyCallback(s_window, [](GLFWwindow *window, i32 key, i32 scancode, i32 action, i32 mods)
	{
		u16 k = SCAST(u16)(key);
		switch (action)
		{
			case GLFW_PRESS:
				GamePlatformEvent(evt::NewKeyPressed(inp::EKey{ k }, 0));
				break;
			case GLFW_RELEASE:
				GamePlatformEvent(evt::NewKeyReleased(inp::EKey{ k }));
				break;
			case GLFW_REPEAT:
				GamePlatformEvent(evt::NewKeyPressed(inp::EKey{ k }, 1));
				break;
		}
	});

	glfwSetCharCallback(s_window,
		[](GLFWwindow *window, u32 keycode) { GamePlatformEvent(evt::NewKeyTyped(inp::EKey{ SCAST(u16)(keycode) })); });

	glfwSetMouseButtonCallback(s_window, [](GLFWwindow *window, i32 button, i32 action, i32 mods)
	{
		u8 b = SCAST(u8)(button);
		switch (action)
		{
			case GLFW_PRESS:
			{
				GamePlatformEvent(evt::NewMouseButtonPressed(inp::EMouseButton{ b }));
				break;
			}
			case GLFW_RELEASE:
			{
				GamePlatformEvent(evt::NewMouseButtonReleased(inp::EMouseButton{ b }));
				break;
			}
		}
	});

	glfwSetScrollCallback(s_window, [](GLFWwindow *window, f64 xOffset, f64 yOffset)
	{
		GamePlatformEvent(evt::NewMouseScrolled(glm::vec2(xOffset, yOffset)));
	});

	glfwSetCursorPosCallback(s_window, [](GLFWwindow *window, f64 xPos, f64 yPos)
	{
		GamePlatformEvent(evt::NewMouseMoved(glm::vec2(xPos, yPos)));
	});

	glfwSetJoystickCallback([](i32 ID, i32 event)
	{
		if (!glfwJoystickIsGamepad(ID))
			return;

		u8 jid = SCAST(u8)(ID);
		if (event == GLFW_CONNECTED)
		{
			const char *name = glfwGetGamepadName(ID);

			if (s_controllers.contains(jid))
				PF_TRC("Joystick {}:'{}' is already connected", ID, name);
			else
			{
				PF_INF("Gamepad {} Connected: {}", ID, name);
				s_controllers.push(jid);
			}
		}
		else if (event == GLFW_DISCONNECTED)
		{
			if (s_controllers.contains(jid))
			{
				PF_INF("Gamepad {} Disconnected", ID);
				s_controllers.remove_by(jid);
			}
			else
				PF_TRC("Gamepad {} is not connected", ID);
		}
	});
}

void DestroyWindow()
{
	// glfwDestroyWindow(g_window);
	glfwTerminate();
}

};
