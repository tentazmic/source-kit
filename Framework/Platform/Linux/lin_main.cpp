#include "pch.h"

#include <GLFW/glfw3.h>
#include "debug"
#include "soundio.hpp"

#ifdef GAPI_OPENGL
	#include "ogl/ogl_render_api.hpp"
#endif

#include "app/application.hpp"

#ifdef INSTALL_USER
	#include "instl/install.hpp"
	#include "instl/install_driver.hpp"
#endif

namespace {
	GLFWwindow *s_window;

	const char *s_title = PRJ_NAME;

	const struct {
		u32 windowWidth  = 1280;
		u32 windowHeight = 720;
		bool vSync = true;
		app::EGraphicsAPI graphicsAPI = app::EGraphicsAPI::OPENGL;
	} kStarting;

	u32 s_width;
	u32 s_height;
	bool s_vSync;
	app::EGraphicsAPI s_graphicsAPI;

	arr::sttc_list<u8, 4> s_controllers;

	rdr::_RenderAPI * CreateRenderAPI(app::EGraphicsAPI api);
};

namespace app {

	bool alive = true;
	bool running = true;
	bool nextVSync = true;

	bool IsAlive() { return alive; }
	bool IsRunning() { return running; }
	void Kill() { alive = false; }
	void Restart() { running = false; }

	void * NativeWindow() { return s_window; }
	glm::uvec2 WindowDimensions() { return glm::uvec2(s_width, s_height); }

	bool VSyncEnabled() { return s_vSync; }
	void SetVSync(bool enabled) { nextVSync = enabled; }
};

#include "glfw/glfw_window.ipp"
#include "glfw/glfw_input.ipp"

#include "Application/tick.hpp"
#include "Util/list.hpp"
#include "shared_lib_handle.hpp"

#include <libgen.h>         // dirname
#include <unistd.h>         // readlink
#include <linux/limits.h>   // PATH_MAX

namespace {
	const char *s_fullExecutablePath = NULL;

#ifdef INSTALL_USER
	arr::dync_list<shared_library> s_installs;
	bool FindInstallDLLs();
#endif
};

int main(void)
{
	LG_INIT;
	// Get some config from the launcher
	// process the command line arguments

	{
		char result[PATH_MAX];
		ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
		char *path = RCAST(char *)(malloc(count + 1));
		strcpy(path, result);
		path[count] = '\0';
		s_fullExecutablePath = path;
	}

	s_width  = kStarting.windowWidth;
	s_height = kStarting.windowHeight;
	s_vSync = kStarting.vSync;
	s_graphicsAPI = kStarting.graphicsAPI;

#ifdef INSTALL_USER
	// Find Installs
	if (!FindInstallDLLs())
		s_installs.initialise(0);

	instl::install_driver installDriver;
	instl::FindInstalls(installDriver);
#endif

	// By setting app.running to false and leaving app.alive at true
	// we get an application restart
	while (app::alive)
	{
		app::running = true;

		// Doing this here, rather than in the top level of main
		// means that the window will be closed and then recreated upon
		// restarting, not sure if I want one or the other
		glfw::CreateWindow();

		soundio::Initialise(48000, 2.0f, 0.5f);

		rdr::_RenderAPI *api = CreateRenderAPI(s_graphicsAPI);
#ifdef INSTALL_USER
		instl::SetDriver(&installDriver);
#endif

		f64 startTime = glfwGetTime();
		f64 lastTime = startTime;
		GameStartup(app::startup_config{ .api = api }, soundio::GetBuffer());

		u32 frame = 0;

		soundio::Play();

		while (app::running && app::alive)
		{
			frame++;
			f64 time = glfwGetTime();
			tik::frame_stamp fs {
				.frame = frame,
				.deltaTime = SCAST(f32)(time - lastTime),
				.timeSinceStart = SCAST(f32)(time - startTime)
			};

			lastTime = time;

			// Trigger a thread for each of these
			GameUpdate(fs, s_controllers.get());
			GameRender();

			glfwSwapBuffers(s_window);
			glfwPollEvents();

			if (app::nextVSync != s_vSync)
				glfw::SetVSync(app::nextVSync);

			GameFireEvents();

			// f32 timeElapsed = Time::g_currentTime->currentFrameTime - Time::g_currentTime->previousFrameTime;
			// f32 msPerFrame = fs.deltaTime * 1000.0f;
			// f32 fps = 1.0f / fs.deltaTime;
			// PF_TRC("F{}: Timing {:.3f}ms | {:.1f}FPS", frame, msPerFrame, fps);
		}

		soundio::Shutdown();
		GameShutdown();
		delete api;

		glfw::DestroyWindow();
	}

	LG_SHUTDOWN;
	return 0;
}

namespace {
	rdr::_RenderAPI * CreateRenderAPI(app::EGraphicsAPI api)
	{
		switch (api)
		{
	#ifdef GAPI_OPENGL
			case app::EGraphicsAPI::OPENGL:
				return new rdr::OGLRenderAPI();
	#endif
		}

		PF_ERR("{} Render API not available", api);
		return nullptr;
	}

#ifdef INSTALL_USER
	bool FindInstallDLLs()
	{
	#ifdef ENABLE_ASSERTS
		if (!s_fullExecutablePath)
		{
			PF_ERR("FAIL Find Installs without EXE Path");
			return false;
		}
	#endif

		// Paths returned by dirname should not be freed
		char * path = RCAST(char *)(malloc(strlen(s_fullExecutablePath) + 1));
		strcpy(path, s_fullExecutablePath);
		char * directory = dirname(path);
		size_t lenDirectory = strlen(directory);
		char * installPath =
			RCAST(char *)(malloc(lenDirectory + sizeof("Installs") + 2)); // '/' char and NULL
		strcpy(installPath, directory);
		strcpy(installPath + lenDirectory + 1, "Installs");

		if (!fs::exists(installPath))
		{
			PF_DBG("No Installs Found");
			return false;
		}

		u32 count = 0;
		for (const fs::directory_entry& entry : fs::recursive_directory_iterator(installPath))
		{
			if (entry.is_directory() || entry.path().extension().string().compare(INSTALL_EXT) != 0)
				continue;

			count++;
		}

		s_installs.initialise(count);
		for (const fs::directory_entry& entry : fs::recursive_directory_iterator(installPath))
		{
			if (entry.is_directory() || entry.path().extension().string().compare(INSTALL_EXT) != 0)
				continue;

			s_installs.emplace(entry.path().string().c_str());
			if (!s_installs.back().IsValid())
			{
				PF_ERR("{} Not a shared object", entry.path().string());
				s_installs.pop();
				continue;
			}

			if (!s_installs.back().GetFunction("instl_Information"))
			{
				PF_ERR("{} Not an Install", entry.path().string());
				s_installs.pop();
				continue;
			}

			// Use the install info to categorise and filter the install
		}

		free(installPath);
		return true;
	}
#endif
};

#ifdef INSTALL_USER
namespace instl {

void FindInstalls(install_driver &driver)
{
	driver._startups.initialise(s_installs.count());
	driver._shutdowns.initialise(s_installs.count());
	driver._loadScenes.initialise(s_installs.count());

	FOR (it, s_installs)
	{
		install_info *info
		  = RCAST(install_info *)(it->GetFunction(id_Information));
		PF_INF("    Loading Install Functions from {}", info->name);

		fn_Startup startup
		  = RCAST(fn_Startup)(it->GetFunction(id_Startup));
		if (startup)
		{
			driver._startups.push(startup);
			PF_INF(id_Startup);
		}

		fn_Shutdown shutdown
		  = RCAST(fn_Shutdown)(it->GetFunction(id_Shutdown));
		if (shutdown)
		{
			driver._shutdowns.push(shutdown);
			PF_INF(id_Shutdown);
		}

		fn_LoadScene loadScene
		  = RCAST(fn_LoadScene)(it->GetFunction(id_LoadScene));
		if (loadScene)
		{
			driver._loadScenes.push(loadScene);
			PF_INF(id_LoadScene);
		}
	}
}
};
#endif
