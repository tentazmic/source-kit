#include "pch.h"
#include "soundio.hpp"

#include "debug"
#include "app/application.hpp"

#include <soundio/soundio.h>

namespace soundio {

namespace {
	struct {
		SoundIo * soundio;
		SoundIoDevice * device;
		SoundIoOutStream * outstream;

		// Audio Format Data
		u32 samplesPerSecond;
		u32 numChannels;
		f32 latency;
	} s_audio;

	bool s_initialised = false;
	bool s_started = false;
	bool s_playing = false;

	snd::buffer s_buffer;

	void WriteCallback(SoundIoOutStream *outstream, int minFrames, int maxFrames);
};

};

namespace snd {
u32 GetSamplesPerSecond() { return soundio::s_audio.samplesPerSecond; }
};

namespace soundio {

snd::buffer GetBuffer() { return s_buffer; }

void Initialise(u32 samplesPerSecond, f32 bufferDuration, f32 latency)
{
	s_audio.samplesPerSecond = samplesPerSecond;
	s_audio.numChannels = 2;
	s_audio.latency = latency;

	int err;

	s_audio.soundio = soundio_create();
	if (!s_audio.soundio)
	{
		PF_ERR("SoundIo Out of memory for SoundIo");
		return;
	}

	if ((err = soundio_connect(s_audio.soundio)))
	{
		PF_ERR("SoundIo Error Connecting: {}", soundio_strerror(err));
		return;
	}

	soundio_flush_events(s_audio.soundio);

	int idxDefaultOutDevice =
		soundio_default_output_device_index(s_audio.soundio);
	if (idxDefaultOutDevice < 0)
	{
		PF_ERR("SoundIo No Out Device found");
		return;
	}

	s_audio.device =
		soundio_get_output_device(s_audio.soundio, idxDefaultOutDevice);
	if (!s_audio.device)
	{
		PF_ERR("SoundIo Out of memory for Device");
		return;
	}

	LG_INF("SoundIo Output Device: {}", s_audio.device->name);

	s_audio.outstream = soundio_outstream_create(s_audio.device);
	s_audio.outstream->format = SoundIoFormatS16NE;
	s_audio.outstream->write_callback = WriteCallback;
	s_audio.outstream->sample_rate = s_audio.samplesPerSecond;
	s_audio.outstream->software_latency = s_audio.latency;

	if ((err = soundio_outstream_open(s_audio.outstream)))
	{
		PF_ERR("SoundIo Cannot Open Device {}: {}",
		       s_audio.device->name, soundio_strerror(err));
		return;
	}

	if (s_audio.outstream->layout.channel_count < s_audio.numChannels)
	{
		PF_ERR("SoundIo Not Enough Channels WANT {} GOT {}",
		       s_audio.numChannels, s_audio.outstream->layout.channel_count);
		Shutdown();
		return;
	}

	if (s_audio.outstream->layout_error)
		PF_ERR("SoundIo Cannot Set Channel Layout: {}",
		       soundio_strerror(s_audio.outstream->layout_error));

	if (sizeof(snd::sample) != s_audio.outstream->bytes_per_sample)
	{
		PF_ERR("SoundIo Mismatched Sample Size GAME {} OUTSTREAM {}",
		       sizeof(snd::sample), s_audio.outstream->bytes_per_sample);
	}

	s_initialised = true;

	size_t lenBuffer =
		s_audio.outstream->bytes_per_frame * s_audio.samplesPerSecond * bufferDuration;
	snd::sample * fullBuffer = RCAST(snd::sample *)(malloc(lenBuffer));
	s_buffer.length = lenBuffer / s_audio.outstream->bytes_per_frame;
	s_buffer.left = fullBuffer;
	s_buffer.right = fullBuffer + s_buffer.length;
}

void Shutdown()
{
	soundio_outstream_destroy(s_audio.outstream);
	soundio_device_unref(s_audio.device);
	soundio_destroy(s_audio.soundio);

	s_initialised = false;
	s_started = false;
	free(s_buffer.left);
}

void Play()
{
	if (!s_initialised)
	{
		PF_ERR("SoundIo Uninitialised");
		return;
	}

	if (s_playing)
	{
		PF_WRN("SoundIo Attempt to unpause while playing");
		return;
	}

	int err;
	if (s_started)
	{
		if ((err = soundio_outstream_pause(s_audio.outstream, false)))
		{
			PF_ERR("SoundIo Cannot Unpause Device {}: {}",
			       s_audio.device->name, soundio_strerror(err));
			return;
		}
	}
	else
	{
		if ((err = soundio_outstream_start(s_audio.outstream)))
		{
			PF_ERR("SoundIo Cannot Start Device {}: {}",
			       s_audio.device->name, soundio_strerror(err));
			return;
		}
		s_started = true;
	}

	s_playing = true;
}

void Stop()
{
	if (!s_initialised)
	{
		PF_ERR("SoundIo Uninitialised");
		return;
	}

	if (! (s_started || s_playing))
	{
		PF_WRN("SoundIo Attempt to pause paused audio");
	}

	int err = soundio_outstream_pause(s_audio.outstream, true);
	if (err)
	{
		PF_ERR("SoundIo Cannot Pause Device {}: {}",
		       s_audio.device->name, soundio_strerror(err));
		return;
	}

	s_playing = false;
}

namespace {
	void WriteCallback(SoundIoOutStream *outstream, int minFrames, int maxFrames)
	{
		s_buffer.length = maxFrames;
		GameStreamAudio(s_buffer);

		const SoundIoChannelLayout *layout = &outstream->layout;
		float sampleRate = outstream->sample_rate;
		SoundIoChannelArea *areas;

		int err;
		int framesLeft = maxFrames;
		size_t idxBuffer = 0;
		while (framesLeft > 0)
		{
			int nFrames = framesLeft;
			if ((err = soundio_outstream_begin_write(outstream, &areas, &nFrames)))
			{
				PF_ERR(soundio_strerror(err));
				exit(1);
			}

			if (!nFrames)
				break;

			int frame = 0;
			for (; frame < nFrames && idxBuffer < s_buffer.length; frame++)
			{
				snd::sample * ptr = RCAST(snd::sample *)
					(areas[0].ptr + areas[0].step * frame);
				*ptr = s_buffer.left[idxBuffer];
				ptr = RCAST(snd::sample *)
					(areas[1].ptr + areas[1].step * frame);
				*ptr = s_buffer.right[idxBuffer];
				idxBuffer++;
			}

			if (frame != nFrames)
			{
				PF_ERR("SoundIo Not enough data allocated to buffer");

				for (; frame < nFrames; frame++)
				{
					snd::sample * ptr = RCAST(snd::sample *)
						(areas[0].ptr + areas[0].step * frame);
					*ptr = 0;
					ptr = RCAST(snd::sample *)
						(areas[1].ptr + areas[1].step * frame);
					*ptr = 0;
				}
			}

			if ((err = soundio_outstream_end_write(outstream)))
			{
				PF_ERR(soundio_strerror(err));
				exit(1);
			}

			framesLeft -= nFrames;
		}
	}
};
};
