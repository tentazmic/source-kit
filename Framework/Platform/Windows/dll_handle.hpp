#pragma once
#include <Windows.h>

struct dll_handle
{
private:
	HMODULE _library;
	const char *_path;

public:
	dll_handle(const char *filepath)
		: _library(LoadLibrary(filepath)),
		_path(filepath)
	{ }
	~dll_handle() { FreeLibrary(_library); }

	bool IsValid() const { return _library; }
	const char * GetPath() const { return _path; }

	FARPROC GetFunction(const char *name) const { return GetProcAddress(_library, name); }
};
