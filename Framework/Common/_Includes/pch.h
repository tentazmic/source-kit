#pragma once

#include <fstream>
#include <iostream>
#include <thread>
#include <chrono>
#include <algorithm>
#include <filesystem>
#include <type_traits>
#include <iterator> // iterator tags
#include <cstddef> // ptrdiff_t
#include <cstdint>
#include <limits>

#ifdef PLT_LIN
	#include <alloca.h>
#endif
namespace fs = std::filesystem;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
#include <glm/gtc/type_ptr.hpp>

// Data structures
// TODO Be able to remove all of these, replace with own
// structs and classes
// #include <cstring>
// #include <sstream>
#include <unordered_map>
#include <map>
#include <unordered_set>
