#pragma once
#include "core"
#include "math"
#include "Graphics/texture.hpp"

namespace ui {

// To be defined in an asset module
struct font_data;

struct glyph
{
	math::rect atlasBounds;
	math::rect planeBounds;
	f32 advance = 0.0f;
};

struct metrics
{
	f32 ascenderY;
	f32 descenderY;
	f32 lineHeight;
};

class Font
{
public:
	Font() : _data(nullptr), _texture{} { }
	Font(font_data *f, gfx::hdl_texture h): _data(f), _texture(h) { }

	// Functions about the characteristics of the font
	glyph GetGlyph(u32) const;
	metrics GetMetrics() const;
	f32 GetAdvance(u32 curr, u32 next) const;

	inline bool valid() const { return _data; }
	inline gfx::hdl_texture texture() const { return _texture; }

private:
	font_data *_data;
	gfx::hdl_texture _texture;
};

struct text
{
	const Font *font;
	// TODO Keep this const
	const char *string;
	u32 length = MAX(u32);
	f32 lineHeightOffset;
	f32 kerningOffset;
};
};
