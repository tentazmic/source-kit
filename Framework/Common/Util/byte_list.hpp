#pragma once

#include "debug"

namespace arr {

template<bool is_const>
struct byte_iterator
{
	using iterator_category = std::random_access_iterator_tag;
	using difference_type = ptrdiff_t;
	using value_type = std::conditional_t<is_const, const byte, byte>;
	using pointer    = value_type*;
	using reference  = value_type&;

	using self = byte_iterator<is_const>;
	byte_iterator(pointer p, size_t elemSize) : ptr(p), elemSize(elemSize) { }
	template<bool _const>
	byte_iterator(const byte_iterator<_const>& other)
	{
if constexpr (!is_const && _const) {
	LG_ERR("Illegal Assign of CONST to NON-CONST");
} else {
		ptr = other.ptr;
		elemSize = other.elemSize;
}
	}

	template<bool _const>
	self& operator =(const byte_iterator<_const>& other)
	{
if constexpr (!is_const && _const) {
	LG_ERR("Illegal Assign of CONST to NON-CONST");
} else {
		ptr = other.ptr;
		elemSize = other.elemSize;
}
		return *this;
	}

	pointer operator *() { return ptr; }
	pointer operator ->() { return ptr; }

	/* prefix */  self& operator ++() { ptr += elemSize; return *this; }
	/* postfix */ self  operator ++(int) { self tmp = *this; operator++(); return tmp; }
	/* prefix */  self& operator --() { ptr -= elemSize; return *this; }
	/* postfix */ self  operator --(int) { self tmp = *this; operator--(); return tmp; }

	self& operator +=(size_t i) { ptr += i * elemSize; return *this; }
	friend self operator +(const self& iter, size_t i) { return self(iter.ptr + i * iter.elemSize); }
	friend self operator +(size_t i, const self& iter) { return operator+(iter, i); }

	self& operator -=(size_t i) { ptr -= i * elemSize; return *this; }
	friend self operator -(const self& iter, size_t i) { return self(iter.ptr - i * iter.elemSize); }
	friend difference_type operator -(const self& a, const self& b)
	{
#ifdef ENABLE_ASSERTS
		if (a.elemSize != b.elemSize)
		{
			LG_ERR("Illegal Operation on byte_iterator A {} B {}", a.elemSize, b.elemSize);
			return 0;
		}
#endif
		return (a.ptr - b.ptr) / a.elemSize;
	}

	self operator [](int i) const { self iter = *this; iter += i; return iter; }

	pointer ptr;
	size_t elemSize;
};

#define OP_OVERLOAD(OP) template<bool A, bool B> \
bool operator OP(const byte_iterator<A>& a, const byte_iterator<B>& b)

OP_OVERLOAD(==) { return a.ptr == b.ptr; }
OP_OVERLOAD(!=) { return !(a == b); }
OP_OVERLOAD(>)  { return a.ptr > b.ptr; }
OP_OVERLOAD(>=) { return a > b && a == b; }
OP_OVERLOAD(<)  { return !(a > b) && !(a == b); }
OP_OVERLOAD(<=) { return !(a > b); }
#undef OP_OVERLOAD

using biter = byte_iterator<false>;
using bciter = byte_iterator<true>;

struct byte_list
{
	byte_list() : _data{nullptr}, _elementSize{0}, _count{nullptr}, _capacity{0} { }
	byte_list(byte *data, size_t elementSize, size_t *count, size_t capacity)
		: _data{data}, _elementSize(elementSize), _count{count}, _capacity(capacity)
	{ }

	byte_list(const byte_list& other)
		: _data{other._data}, _elementSize(other._elementSize),
		  _count{other._count}, _capacity(other._capacity)
	{ }

	byte_list(byte_list&& other)
		: _data{other._data}, _elementSize(other._elementSize),
		  _count{other._count}, _capacity(other._capacity)
	{
		MOVE_CONSTRUCTOR_CHECK
		other._data = nullptr;
		other._elementSize = 0;
		other._count = nullptr;
		other._capacity = 0;
	}

	byte_list& operator =(const byte_list& other)
	{
		_data = other._data;
		_elementSize = other._elementSize;
		_count = other._count;
		_capacity = other._capacity;
		return *this;
	}

	byte_list& operator =(byte_list&& other)
	{
		MOVE_CHECK
		_data = other._data;
		_elementSize = other._elementSize;
		_count = other._count;
		_capacity = other._capacity;
		other._data = nullptr;
		other._elementSize = 0;
		other._count = nullptr;
		other._capacity = 0;
		return *this;
	}

#define COUNT_PTR
#include "_byte_list_funcs.ipp"
#undef COUNT_PTR

	// size_t * count_ptr() { return _count; }
	friend struct dync_byte_list;

private:
	void clear_pointers() { _data = nullptr; _count = nullptr; }

	byte *_data;
	size_t _elementSize;
	size_t *_count;
	size_t _capacity;
};

template<size_t ELEM_SIZE, size_t CAPACITY>
struct sttc_byte_list
{
	sttc_byte_list() : _count{0} { }

	sttc_byte_list(const sttc_byte_list& other) : _count(other._count)
	{
		memcpy(_data, other._data, ELEM_SIZE * CAPACITY);
	}

	sttc_byte_list(sttc_byte_list&& other)
		: _count(other._count)
	{
		MOVE_CONSTRUCTOR_CHECK
		memcpy(_data, other._data, ELEM_SIZE * CAPACITY);
		other.clear();
	}

	sttc_byte_list& operator =(const sttc_byte_list& other)
	{
		_count = other._count;
		memcpy(_data, other._data, ELEM_SIZE * CAPACITY);
		return *this;
	}

	sttc_byte_list& operator =(sttc_byte_list&& other)
	{
		MOVE_CHECK
		_count = other._count;
		memcpy(_data, other._data, ELEM_SIZE * CAPACITY);
		other.clear();
		return *this;
	}

	inline byte_list get() const { return byte_list(_data, ELEM_SIZE, &_count, CAPACITY); }

#define _capacity CAPACITY
#define _elementSize ELEM_SIZE
#include "_byte_list_funcs.ipp"
#undef _elementSize
#undef _capacity

private:
	byte _data[ELEM_SIZE * CAPACITY];
	size_t _count;
};

struct dync_byte_list
{
	dync_byte_list() : _list() { }
	dync_byte_list(size_t capacity, size_t elementSize) { initialise(capacity, elementSize); }

	dync_byte_list(const dync_byte_list& other) = delete;
	dync_byte_list(dync_byte_list&& other) = default;

	~dync_byte_list() { Free(); }

	dync_byte_list& operator =(const dync_byte_list& other) = delete;
	dync_byte_list& operator =(dync_byte_list&& other) = default;

	void initialise(size_t capacity, size_t elementSize)
	{
		Free();

		const size_t kSize = sizeof(size_t) + elementSize * capacity;
		size_t *data = (size_t *)malloc(kSize);
		if (data)
		{
			*data = 0;
			_list = byte_list(RCAST(byte*)(data + 1), elementSize, data, capacity);
			return;
		}
		LG_ERR("Failed Allocation of {}B", kSize);
		_list = byte_list();
	}

	inline byte_list get() const { return _list; }

	INLINE void push(const byte *item) { _list.push(item); }
	INLINE byte * pop() { return _list.pop(); }
	INLINE void insert(size_t index, const byte *item) { _list.insert(index, item); }
	inline void set(size_t index, const byte *item) { _list.insert(index, item); }

	template<bool is_const>
	INLINE void remove(const byte_iterator<is_const>& it) { _list.remove(it); }
	INLINE void remove(size_t index) { _list.remove(index); }
	INLINE void clear() { _list.clear(); }

	INLINE byte * at(size_t index) { return _list.at(index); }
	INLINE const byte * at(size_t index) const { return _list.at(index); }

	INLINE byte * operator [](size_t index) { return at(index); }
	INLINE const byte * operator [](size_t index) const { return at(index); }

	inline biter find(const byte * other) { return _list.find(other); }
	inline bciter find(const byte * other) const { return _list.find(other); }

	INLINE biter begin() { return _list.begin(); }
	INLINE biter end() { return _list.end(); }

	INLINE biter rbegin() { return _list.rbegin(); }
	INLINE biter rend() { return _list.rend(); }

	INLINE bciter cbegin() const { return _list.cbegin(); }
	INLINE bciter cend() const { return _list.cend(); }

	INLINE bciter rcbegin() const { return _list.rcbegin(); }
	INLINE bciter rcend() const { return _list.rcend(); }

	INLINE byte * front() { return _list.front(); }
	INLINE byte * back() { return _list.back(); }
	INLINE const byte * front() const { return _list.front(); }
	INLINE const byte * back() const { return _list.back(); }

	INLINE bool full() const { return _list.full(); }
	INLINE bool empty() const { return _list.empty(); }

	INLINE byte * data() { return _list.data(); }
	INLINE size_t elementSize() const { return _list.elementSize(); }
	INLINE size_t count() const { return _list.count(); }
	INLINE size_t capacity() const { return _list.capacity(); }

private:
	void Free()
	{
		if (_list.data())
		{
			size_t *start = RCAST(size_t*)(_list.data()) - 1;
			free(start);
			_list.clear_pointers();
		}
	}

	byte_list _list;
};
};
