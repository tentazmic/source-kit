#ifdef COUNT_PTR
	#define COUNT (*_count)
#else
	#define COUNT _count
#endif

void push(access_type item)
{
#ifdef ENABLE_ASSERTS
	if (COUNT == _capacity)
	{
		LG_ERR("List is Full {}", _capacity);
		return;
	}
#endif
	_data[COUNT++] = item;
}

pop_type pop()
{
#ifdef ENABLE_ASSERTS
	if (COUNT == 0)
	{
		LG_ERR("List is Empty");
if constexpr (std::is_move_constructible_v<T>) {
	return std::move(_data[0]);
} else if constexpr (std::is_copy_constructible_v<T>) {
		return _data[0];
} else {
		return &_data[0];
}
	}
#endif
if constexpr (std::is_move_constructible_v<T>) {
	return std::move(_data[COUNT--]);
} else if constexpr (std::is_copy_constructible_v<T>) {
	return _data[COUNT--];
} else {
	return &_data[COUNT--];
}
}

void insert(size_t index, access_type item)
{
#ifdef ENABLE_ASSERTS
	if (index >= _capacity)
	{
		LG_ERR("Out of bounds insert IDX {} CAPACITY {}", index, _capacity);
		return;
	}
	if (COUNT >= _capacity)
	{
		LG_ERR("List is Full {}", _capacity);
		return;
	}
#endif
	if (index >= COUNT)
	{
		push(item);
		return;
	}

	for (size_t i = COUNT - 1; i >= index; i--)
		_data[i + 1] = _data[i];

	_data[index] = item;
	COUNT++;
}

template<class... Args>
void emplace(Args&&... args)
{
#ifdef ENABLE_ASSERTS
	if (COUNT == _capacity)
	{
		LG_ERR("List is Full {}", _capacity);
	}
#endif
	T *slot = &_data[COUNT++];
	new (slot) T(std::forward<Args>(args)...);
}

template<bool is_const>
void remove(const iterator<is_const, T>& it)
{
#ifdef ENABLE_ASSERTS
	if (it < begin() || it >= end())
	{
		LG_ERR("Illegal List Address ITER {}; BEG {} END {}",
			RCAST(uintptr_t)(it.ptr),
			RCAST(uintptr_t)(begin().ptr),
			RCAST(uintptr_t)(end().ptr)
		);
		return;
	}
#endif
	size_t index = it - begin();
	remove(index);
}

void remove(size_t index)
{
#ifdef ENABLE_ASSERTS
	if (index >= COUNT)
	{
		LG_ERR("Out of Bounds Remove IDX {} COUNT {}", index, COUNT);
		return;
	}
#endif
	if (index == COUNT - 1)
	{
		pop();
		return;
	}

	COUNT--;
	for (; index < COUNT; index++)
	{
if constexpr (std::is_copy_constructible_v<T>) {
		_data[index] = _data[index + 1]; 
} else {
		_data[index] = std::move(_data[index + 1]);
}
	}
}

bool remove_by(access_type item)
{
	iter it = find(item);
	if (it == end())
		return false;

	remove(it);
	return true;
}

// TODO Add a slice function that returns an arr::list (doesn't duplicate the data)
//      Then overloads to copy the data into stack and heap lists

bool contains(access_type item) const
{ return find(item) != cend(); }

void clear()
{
if constexpr (!std::is_pod_v<T>) {
	for (auto it = rbegin(); it != rend(); it--)
		it->~T();
}
	COUNT = 0;
}

void fill(access_type value)
{
	for (size_t i = 0; i < _capacity; i++)
	{
if constexpr (std::is_pointer_v<T>) {
		delete _data[i];
} else if constexpr (!std::is_pod_v<T>) {
		_data[i].~T();
}
		_data[i] = value;
	}
	COUNT = _capacity;
}

T& at(size_t index)
{
#ifdef ENABLE_ASSERTS
	if (index >= COUNT)
	{
		LG_ERR("Out of Bounds Access IDX {} COUNT {}", index, COUNT);
		return _data[0];
	}
#endif
	return _data[index];
}

access_type at(size_t index) const
{
#ifdef ENABLE_ASSERTS
	if (index >= COUNT)
	{
		LG_ERR("Out of Bounds Access IDX {} COUNT {}", index, COUNT);
		return _data[0];
	}
#endif
	return _data[index];
}

INLINE T& operator [](size_t index) { return at(index); }
INLINE access_type operator [](size_t index) const { return at(index); }

inline iter find(access_type other) { return std::find(begin(), end(), other); }
inline citer find(access_type other) const { return std::find(cbegin(), cend(), other); }

INLINE iter begin() { return iter(&_data[0]); }
INLINE iter end()   { return iter(&_data[COUNT]); }

INLINE iter rbegin() { return iter(&_data[COUNT - 1]); }
INLINE iter rend()   { return iter(&_data[-1]); }

INLINE citer cbegin() const { return citer(&_data[0]); }
INLINE citer cend() const { return citer(&_data[COUNT]); }

INLINE citer rcbegin() const { return citer(&_data[COUNT - 1]); }
INLINE citer rcend() const { return citer(&_data[-1]); }

INLINE T& front() { return _data[0]; }
INLINE T& back() { return _data[COUNT - 1]; }
INLINE access_type front() const { return _data[0]; }
INLINE access_type back() const { return _data[COUNT - 1]; }

INLINE bool full() const { return COUNT == _capacity; }
INLINE bool empty() const { return COUNT == 0; }

inline T * data() { return _data; }
inline size_t count() const { return COUNT; }
inline size_t capacity() const { return _capacity; }

#undef COUNT
