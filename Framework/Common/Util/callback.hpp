#pragma once
// CREDIT
// Blog Post: https://blog.molecular-matters.com/2011/09/19/generic-type-safe-delegates-and-events-in-c/
// Implementation: http://gameenginegems.com/geg3/geg3-chapter13.zip

#include "debug"
#include "list.hpp"

namespace clbk {

template<typename T> struct delegate;
template<typename T> struct sink;

template<typename T> struct types {};
template<typename R, typename... Args>
struct types<R (Args...)>
{
	using del = delegate<R (Args...)>;
	using snk = sink<R (Args...)>;
};

template<typename T> struct delegate {};
template<typename R, typename... Args>
struct delegate<R (Args...)>
{
	using ProxyFunction = R (*)(void *, Args...);

	template<R (*Function)(Args...)>
	static inline R FunctionProxy(void*, Args... args)
	{
		return Function(std::forward<Args>(args)...);
	}

	template<class C, R (C::*Function)(Args...)>
	static inline R MethodProxy(void *instance, Args... args)
	{
		return (static_cast<C*>(instance)->*Function)(std::forward<Args>(args)...);
	}

	template<class C, R (C::*Function)(Args...) const>
	static inline R ConstMethodProxy(void *instance, Args... args)
	{
		return (static_cast<const C*>(instance)->*Function)(std::forward<Args>(args)...);
	}

public:
	template<R (*Function)(Args...)>
	void Bind()
	{
		_instance = nullptr;
		_proxy = &FunctionProxy<Function>;
	}

	template<class C, R (C::*Function)(Args...)>
	void Bind(C *instance)
	{
		_instance = instance;
		_proxy = &MethodProxy<C, Function>;
	}

	template<class C, R (C::*Function)(Args...) const>
	void Bind(const C *instance)
	{
		_instance = instance;
		_proxy = &ConstMethodProxy<C, Function>;
	}

	// Should this be inlined? Could call this in the operator
	// instead of duplicating
	R Invoke(Args... args) const
	{
		ASSERT((_proxy != nullptr), "Cannot invoke unbound delegate");
		return _proxy(_instance, std::forward<Args>(args)...);
	}

	R operator()(Args... args) const
	{
		ASSERT((_proxy != nullptr), "Cannot invoke unbound delegate");
		return _proxy(_instance, std::forward<Args>(args)...);
	}

	template<R (*Function)(Args...)>
	static delegate<R (Args...)> Create()
	{
		delegate<R (Args...)>  del;
		del.Bind<Function>();
		return del;
	}

	template<class C, R (C::*Function)(Args...)>
	static delegate<R (Args...)> Create(C *instance)
	{
		delegate<R (Args...)>  del;
		del.Bind<C, Function>(instance);
		return del;
	}

	template<class C, R (C::*Function)(Args...)>
	static delegate<R (Args...)> Create(const C *instance)
	{
		delegate<R (Args...)>  del;
		del.Bind<C, Function>(instance);
		return del;
	}

	inline friend bool operator ==(const delegate<R (Args...)>& a, const delegate<R (Args...)>& b)
	{ return a._instance == b._instance && a._proxy == b._proxy; }

	inline friend bool operator !=(const delegate<R (Args...)>& a, const delegate<R (Args...)>& b)
	{ return !(a == b); }

private:
	void *_instance = nullptr;
	ProxyFunction _proxy = nullptr; 
};

template<typename T> struct sink {};
template<typename R, typename... Args>
struct sink<R (Args...)>
{
public:
	using del  = delegate<R (Args...)>;
	using list = arr::sttc_list<del, 16>;

	void AddDelegate(del d)
	{
		_listeners.push(d);
	}

	void RemoveDelegate(del d)
	{
		auto it = std::find(_listeners.begin(), _listeners.end(), d);
		if (it != _listeners.end())
			_listeners.remove(it);
	}

	inline sink& operator +=(del d)
		{ AddDelegate(d); return *this; }
	inline sink& operator -=(del d)
		{ RemoveDelegate(d); return *this; }

	inline void Clear() { _listeners.Clear(); }

	INLINE typename list::iter rbegin() { return _listeners.rbegin(); }
	INLINE typename list::iter rend() { return _listeners.rend(); }

	INLINE typename list::citer rcbegin() const { return _listeners.rcbegin(); }
	INLINE typename list::citer rcend() const { return _listeners.rcend(); }

private:
	list _listeners;
};

template<typename R, typename... Args>
void Invoke(const sink<R (Args...)>& snk, Args... args)
{
	RCFOR (del, snk)
		del->Invoke(std::forward<Args>(args)...);
}
};
