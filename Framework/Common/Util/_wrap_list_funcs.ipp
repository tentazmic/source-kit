#ifdef PTRS
	#define COUNT (*_count)
	#define HEAD (*_head)
#else
	#define COUNT _count
	#define HEAD _head
#endif

size_t step(access_type item)
{
#ifdef ENABLE_ASSERTS
	if (_capacity == 0)
	{
		LG_ERR("Wrap List is Empty");
		return 0;
	}
#endif
	size_t i = HEAD;
	if (!full())
	{
		_data[HEAD++] = item;
		COUNT = HEAD;
		return i;
	}

if constexpr (std::is_pod_v<T>) {
	(&_data[HEAD])->~T();
}
	HEAD++;
	_data[i] = item;
	HEAD %= _capacity;
	return i;
}

T * next_slot()
{
#ifdef ENABLE_ASSERTS
	if (_capacity == 0)
	{
		LG_ERR("Wrap List is Empty");
		return nullptr;
	}
#endif
	size_t next = HEAD++;
	HEAD %= _capacity;
	if (COUNT < _capacity) COUNT++;
	else
	{
if constexpr (std::is_pointer_v<T>) {
		delete _data[next];
} else if constexpr (!std::is_pod_v<T>) {
		_data[next].~T();
}
	}
	return &_data[next];
}

void clear()
{
if constexpr (std::is_pointer_v<T>) {
	for (size_t i = 0; i < COUNT; i++)
		delete _data[i];
} else if constexpr (!std::is_pod_v<T>) {
	for (size_t i = 0; i < COUNT; i++)
		_data[i].~T();
}
	HEAD = 0;
	COUNT = 0;
}

T& at(size_t index)
{
#ifdef ENABLE_ASSERTS
	if (index >= COUNT)
	{
		LG_ERR("Out of Bounds Access IDX {} COUNT {}", index, COUNT);
		return _data[0];
	}
#endif
	return _data[index];
}

access_type at(size_t index) const
{
#ifdef ENABLE_ASSERTS
	if (index >= COUNT)
	{
		LG_ERR("Out of Bounds Access IDX {} COUNT {}", index, COUNT);
		return _data[0];
	}
#endif
	return _data[index];
}

INLINE T& operator [](size_t index) { return at(index); }
INLINE access_type operator [](size_t index) const { return at(index); }

INLINE iter find(access_type obj) { return std::find(begin(), end(), obj); }
INLINE citer find(access_type obj) const { return std::find(cbegin(), cend(), obj); }

INLINE iter begin() { return iter(&_data[0]); }
INLINE iter end() { return iter(&_data[COUNT]); }

INLINE iter rbegin() { return iter(&_data[COUNT - 1]); }
INLINE iter rend() { return iter(&_data[-1]); }

INLINE citer cbegin() const { return citer(&_data[0]); }
INLINE citer cend() const { return citer(&_data[COUNT]); }

INLINE citer rcbegin() const { return citer(&_data[COUNT - 1]); }
INLINE citer rcend() const { return citer(&_data[-1]); }

INLINE bool full() const { return COUNT == _capacity; }

INLINE T * data() { return _data; }
INLINE size_t head() const { return HEAD; }
INLINE size_t count() const { return COUNT; }
INLINE size_t capacity() const { return _capacity; }

#undef COUNT
#undef HEAD