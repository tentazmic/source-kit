#ifdef LENGTH_PTR
	#define LENGTH (*_length)
#else
	#define LENGTH _length
#endif

friend bool operator ==(const BITSET& A, const BITSET& B)
{
#ifdef ENABLE_ASSERTS
	if (A.length() != B.length())
	{
		LG_ERR("Attempted equality on BITSETs of different length {} {}",
			A.length(), B.length());
		return false;
	}
#endif
	size_t length = GetMinBytes(A.length(), B.length());
	return std::memcmp(A.data(), B.data(), length) == 0;
}

friend bool operator !=(const BITSET& A, const BITSET& B)
{ return !(A == B); }

#define OPERATOR(OP)                                  \
BITSET& operator OP##=(const BITSET& other)           \
{                                                     \
	size_t count = GetMinBytes(LENGTH, other.length()); \
	for (size_t i = 0; i < count; i++)                  \
		_data[i] OP##= other._data[i];                    \
	return *this;                                       \
}

OPERATOR(&)
OPERATOR(|)
OPERATOR(^)

#undef OPERATOR

bit at(size_t index)
{
#ifdef ENABLE_ASSERTS
	if (index >= LENGTH)
	{
		LG_ERR("Out of Bounds Bit Access IDX {} LENGTH {}", index, LENGTH);
		return bit(nullptr, 0);
	}
#endif

	const size_t offset = index / 8;
	return bit(&_data[offset], index & 0b111); // Does the same as % 8
}
const bit at(size_t index) const
{
#ifdef ENABLE_ASSERTS
	if (index >= LENGTH)
	{
		LG_ERR("Out of Bounds Bit Access IDX {} LENGTH {}", index, LENGTH);
		return bit(nullptr, 0);
	}
#endif

	const size_t offset = index / 8;
	return bit(&_data[offset], index & 0b111); // Does the same as % 8
}

INLINE bit operator [](size_t index) { return at(index); }
INLINE const bit operator [](size_t index) const { return at(index); }

void flip()
{
	size_t count = BYTE_COUNT;
	for (size_t i = 0; i < count; i++)
		_data[i] = ~_data[i];
}

void clear()
{
	memset(_data, 0, BYTE_COUNT);
}

#undef LENGTH