#include "pch.h"
#include "string.hpp"

namespace str {

bool EndsWith(const char *str, const char *suffix)
{
	size_t lStr = strlen(str);
	size_t lSuffix = strlen(suffix);
	return strcmp(suffix, &str[lStr - lSuffix]) == 0;
}

};
