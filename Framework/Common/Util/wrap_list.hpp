#pragma once

#include "list.hpp"
#include "debug"

namespace arr {

template<typename T> struct dync_wrap_list;

template<typename T>
struct wrap_list
{
	using access_type = std::conditional_t<sizeof(T) <= sizeof(void*), T, const T&>;
	using iter = iterator<false, T>;
	using citer = iterator<true, T>;

	wrap_list()
		: _data{nullptr}, _head(nullptr), _count{nullptr}, _capacity{0} { }
	wrap_list(T *data, size_t *head, size_t *count, size_t capacity)
		: _data{data}, _head(head), _count{count}, _capacity{capacity}
	{ }

	wrap_list(const wrap_list& other)
		: _data{other._data}, _head(other._head),
		  _count(other._count), _capacity{other._capacity}
	{ }

	wrap_list(wrap_list&& other)
		: _data{other._data}, _head(other._head),
		  _count(other._count), _capacity{other._capacity}
	{ MOVE_CONSTRUCTOR_CHECK }

	wrap_list& operator =(const wrap_list& other)
	{
		_data = other._data;
		_head = other._head;
		_count = other._count;
		_capacity = other._capacity;
		return *this;
	}

	wrap_list& operator =(wrap_list&& other)
	{
		MOVE_CHECK
		_data = other._data;
		_head = other._head;
		_count = other._count;
		_capacity = other._capacity;
		other._data = nullptr;
		other._head = nullptr;
		other._count = nullptr;
		other._capacity = 0;
		return *this;
	}

#define PTRS
#include "_wrap_list_funcs.ipp"
#undef PTRS

	// inline size_t * head_ptr() { return _head; }

	friend struct dync_wrap_list<T>;

private:
	void clear_pointers() { _data = nullptr; _head = nullptr; _count = nullptr; }

	T *_data;
	size_t *_head;
	size_t *_count;
	size_t _capacity;
};

template<typename T, size_t CAPACITY>
struct sttc_wrap_list
{
	using access_type = std::conditional_t<sizeof(T) <= sizeof(void*), T, const T&>;
	using iter = iterator<false, T>;
	using citer = iterator<true, T>;

	sttc_wrap_list() : _head{0}, _count{0} { }
	sttc_wrap_list(std::initializer_list<T> l) : _head{0},  _count{0}
	{
		for (auto it : l)
		{
			if (_count == CAPACITY)
			{
				LG_ERR("Init List is Too Large CAP {} LIST SIZE {}", CAPACITY, l.size());
				return;
			}
			_data[_count] = it;
			_count++;
		}
	}

	sttc_wrap_list(const sttc_wrap_list& other)
		: _head(other._head), _count(other._count)
	{
		memcpy(_data, other._data, sizeof(T) * _count);
	}

	sttc_wrap_list(sttc_wrap_list&& other)
	{
		MOVE_CONSTRUCTOR_CHECK
		_head = other._head;
		_count = other._count;
		memcpy(_data, other._data, sizeof(T) * _count);
		other.clear();
	}

	sttc_wrap_list& operator =(const sttc_wrap_list& other)
	{
		_head = other._head;
		_count = other._count;
		memcpy(_data, other._data, sizeof(T) * _count);
		return *this;
	}

	sttc_wrap_list& operator =(sttc_wrap_list&& other)
	{
		MOVE_CHECK
		_head = other._head;
		_count = other._count;
		memcpy(_data, other._data, sizeof(T) * _count);
		other.clear();
		return *this;
	}

	inline wrap_list<T> get() const { return wrap_list<T>(_data, &_head, &_count, CAPACITY); }

#define _capacity CAPACITY
#include "_wrap_list_funcs.ipp"
#undef _capacity

private:
	T _data[CAPACITY];
	size_t _head;
	size_t _count;
};

template<typename T>
struct dync_wrap_list
{
	using list = wrap_list<T>;
	using access_type = typename list::access_type;
	using iter = typename list::iter;
	using citer = typename list::citer;

	dync_wrap_list() : _list() { }
	dync_wrap_list(size_t capacity) { initialise(capacity); }

	dync_wrap_list(const dync_wrap_list& other) = delete;
	dync_wrap_list(dync_wrap_list&& other) = default;

	~dync_wrap_list() { Free(); }

	dync_wrap_list& operator =(const dync_wrap_list& other) = delete;
	dync_wrap_list& operator =(dync_wrap_list&& other) = default;


	void initialise(size_t capacity)
	{
		Free();

		const size_t kSize = sizeof(size_t) * 2 + sizeof(T) * capacity;
		size_t *data = (size_t *)malloc(kSize);
		if (data)
		{
			size_t *head = data;
			size_t *count = data + 1;
			*head = 0;
			*count = 0;
			_list = list(RCAST(T*)(data + 2), head, count, capacity);
			return;
		}
		LG_ERR("Failed Allocation of {}B", kSize);
		_list = list();
	}

	inline list get() const { return _list; }

	inline size_t step(access_type item) { return _list.step(item); }
	inline T * next_slot() { return _list.next_slot(); }

	inline void clear() { return _list.clear(); }

	INLINE T& at(size_t index) { return _list.at(index); }
	INLINE access_type at(size_t index) const { return _list.at(index); }

	INLINE T& operator [](size_t index) { return _list[index]; }
	INLINE access_type operator [](size_t index) const { return _list[index]; }

	INLINE iter find(access_type other) { return _list.find(other); }
	INLINE citer find(access_type other) const { return _list.find(other); }

	INLINE iter begin() { return _list.begin(); }
	INLINE iter end() { return _list.end(); }

	INLINE iter rbegin() { return _list.rbegin(); }
	INLINE iter rend() { return _list.rend(); }

	INLINE iter cbegin() const { return _list.cbegin(); }
	INLINE iter cend() const { return _list.cend(); }

	INLINE iter rcbegin() const { return _list.rcbegin(); }
	INLINE iter rcend() const { return _list.rcend(); }

	INLINE bool full() const { return _list.full(); }

	INLINE T * data() { return _list.data(); }
	INLINE size_t head() const { return _list.head(); }
	INLINE size_t count() const { return _list.count(); }
	INLINE size_t capacity() const { return _list.capacity(); }

private:
	void Free()
	{
		if (_list.data())
		{
			size_t *start = RCAST(size_t *)(_list.data()) - 2;
			free(start);
			_list.clear_pointers();
		}
	}

	list _list;
};
};
