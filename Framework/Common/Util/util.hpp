#pragma once
#include <string>
#include <string_view>
#include <array>
#include <utility>

namespace util
{
#ifndef BLD_REL
	template<std::size_t... Idxs>
	constexpr auto substringAsArray(std::string_view str, std::index_sequence<Idxs...>)
	{
		return std::array{ str[Idxs]..., '\0' };
	}

	template<typename T>
	constexpr auto typeNameArray()
	{
	#ifdef __GNUC__
		constexpr std::string_view prefix   = std::string_view{ "with T = " };
		constexpr std::string_view suffix   = std::string_view{ "]" };
		constexpr std::string_view function = std::string_view{ __PRETTY_FUNCTION__ };
	#else
		#error Unsupported compiler for typeNameArray
	#endif

		constexpr size_t start = function.find(prefix) + prefix.size();
		constexpr size_t end   = function.rfind(suffix);
		static_assert(start < end);

		constexpr std::string_view name = function.substr(start, end - start);
		return substringAsArray(name, std::make_index_sequence<name.size()>{});
	}

	template<typename T>
	struct TypeNameHolder
	{
		static inline constexpr auto value = typeNameArray<T>();
	};

	template<typename T>
	constexpr const char * TypeName()
	{
		return TypeNameHolder<T>::value.data();
	}
#endif
};
