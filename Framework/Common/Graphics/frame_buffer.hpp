#pragma once
#include "core"
#include "Memory/generations.hpp"

namespace gfx {

using hdl_frame_bfr = mem::handle<u16, 8, 8>;

struct frame_buffer
{
	u32 width;
	u32 height;
	u32 samples;
	bool swapChainTarget = false;

	frame_buffer& operator=(const frame_buffer& other)
	{
		if (this == &other)
			return *this;

		width = other.width;
		height = other.height;
		samples = other.samples;
		swapChainTarget = other.swapChainTarget;
		return *this;
	}
};

class _FrameBuffer
{
public:
	virtual ~_FrameBuffer() { }
	virtual const frame_buffer& GetSpec() const = 0;
	virtual void Bind() = 0;
	virtual void UnBind() = 0;
	virtual void Resize(u32 width, u32 height) = 0;
	virtual glm::uvec2 GetSize() const = 0;
	virtual u32 GetColourAttachment() const = 0;
};

class _FrameBufferLib
{
public:
	virtual ~_FrameBufferLib() { }
	virtual hdl_frame_bfr Create(const frame_buffer& frameBuffer) = 0;
	virtual _FrameBuffer * Get(hdl_frame_bfr handle) = 0;
	virtual bool IsValid(hdl_frame_bfr handle) const = 0;

	inline _FrameBuffer * CreateImmediate(const frame_buffer& frameBuffer)
	{
		hdl_frame_bfr handle = Create(frameBuffer);
		return Get(handle);
	}
};
};
