#pragma once

#include "core"
#include "debug"
#include "Util/list.hpp"
#include "Memory/generations.hpp"
#include "Graphics/unit.hpp"

namespace gfx {

using hdl_mesh = mem::handle<u16, 8, 8>;

// You submit an array of vbdatas and an ibdata and get a handle to the vertex array
// which contains handles to the stored vbs and ibs

enum struct EUniformType
{
	NONE = 0,
	BOOL,
	FLOAT1, FLOAT2, FLOAT3, FLOAT4,
	INT1, INT2, INT3, INT4,
	MAT3x3, MAT4x4
};

enum struct EVertexBufferType { STATIC, DYNAMIC };

u32 GetUniformTypeSize(const EUniformType type);
u32 GetUniformComponentCount(const EUniformType type);

#ifdef HOLD_IDS
	#define VB_ELEM(NAME, TYPE, NRM) \
		::gfx::vertex_buffer_element{ NAME, gfx::EUniformType:: TYPE, NRM, 0 }
#else
	#define VB_ELEM(NAME, TYPE, NRM) \
		::gfx::vertex_buffer_element{ gfx::EUniformType:: TYPE, NRM, 0 }
#endif


// Defines a single element within a vertex buffer
struct vertex_buffer_element
{
#ifdef HOLD_IDS
	const char *name;
#endif
	EUniformType type;
	bool normalised = false;

	u8 offset; // The elements offset in bytes from the first byte of the vertex

	inline u32 size() const { return GetUniformTypeSize(type); }
};

struct vertex_buffer_layout
{
	arr::sttc_list<vertex_buffer_element, 5> elements;
	u32 stride;

	vertex_buffer_layout() : elements(), stride(0) { }

	vertex_buffer_layout(std::initializer_list<vertex_buffer_element> elems)
		: elements(), stride(0)
	{
		u8 offset = 0;
		for (auto e : elems)
		{
			e.offset = offset;
			u32 size = e.size();
			offset += size;
			stride += size;
			elements.push(e);
		}
	}
};

struct vertex_buffer_data
{
	void *data = nullptr;
	u32 size = 0;
	EVertexBufferType type = EVertexBufferType::STATIC;
	vertex_buffer_layout layout;
};

struct index_buffer_data
{
	using TYPE = u32;
	TYPE *indexes;
	u64 count;
};

// The thing is the only things that will use this functionality
// will be the render queue and the centralised buffer containers
// Not sure if we're going to need this
class _VertexBuffer
{
public:
	virtual ~_VertexBuffer() { }
	virtual void Bind() const = 0;
	virtual void UnBind() const = 0;

	virtual void AssignData(void *, u32) = 0;

	virtual const vertex_buffer_layout& layout() const = 0;
};

class _IndexBuffer
{
public:
	virtual ~_IndexBuffer() { }
	virtual void Bind() const = 0;
	virtual void UnBind() const = 0;

	virtual u32 count() const = 0;
};

class _MeshBuffer
{
public:
	virtual ~_MeshBuffer() { }
	virtual void Bind() const = 0;
	virtual void UnBind() const = 0;
	virtual unit<_VertexBuffer> GetVertexBuffer(u8 idx) = 0;

	virtual u32 indexes() const = 0;
	virtual u32 vertexes() const = 0;
	virtual u32 triangles() const = 0;
};

class _MeshBufferLib
{
public:
	virtual ~_MeshBufferLib() { }
	virtual hdl_mesh Submit(index_buffer_data ib, const arr::list<vertex_buffer_data>& vbs) = 0;
	virtual _MeshBuffer * Get(hdl_mesh h) = 0;
	virtual bool IsValid(hdl_mesh h) const = 0;
};
};
