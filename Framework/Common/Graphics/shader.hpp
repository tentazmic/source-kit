#pragma once

#include "core"
#include "math"
#include "Memory/generations.hpp"

namespace gfx {

using hdl_shader = mem::handle<u16, 8, 8>;

class _Shader
{
public:
	virtual ~_Shader() { }

	virtual void Bind() const = 0;
	virtual void UnBind() const = 0;

	virtual void UploadInt1(const char *uniform, i32 value) = 0;
	virtual void UploadFloat1(const char *uniform, f32 value) = 0;
	virtual void UploadFloat2(const char *uniform, const math::vec2& value) = 0;
	virtual void UploadFloat3(const char *uniform, const math::vec3& value) = 0;
	virtual void UploadFloat4(const char *uniform, const math::vec4& value) = 0;
	virtual void UploadMat3(const char *uniform, const glm::mat3& matrix) = 0;
	virtual void UploadMat4(const char *uniform, const glm::mat4& matrix) = 0;

	virtual void UploadInt1Array(const char *, i32*, u32) = 0;

	virtual const char * GetPath() const = 0;
};

class _ShaderLib
{
public:
	virtual ~_ShaderLib() { }
	virtual hdl_shader Load(const char *) = 0;
	virtual _Shader * Get(hdl_shader handle) = 0;
	virtual bool IsValid(hdl_shader handle) const = 0;
};
};
