#include "pch.h"
#include "mesh_buffer.hpp"

namespace gfx {

u32 GetUniformTypeSize(const EUniformType type)
{
	switch (type)
	{
		case EUniformType::BOOL:      return 1;
		case EUniformType::FLOAT1:    return sizeof(f32) * 1;
		case EUniformType::FLOAT2:    return sizeof(f32) * 2;
		case EUniformType::FLOAT3:    return sizeof(f32) * 3;
		case EUniformType::FLOAT4:    return sizeof(f32) * 4;
		case EUniformType::INT1:      return sizeof(i32) * 1;
		case EUniformType::INT2:      return sizeof(i32) * 2;
		case EUniformType::INT3:      return sizeof(i32) * 3;
		case EUniformType::INT4:      return sizeof(i32) * 4;
		case EUniformType::MAT3x3:    return sizeof(f32) * 3 * 3;
		case EUniformType::MAT4x4:    return sizeof(f32) * 4 * 4;
		case EUniformType::NONE:      break;
	};

	ASSERT(false, "Unknown EUniformType");
	return 0;
}

u32 GetUniformComponentCount(const EUniformType type)
{
	switch (type)
	{
		case EUniformType::BOOL:      return 1;
		case EUniformType::FLOAT1:    return 1;
		case EUniformType::FLOAT2:    return 2;
		case EUniformType::FLOAT3:    return 3;
		case EUniformType::FLOAT4:    return 4;
		case EUniformType::INT1:      return 1;
		case EUniformType::INT2:      return 2;
		case EUniformType::INT3:      return 3;
		case EUniformType::INT4:      return 4;
		case EUniformType::MAT3x3:    return 3 * 3;
		case EUniformType::MAT4x4:    return 4 * 4;
		case EUniformType::NONE:      break;
	};

	ASSERT(false, "Unknown EUniformType");
	return 0;
}
};
