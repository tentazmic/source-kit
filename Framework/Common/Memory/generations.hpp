#pragma once
#include "Util/list.hpp"
#include "Util/magic.hpp"

namespace mem {

#ifndef BLD_REL
template<
	typename T, size_t INDEX_SIZE, size_t GEN_SIZE,
	typename = std::enable_if_t<same_num<(INDEX_SIZE + GEN_SIZE), sizeof(T) * 8>::value>,
	typename = std::enable_if_t<std::is_unsigned_v<T>>
>
struct handle
{
	using TYPE = T;
	T index : INDEX_SIZE;
	T generation : GEN_SIZE;

	// Had to get rid of this
	// There's no way to check activeness in release so
	// better not get used to using this
	// inline bool IsActive() const { return generation != 0; }

	inline bool operator ==(const handle<T, INDEX_SIZE, GEN_SIZE>& other) const
	{ return index == other.index; } // && generation == other.generation;
	inline bool operator !=(const handle<T, INDEX_SIZE, GEN_SIZE>& other) const
	{ return !(*this == other); }
};
#else
template<typename T>
struct handle
{
	using TYPE = T;
	T index;
};
#endif

template<typename HDL>
constexpr HDL make_dead()
{
	union
	{
		HDL h;
		typename HDL::TYPE t;
	};
	t = MAX(typename HDL::TYPE);
	return h;
}

#ifndef BLD_REL
template<typename T, std::enable_if_t<std::is_integral_v<T>, bool> = true>
struct dync_generations_tracker;

template<typename T, std::enable_if_t<std::is_integral_v<T>, bool> = true>
struct generations_tracker
{
	using list = arr::list<T>;

	generations_tracker() : _list() { }
	generations_tracker(T *data, size_t *head, size_t *count, size_t capacity)
		: _head(head), _list(data, count, capacity) { }

	generations_tracker(const list& l) : _list(l) { }
	generations_tracker(const generations_tracker& other) : _head(other._head), _list(other._list) { }
	generations_tracker(generations_tracker&& other)
		: _head(other._head), _list(std::move(other._list))
	{
		MOVE_CONSTRUCTOR_CHECK
		other._head = nullptr;
	}

	generations_tracker& operator =(const generations_tracker& other)
	{ _head = other._head; _list = other._list; return *this; }
	generations_tracker& operator =(generations_tracker&& other)
	{
		MOVE_CHECK
		_head = other._head;
		_list = std::move(other._list);
		other._head = nullptr;
		return *this;
	}

	T tick(size_t index)
	{
		_list[index]++;
		if (_list[index] == 0) _list[index]++;
		return _list[index];
	}

	void clear_gen(size_t index) { _list[index] = 0; }

	INLINE T& operator [](size_t index) { return _list[index]; }
	INLINE T operator [](size_t index) const { return _list[index]; }

	INLINE T& at(size_t index) { return _list.at(index); }
	INLINE T at(size_t index) const { return _list.at(index); }

	inline void clear() { _list.fill(0); }

	inline size_t *head_ptr() { return _head; }
	INLINE size_t head() const { return *_head; }
	INLINE T * data() { return _list.data(); }
	INLINE size_t capacity() const { return _list.capacity(); }

	friend struct dync_generations_tracker<T>;

private:
	void clear_pointers() { _head = nullptr; _list = list(); }

	size_t *_head;
	list _list;
};

template<typename T, size_t N, std::enable_if_t<std::is_integral_v<T>, bool> = true>
struct sttc_generations_tracker
{
	using list = arr::sttc_list<T, N>;

	sttc_generations_tracker() : _head(0), _list() { _list.fill(0); }
	sttc_generations_tracker(const list& l) : _head(0), _list(l) { }
	// sttc_generations_tracker(const sttc_generations_tracker& other) : _head(other._head), _list(other._list) { }
	// sttc_generations_tracker(sttc_generations_tracker&& other)
	// : _head(other._head), _list(std::move(other._list))
	// {
	// 	MOVE_CONSTRUCTOR_CHECK
	// 	other._head = 0;
	// }

	// sttc_generations_tracker& operator =(const sttc_generations_tracker& other)
	// { _head = other._head; _list = other._list; return *this; }
	// sttc_generations_tracker& operator =(sttc_generations_tracker&& other)
	// {
	// 	MOVE_CHECK
	// 	_head = other._head;
	// 	_list = std::move(other._list);
	// 	other._head = nullptr;
	// 	return *this;
	// }

	T tick(size_t index)
	{
		_list[index]++;
		if (_list[index] == 0) _list[index]++;
		return _list[index];
	}

	inline generations_tracker<T> get() const { return generations_tracker<T>(_list.data(), &_head, _list.get().count_ptr(), _list.capacity()); }

	void clear_gen(size_t index) { _list[index] = 0; }

	INLINE T& operator [](size_t index) { return _list[index]; }
	INLINE T operator [](size_t index) const { return _list[index]; }

	INLINE T& at(size_t index) { return _list.at(index); }
	INLINE T at(size_t index) const { return _list.at(index); }

	inline void clear() { _list.fill(0); }

	INLINE size_t head() const { return _head; }
	INLINE size_t count() const { return _list.count(); }

private:
	size_t _head = 0;
	list _list;
};

template<typename T, std::enable_if_t<std::is_integral_v<T>, bool>>
struct dync_generations_tracker
{
	dync_generations_tracker() : _count{0}, _tracker() { }
	dync_generations_tracker(size_t capacity)
		: _count{capacity} { initialise(capacity); }
	dync_generations_tracker(const arr::list<T>& l)
		: _count{l.count()}, _tracker(l) { }

	dync_generations_tracker(const dync_generations_tracker& other) = delete;
	dync_generations_tracker(dync_generations_tracker&& other) = default;

	~dync_generations_tracker() { Free(); }

	dync_generations_tracker& operator =(const dync_generations_tracker& other) = delete;
	// { _count = other.count(); _tracker = other._tracker; return *this; }
	dync_generations_tracker& operator =(dync_generations_tracker&& other) = default;

	void initialise(size_t capacity)
	{
		Free();

		const size_t kSize = sizeof(size_t) + sizeof(T) * capacity;
		size_t *data = (size_t *)malloc(kSize);
		if (data)
		{
			*data = 0; // head
			_tracker = generations_tracker<T>(RCAST(T*)(data + 1), data, &_count, capacity);
			_tracker.clear();
			return;
		}
		LG_ERR("Failed Allocation of {}B", kSize);
		_tracker = generations_tracker<T>();
	}

	inline generations_tracker<T> get() const { return _tracker; }

	INLINE T tick(size_t index) { return _tracker.tick(index); }
	INLINE void clear_gen(size_t index) { _tracker.clear_gen(index); }

	INLINE T& operator [](size_t index) { return _tracker[index]; }
	INLINE T operator [](size_t index) const { return _tracker[index]; }

	INLINE T& at(size_t index) { return _tracker.at(index); }
	INLINE T at(size_t index) const { return _tracker.at(index); }

	inline void clear() { _tracker.clear(); }

	INLINE size_t head() const { return _tracker.head(); }
	INLINE size_t count() const { return _tracker.capacity(); }

private:
	void Free()
	{
		if (_tracker.data())
		{
			size_t *start = RCAST(size_t *)(_tracker.data()) - 1;
			free(start);
			_tracker.clear_pointers();
		}
	}

	size_t _count;
	generations_tracker<T> _tracker;
};

#endif
};
