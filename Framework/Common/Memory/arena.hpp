#pragma once
#include "core"
#include "debug"

namespace mem {
byte * Align(byte *ptr, u16 alignment);

template<class AllocatorPolicy, class BoundsCheckerPolicy>
struct Arena
{
	using Self = Arena<AllocatorPolicy, BoundsCheckerPolicy>;

	Arena() : _allocator() { }

	template<class AreaPolicy>
	// Pass in the bounds checker sizes as the offset for the pool allocator
	explicit Arena(const AreaPolicy& area)
		: _allocator(area, BoundsCheckerPolicy::SIZE_FRONT, BoundsCheckerPolicy::SIZE_BACK)
	{ }

	Arena(Self&& other)
		: _allocator(other._allocator)
	{
		MOVE_CONSTRUCTOR_CHECK
	}

	Self& operator=(Self&& other)
	{
		MOVE_CHECK
		_allocator = std::move(other._allocator);
		return *this;
	}

	void * Allocate(size_t size, size_t alignment, const char *file = nullptr, size_t line = 0)
	{
		const size_t originalSize = size;
		const size_t newSize = size + BoundsCheckerPolicy::SIZE_FRONT + BoundsCheckerPolicy::SIZE_BACK;

		byte *data = static_cast<byte *>(_allocator.Allocate(newSize, alignment, BoundsCheckerPolicy::SIZE_FRONT));

		_boundsChecker.SetAllocationSize(data, newSize);
		_boundsChecker.GuardFront(data);
		_boundsChecker.GuardBack(data + originalSize + BoundsCheckerPolicy::SIZE_FRONT);

		return data + BoundsCheckerPolicy::SIZE_FRONT;
	}

	void Free(void *ptr, const char *file = nullptr, size_t line = 0)
	{
		if (ptr < _allocator.start() || ptr > _allocator.end())
		{
			LG_ERR("MEMORY Attempted to free pointer {} in arena of range {} to {}",
				reinterpret_cast<uintptr_t>(ptr),
				reinterpret_cast<uintptr_t>(_allocator.start()),
				reinterpret_cast<uintptr_t>(_allocator.end())
				);
			return;
		}

		byte *data = static_cast<byte *>(ptr) - BoundsCheckerPolicy::SIZE_FRONT;

		size_t allocSize = _boundsChecker.GetAllocationSize(data);
		_boundsChecker.CheckFront(data);
		_boundsChecker.CheckBack(data + allocSize - BoundsCheckerPolicy::SIZE_BACK);
		_boundsChecker.RemoveAllocationSize(data);

		_allocator.Free(data);
	}

	inline void Reset() const { _allocator.Reset(); }

	inline void * start() { return _allocator.start(); }

private:
	AllocatorPolicy _allocator;
	BoundsCheckerPolicy _boundsChecker;
};

struct heap_area
{
	explicit heap_area(size_t size, size_t elemSize = 0, size_t alignment = 0)
		: _start(nullptr), _end(nullptr), _size(size),
		  _elemSize(elemSize), _alignment(alignment)
	{
		_start = malloc(size);
		byte *s = static_cast<byte *>(_start);
		_end = s + (size + 1);
	}

	heap_area(void *start, void *end, size_t elemSize = 0, size_t alignment = 0)
		: _start(start), _end(end),
		  _size(static_cast<byte *>(end) - static_cast<byte *>(start)),
		  _elemSize(elemSize), _alignment(alignment)
	{ }

	void *start() const { return _start; }
	void *end() const { return _end; }
	size_t size() const { return _size; }
	size_t elementSize() const { return _elemSize; }
	size_t alignment() const { return _alignment; }

private:
	void *_start;
	void *_end;
	size_t _size;
	size_t _elemSize;
	size_t _alignment;
};

struct stack_area
{
	explicit stack_area(void *begin, void *end, size_t elemSize = 0, size_t alignment = 0)
		: _start(begin), _end(end),
		  _size(static_cast<byte *>(end) - static_cast<byte *>(begin)),
		  _elemSize(elemSize),
		  _alignment(alignment)
	{ }

	void *start() const { return _start; }
	void *end() const { return _end; }
	size_t size() const { return _size; }
	size_t elementSize() const { return _elemSize; }
	size_t alignment() const { return _alignment; }

private:
	void *_start;
	void *_end;
	size_t _size;
	size_t _elemSize;
	size_t _alignment;
};

struct linear_allocator
{
	linear_allocator() : _start{nullptr}, _end{nullptr}, _head{nullptr} { }

	template<class AreaPolicy>
	explicit linear_allocator(const AreaPolicy& area, size_t, size_t)
		: _start(static_cast<byte *>(area.start())),
		  _end(static_cast<byte *>(area.end())),
		  _head(static_cast<byte *>(area.start()))
	{ }

	linear_allocator(void *start, void *end)
		: _start(static_cast<byte *>(start)),
		  _end(static_cast<byte *>(end)),
		  _head(static_cast<byte *>(start))
	{ }

	linear_allocator(linear_allocator&& other)
		: _start(other._start), _end(other._end), _head(other._head)
	{
		other._start = nullptr;
		other._end = nullptr;
		other._head = nullptr;
	}

	linear_allocator&  operator=(linear_allocator&& other)
	{
		MOVE_CHECK
		_start = other._start;
		_end = other._end;
		_head = other._head;

		other._start = nullptr;
		other._end = nullptr;
		other._head = nullptr;
	}

	void * Allocate(size_t size, size_t alignment, size_t offset)
	{
		// Move the data pointer to the start of the alignment
		// then get the pointer to offset
		byte *head = Align(_head + offset, alignment) - offset;

		void *ptr = head;
		head += size;

		if (head > _end)
		{
			LG_ERR("{}B Linear Allocator is out of memory", _end - _start);
			return nullptr;
		}

		_head = head;
		return ptr;
	}

	inline void Free(void *data) { }
	inline void Reset() { _head = _start; }

	inline void * start() { return _start; }
	inline void * end() { return _end; }

private:
	byte *_start;
	byte *_end;
	byte *_head;
};

struct stack_allocator
{
	stack_allocator() : _start{nullptr}, _end{nullptr}, _head{nullptr} { }

	template<class AreaPolicy>
	explicit stack_allocator(const AreaPolicy& area, size_t, size_t)
		: _start(static_cast<byte *>(area.start())),
		  _end(static_cast<byte *>(area.end())),
		  _head(static_cast<byte *>(area.start()))
	{ }

	stack_allocator(void *start, void *end)
		: _start(static_cast<byte *>(start)),
		  _end(static_cast<byte *>(end)),
		  _head(static_cast<byte *>(start))
	{ }

	stack_allocator(stack_allocator&& other)
		: _start(other._start), _end(other._end), _head(other._head)
	{
		other._start = nullptr;
		other._end = nullptr;
		other._head = nullptr;
	}

	stack_allocator&  operator=(stack_allocator&& other)
	{
		MOVE_CHECK
		_start = other._start;
		_end = other._end;
		_head = other._head;

		other._start = nullptr;
		other._end = nullptr;
		other._head = nullptr;
	}

	void * Allocate(size_t size, size_t alignment, size_t offset)
	{
		// Due to offset and alignment some bytes may be skipped
		// I we just set _head to the Free'd pointer we would lose those
		// bytes. So we store the offet ahead of the allocation and
		// an overhead of four bytes is generated
		const size_t kAllocOffsetSize = sizeof(u32);
		static_assert(kAllocOffsetSize == 4, "Allocation offset for stack allocator is not 4 bytes");

		size += kAllocOffsetSize;
		offset += kAllocOffsetSize;
		const u32 allocOffset = _head - _start;

		byte *head = Align(_head + offset, alignment) - offset;
		if (head + size > _end)
		{
			LG_ERR("{}B Stack Allocator is out of memory", _start - _end);
			return nullptr;
		}

		union { byte *as_byte; u32 *as_int; void *as_void; };
		as_byte = head;
		*as_int = allocOffset;
		as_int++;

		_head = head + size;
		return as_void;
	}

	void Free(void *ptr)
	{
		union { void *as_void; u32 *as_int; };
		as_void = ptr;
		as_int--;
		_head = _start + *as_int;
	}

	inline void Reset() { _head = _start; }

	inline void * start() { return _start; }
	inline void * end() { return _end; }

private:
	byte *_start;
	byte *_end;
	byte *_head;
};

// Any offset must be in elementSize
struct free_list
{
	free_list() : next(nullptr) { }

	free_list(void *start, void *end, size_t elementSize, size_t alignment)
		: next(nullptr)
	{
		Construct(start, end, elementSize, alignment);
	}

	void Construct(void *start, void *end, size_t elementSize, size_t alignment)
	{
		byte *s = static_cast<byte *>(start);
		s = Align(s, alignment);
		byte *e = static_cast<byte *>(end);
		u64 numElements = (e - s) / elementSize;

		union { byte *as_byte; free_list *as_list; };
		as_byte = s;
		// We point to the first slot
		next = as_list;

		free_list *runner = as_list;
		as_byte += elementSize;
		for (size_t i = 0; i < numElements - 1; i++)
		{
			runner->next = as_list;
			runner = as_list;
			as_byte = Align(as_byte + elementSize, alignment);
		}
		runner->next = nullptr;
	}

	void * Obtain()
	{
		if (next == nullptr)
			return nullptr;

		free_list *head = next;
		next = head->next;
		return head;
	}

	void Return(void *slot)
	{
		free_list *head = static_cast<free_list *>(slot);
		head->next = next;
		next = head;
	}

	free_list *next;
};

struct pool_allocator
{
	pool_allocator() : _list(), _start{nullptr}, _end{nullptr} {}

	template<class AreaPolicy>
	explicit pool_allocator(const AreaPolicy& area, size_t offsetFront, size_t offsetBack)
		: pool_allocator(area.start(), area.end(),
		                 area.elementSize(), area.alignment(), offsetFront, offsetBack)
		{ }

	pool_allocator(void *start, void *end, size_t elementSize, size_t alignment, size_t offsetFront = 0, size_t offsetBack = 0)
		: _list(start, end, elementSize + offsetFront + offsetBack, alignment),
		  _start(static_cast<byte *>(start)),
		  _end(static_cast<byte *>(end)),
		  _elemSize(elementSize + offsetFront + offsetBack),
		  _alignment(alignment),
		  _offsetFront(offsetFront), _offsetBack(offsetBack)
	{ }

	pool_allocator(pool_allocator&& other)
		: _list(other._list), _start(other._start), _end(other._end)
#ifdef ENABLE_ASSERTS
		, _elemSize(other._elemSize), _alignment(other._alignment),
		  _offsetFront(other._offsetFront), _offsetBack(other._offsetBack)
#endif
	{
		other._list = free_list();
		other._start = nullptr;
		other._end = nullptr;
#ifdef ENABLE_ASSERTS
		other._elemSize = 0;
		other._alignment = 0;
		other._offsetFront = 0;
		other._offsetBack = 0;
#endif
	}

	pool_allocator&  operator=(pool_allocator&& other)
	{
		MOVE_CHECK
		_list = other._list;
		_start = other._start;
		_end = other._end;
		other._list = free_list();
		other._start = nullptr;
		other._end = nullptr;
#ifdef ENABLE_ASSERTS
		_elemSize = other._elemSize;
		_alignment = other._alignment;
		_offsetFront = other._offsetFront;
		_offsetBack = other._offsetBack;
		other._elemSize = 0;
		other._alignment = 0;
		other._offsetFront = 0;
		other._offsetBack = 0;
#endif
	}

	void * Allocate(size_t size, size_t alignment, size_t offset)
	{
#ifdef ENABLE_ASSERTS
		if (size > _elemSize)
		{
			LG_ERR("Attempted to allocate object of size {} and offset {} from pool of {}", size, offset, _elemSize);
			return nullptr;
		}
		if (alignment > _alignment)
		{
			LG_ERR("Attempted to allocate to alignment of {} from pool of {}", alignment, _alignment);
			return nullptr;
		}
		if (offset > _offsetFront)
		{
			LG_ERR("Attempted to allocate to offset of {} from pool of {}", offset, _offsetFront);
			return nullptr;
		}
#endif

		// So long as the alignment is at or below the initial alignment
		// this will be algned;
		return _list.Obtain();
	}

	inline void Free(void *ptr)
	{
#if ENABLE_ASSERTS
		uintptr_t diff = reinterpret_cast<uintptr_t>(ptr) - reinterpret_cast<uintptr_t>(_start);
		if (diff % _elemSize != 0)
		{
			LG_ERR("Pointer freed in pool allocator does not have the correct remainder {}", diff % _elemSize);
			return;
		}
#endif
		_list.Return(ptr);
	}

	inline void Reset()
	{
		_list.Construct(_start, _end, _elemSize + _offsetFront + _offsetBack, _alignment);
	}

	inline void * start() const { return _start; }
	inline void * end() const { return _end; }

private:
	free_list _list;
	byte *_start;
	byte *_end;
#ifdef ENABLE_ASSERTS
	size_t _elemSize;
	size_t _alignment;
	size_t _offsetFront;
	size_t _offsetBack;
#endif
};

struct no_bounds_checking
{
	const static size_t SIZE_FRONT = 0;
	const static size_t SIZE_BACK  = 0;

	inline void GuardFront(void *ptr) const { }
	inline void GuardBack(void *ptr) const { }

	inline void CheckFront(void *ptr) const { }
	inline void CheckBack(void *ptr) const { }

	inline void SetAllocationSize(void *ptr, size_t size) { }
	inline size_t GetAllocationSize(void *ptr) { return 0; }
	inline void RemoveAllocationSize(void *ptr) {  }
};

struct simple_bounds_checking
{
	using GUARD = u16;
	const GUARD kFront = 0xDEAD;
	const GUARD kBack  = 0xBEEF;

	const static size_t SIZE_FRONT = sizeof(GUARD);
	const static size_t SIZE_BACK  = sizeof(GUARD);

	inline void GuardFront(void *ptr) const
	{
		union { void *as_void; GUARD *as_guard; };
		as_void = ptr;
		*as_guard = kFront;
	}
	inline void GuardBack(void *ptr) const
	{
		union { void *as_void; GUARD *as_guard; };
		as_void = ptr;
		*as_guard = kBack;
	}

	inline void CheckFront(void *ptr) const
	{
		union { void *as_void; GUARD *as_guard; };
		as_void = ptr;
		if (*as_guard != kFront)
		{
			LG_ERR("Memory underflow at {}", reinterpret_cast<uintptr_t>(ptr));
		}
	}
	inline void CheckBack(void *ptr) const
	{
		union { void *as_void; GUARD *as_guard; };
		as_void = ptr;
		if (*as_guard != kBack)
		{
			LG_ERR("Memory overflow at {}", reinterpret_cast<uintptr_t>(ptr));
		}
	}

	inline void SetAllocationSize(void *ptr, size_t size) { _allocSizes[ptr] = size; }
	inline size_t GetAllocationSize(void *ptr) { return _allocSizes[ptr]; }
	inline void RemoveAllocationSize(void *ptr) { _allocSizes.erase(_allocSizes.find(ptr)); }

	std::map<void *, size_t> _allocSizes;
};
};
