// Credit: https://blog.molecular-matters.com/2011/07/05/memory-system-part-1/
#pragma once
#include "core"
#include "arena.hpp"
#include "Util/magic.hpp"

namespace mem {

template<typename T, class ARENA>
T * NewArray(ARENA *arena, size_t N, size_t alignment, const char *file, size_t line)
{
	union
	{
		void *as_void;
		size_t *as_size_t;
		T *as_T;
	};

	as_void = arena->Allocate(sizeof(size_t) + sizeof(T) * N, alignment, file, line);
	*as_size_t = N;
	as_T++;

if constexpr (std::is_pod_v<T>) {
	return as_T;
} else {
	const T *const end = as_T + N;
	while (as_T < end)
		new (as_T++) T;
	
	return (as_T - N);
}
}

template<typename T, class ARENA>
void Delete(ARENA *arena, T *obj, const char *file, size_t line)
{
if constexpr (! std::is_pod_v<T> && !std::is_same_v<T, void>) {
	obj->~T();
}
	arena->Free(obj, file, line);
}

template<typename T, class ARENA>
void DeleteArray(ARENA *arena, T *ptr, const char *file, size_t line)
{
	union
	{
		size_t *as_size_t;
		T *as_T;
	};

	as_T = ptr;
	as_size_t--;

if constexpr (! std::is_pod_v<T>) {
	for (size_t i = *as_size_t; i > 0; i--)
		as_T[i - 1].~T();
}
	arena->Free(as_size_t, file, line);
}

void * StackAllocate(size_t);
void StackFree(void *);
};

#define NEW(ARENA, TYPE, ALIGNMENT) \
                         new (ARENA->Allocate(sizeof(TYPE), ALIGNMENT, __FILE__, __LINE__)) TYPE
#define SNEW(ARENA, SIZE, ALIGNMENT) \
                         ARENA->Allocate(SIZE, ALIGNMENT, __FILE__, __LINE__)
#define DEL(ARENA, OBJ) ::mem::Delete(ARENA, OBJ, __FILE__, __LINE__)

#define NEW_ARR(ARENA, ARR, ALIGNMENT) \
	::mem::NewArray<type_and_count<ARR>::TYPE>(ARENA, type_and_count<ARR>::COUNT, ALIGNMENT, __FILE__, __LINE__)
#define NEW_ARRC(ARENA, TYPE, COUNT, ALIGNMENT) \
	::mem::NewArray<TYPE>(ARENA, COUNT, ALIGNMENT, __FILE__, __LINE__)
#define DEL_ARR(ARENA, ARR) ::mem::DeleteArray(ARENA, ARR, __FILE__, __LINE__)
