#pragma once

#include "core"

template<typename OStream>
inline OStream& operator<<(OStream& os, glm::vec2& v2)
{
	return os << "(" << v2.x << ", " << v2.y << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, const glm::vec2& v2)
{
	return os << "(" << v2.x << ", " << v2.y << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, glm::vec3& v3)
{
	return os << "(" << v3.x << ", " << v3.y << ", " << v3.z << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, const glm::vec3& v3)
{
	return os << "(" << v3.x << ", " << v3.y << ", " << v3.z << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, glm::vec4& v4)
{
	return os << "(" << v4.x << ", " << v4.y << ", " << v4.z << ", " << v4.w << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, const glm::vec4& v4)
{
	return os << "(" << v4.x << ", " << v4.y << ", " << v4.z << ", " << v4.w << ")";
}

namespace math {
struct vec2
{
	union {
		struct { f32 x, y; };
		struct { f32 width, height; };
	};

	vec2 & operator *=(f32 f)
	{
		x *= f;
		y *= f;
		return *this;
	}

	vec2 & operator *=(const vec2 &v2)
	{
		x *= v2.x;
		y *= v2.y;
		return *this;
	}

	vec2 & operator +=(const vec2 &v2)
	{
		x += v2.x;
		y += v2.y;
		return *this;
	}
};

struct vec3
{
	union {
		struct { f32 x, y, z; };
		struct { f32 r, g, b; };
	};
};

struct vec4
{
	union {
		struct { f32 x, y, z, w; };
		struct { f32 r, g, b, a; };
	};
};

struct uvec2
{
	union {
		struct { u32 x, y; };
		struct { u32 width, height; };
	};

	static uvec2 zero;
};

struct rect
{
	union {
		struct { f32 xMin, yMin, xMax, yMax; };
		struct { vec2 min, max; };
	};

	rect() : xMin(0), yMin(0), xMax(1), yMax(1) { }
	rect(math::vec2 min, math::vec2 max) : min(min), max(max) { }
	rect(f32 xMin, f32 yMin, f32 xMax, f32 yMax)
		: xMin(xMin), yMin(yMin), xMax(xMax), yMax(yMax) { }
};

// TODO Constexpr version, arr::list version

template<typename T>
T max(std::initializer_list<T> list)
{
	if (list.size() == 0)
		return T();

	T max = *list.begin();
	for (auto it = list.begin() + 1; it < list.end(); it++)
	{
		if (*it > max)
			max = *it;
	}
	return max;
}

template<typename T>
T min(std::initializer_list<T> list)
{
	if (list.size() == 0)
		return T();

	T min = *list.begin();
	for (auto it = list.begin() + 1; it < list.end(); it++)
	{
		if (*it < min)
			min = *it;
	}
	return min;
}

glm::mat4 CalculateModel2D(const glm::vec2& position, float rotation, const glm::vec2& scale);
};

inline math::vec2 operator *(const math::vec2 &a, const math::vec2 &b)
	{ return { a.x * b.x, a.y * b.y }; }
inline math::vec2 operator *(const math::vec2 &a, f32 b) { return { a.x * b, a.y * b }; }
inline math::vec2 operator *(f32 a, const math::vec2 &b) { return { a * b.x, a * b.y }; }

inline bool operator ==(const math::uvec2& a, const math::uvec2& b)
	{ return a.x == b.x && a.y == b.y; }
inline bool operator !=(const math::uvec2& a, const math::uvec2& b) { return !(a == b); }

template<typename OStream>
inline OStream& operator<<(OStream& os, const math::vec2& v2)
{
	return os << "(" << v2.x << ", " << v2.y << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, const math::uvec2& v2)
{
	return os << "(" << v2.x << ", " << v2.y << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, const math::vec3& v3)
{
	return os << "(" << v3.x << ", " << v3.y << ", " << v3.z << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, const math::vec4& v4)
{
	return os << "(" << v4.x << ", " << v4.y << ", " << v4.z << ", " << v4.w << ")";
}

template<typename OStream>
inline OStream& operator<<(OStream& os, const math::rect& r)
{
	return os << "[" << r.min << " " << r.max << "]";
}
