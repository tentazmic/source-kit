#pragma once

#include "core"
#include "math"
#include "codes.hpp"

namespace inp {

f32 ApplyDeadzone(f32 input, f32 deadzone);

ENUM EState
{
	const static EState DOWN;
	const static EState HELD;
	const static EState UP;
	const static EState RELEASED;

	u8 value;

	void Update(bool pressed);
	inline void forward() { value = (value + 1) % 4; }
	inline bool pressed() { return value < UP.value; }
};

inline constexpr const EState EState::DOWN     { 0 };
inline constexpr const EState EState::HELD     { 1 };
inline constexpr const EState EState::UP       { 2 };
inline constexpr const EState EState::RELEASED { 3 };

struct keyboard
{
	keyboard() { }
	keyboard(std::initializer_list<EKey> list)
		{ for (auto it : list) RegisterKey(it); }

	void RegisterKey(EKey);
	void UnregisterKey(EKey);

	std::map<EKey, EState, key_less> keys;
};

struct mouse
{
	mouse() { }
	mouse(std::initializer_list<EMouseButton> list)
		{ for (auto it : list) RegisterButton(it); }

	void RegisterButton(EMouseButton);
	void UnregisterButton(EMouseButton);

	glm::vec2 position;
	std::map<EMouseButton, EState, mbutton_less> buttons;
};

struct gamepad
{
	u8 ID = 255;
	glm::vec2 control;
	EState A;
	EState start;

	inline bool active() const { return ID != 255; }
	inline void clear()
	{
		ID = 255;
		control = glm::vec2();
		A.value = 0;
		start.value = 0;
	}
};

#ifndef BLD_INSTALL
void KeyboardUpdate(keyboard&); // defined in the platform layer
void MouseUpdate(mouse&); // defined in the platform layer
bool GamepadUpdate(gamepad&); // defined in the platform layer
#endif
};

inline bool operator ==(const inp::EState a, const inp::EState b)
	{ return a.value == b.value; }
inline bool operator !=(const inp::EState a, const inp::EState b)
	{ return !(a == b); }

#if defined(ENABLE_LOG) || defined(ENABLE_DEBUGGER)
	#include "dbg/input.ipp"
#endif
