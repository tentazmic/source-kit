#pragma once

#include "math"
#include "Graphics/frame_buffer.hpp"
#include "Graphics/mesh_buffer.hpp"
#include "Graphics/shader.hpp"
#include "Graphics/texture.hpp"

namespace game {
struct sprite
{
	glm::vec2 position;
	glm::vec2 scale{ 1.f, 1.f };
	f32 rotation;
	bool moveable;
	gfx::hdl_texture texture = mem::make_dead<gfx::hdl_texture>();
	glm::vec4 colour{ 1.0f, 1.0f, 1.0f, 1.0f };
	f32 tilingFactor = 1.0f;
	u32 parent = MAX(u32);
};

struct camera
{
	glm::vec2 position;
	f32 aspectRatio;
	glm::mat4 projection;

	f32 zoom;
	f32 moveSpeed;
};
};
