#include "pch.h"
#include "debug"

#include "events.hpp"

namespace evt {

u8 GetCategory(Type t)
{
	switch (t)
	{
		case Type::WINDOW_CLOSE: return CAT_Application;
		case Type::WINDOW_FOCUS: return CAT_Application;
		case Type::WINDOW_LOST_FOCUS: return CAT_Application;
		case Type::WINDOW_RESIZED: return CAT_Application;
		case Type::WINDOW_MOVED: return CAT_Application;

		case Type::KEY_PRESSED: return CAT_Keyboard | CAT_Input;
		case Type::KEY_RELEASED: return CAT_Keyboard | CAT_Input;
		case Type::KEY_TYPED: return CAT_Keyboard | CAT_Input;

		case Type::MOUSE_MOVED: return CAT_Mouse | CAT_Input;
		case Type::MOUSE_SCROLLED: return CAT_Mouse | CAT_Input;
		case Type::MOUSE_BUTTON_PRESSED: return CAT_MouseButton | CAT_Input;
		case Type::MOUSE_BUTTON_RELEASED: return CAT_MouseButton | CAT_Input;

		case Type::NONE: return CAT_None;
	}
	return CAT_None;
}

#ifdef HOLD_IDS
void event::GetString(char *buffer, size_t length)
{
	switch (type)
	{
		case Type::WINDOW_CLOSE:
		{
			const char *s = "Window Close";
			u32 count = glm::min(length - 1, strlen(s));
			strncpy(buffer, s, count);
			buffer[count] = 0;
			break;
		}
		case Type::WINDOW_FOCUS:
		{
			const char *s = "Window Focus";
			u32 count = glm::min(length - 1, strlen(s));
			strncpy(buffer, s, count);
			buffer[count] = 0;
			break;
		}
		case Type::WINDOW_LOST_FOCUS:
		{
			const char *s = "Window Lost Focus";
			u32 count = glm::min(length - 1, strlen(s));
			strncpy(buffer, s, count);
			buffer[count] = 0;
			break;
		}
		case Type::WINDOW_RESIZED:
		{
			snprintf(
				buffer, length,
				"Window Resized (%d, %d)",
				size.x, size.y
			);
			break;
		}
		case Type::WINDOW_MOVED:
		{
			snprintf(
				buffer, length,
				"Window Moved (%.3f, %.3f)",
				position.x, position.y
			);
			break;
		}

		case Type::KEY_PRESSED:
		{
			snprintf(
				buffer, length,
				"Key Pressed [%s] with %d repeats",
				inp::KeyToString(key), repeatCount
			);
			break;
		}
		case Type::KEY_RELEASED:
		{
			snprintf(
				buffer, length,
				"Key Released [%s]",
				inp::KeyToString(key)
			);
			break;
		}
		case Type::KEY_TYPED:
		{
			snprintf(
				buffer, length,
				"Key Typed [%s]",
				inp::KeyToString(key)
			);
			break;
		}

		case Type::MOUSE_MOVED:
		{
			snprintf(
				buffer, length,
				"Mouse Moved (%.3f, %.3f)",
				position.x, position.y
			);
			break;
		}
		case Type::MOUSE_SCROLLED:
		{
			snprintf(
				buffer, length,
				"Mouse Scrolled (%.3f, %.3f)",
				position.x, position.y
			);
			break;
		}
		case Type::MOUSE_BUTTON_PRESSED:
		{
			snprintf(
				buffer, length,
				"Mouse Button Pressed [%s]",
				inp::MouseButtonToString(button)
			);
			break;
		}
		case Type::MOUSE_BUTTON_RELEASED:
		{
			snprintf(
				buffer, length,
				"Mouse Button Released [%s]",
				inp::MouseButtonToString(button)
			);
			break;
		}

		case Type::NONE:
		{
			const char *s = "Null Event";
			u32 count = glm::min(length - 1, strlen(s));
			strncpy(buffer, s, count);
			buffer[count] = 0;
			break;
		}
	}
}
#endif

event NewWindowClose()
{
	return event{
		false,
		Type::WINDOW_CLOSE
	};
}
event NewWindowFocus()
{
	return event{
		false,
		Type::WINDOW_FOCUS
	};
}
event NewWindowLostFocus()
{
	return event{
		false,
		Type::WINDOW_LOST_FOCUS
	};
}
event NewWindowResized(glm::uvec2 size)
{
	return event{
		false,
		Type::WINDOW_RESIZED,
		.size = { size.x, size.y }
	};
}
event NewWindowMoved(glm::vec2 position)
{
	return event{
		false,
		Type::WINDOW_MOVED,
		.position = { position.x, position.y }
	};
}

event NewKeyPressed(inp::EKey key, i32 repeatCount)
{
	return event{
		false,
		Type::KEY_PRESSED,
		.key = key,
		.repeatCount = repeatCount
	};
}
event NewKeyReleased(inp::EKey key)
{
	return event{
		false,
		Type::KEY_RELEASED,
		.key = key
	};
}
event NewKeyTyped(inp::EKey key)
{
	return event{
		false,
		Type::KEY_TYPED,
		.key = key
	};
}

event NewMouseMoved(glm::vec2 position)
{
	return event{
		false,
		Type::MOUSE_MOVED,
		.position = { position.x, position.y }
	};
}
event NewMouseScrolled(glm::vec2 offset)
{
	return event{
		false,
		Type::MOUSE_SCROLLED,
		.offset = { offset.x, offset.y }
	};
}
event NewMouseButtonPressed(inp::EMouseButton button)
{
	return event{
		false,
		Type::MOUSE_BUTTON_PRESSED,
		.button = button
	};
}
event NewMouseButtonReleased(inp::EMouseButton button)
{
	return event{
		false,
		Type::MOUSE_BUTTON_RELEASED,
		.button = button
	};
}

#define CASE(TYPE, FUNC) \
	case ::evt::Type:: TYPE : { ::evt::Invoke(On##FUNC, *evt); break; }

void dispatcher::Dispatch()
{
	FOR (evt, _events)
	{
		switch (evt->type)
		{
		CASE(WINDOW_CLOSE, WindowClose)
		CASE(WINDOW_FOCUS, WindowFocus)
		CASE(WINDOW_LOST_FOCUS, WindowLostFocus)
		CASE(WINDOW_RESIZED, WindowResized)
		CASE(WINDOW_MOVED, WindowMoved)

		CASE(KEY_PRESSED, KeyPressed)
		CASE(KEY_RELEASED, KeyReleased)
		CASE(KEY_TYPED, KeyTyped)

		CASE(MOUSE_MOVED, MouseMoved)
		CASE(MOUSE_SCROLLED, MouseScrolled)
		CASE(MOUSE_BUTTON_PRESSED, MouseButtonPressed)
		CASE(MOUSE_BUTTON_RELEASED, MouseButtonReleased)
		case ::evt::Type::NONE:
			LG_WRN("None Event");
			break;
		}

		::evt::Invoke(OnEvent, *evt);
	}

	_events.clear();
}

void Invoke(const clbk::sink<void (event&)>& snk, event& evt)
{
	RCFOR (del, snk)
	{
		del->Invoke(evt);
		if (evt.handled)
			break;
	}
}

};
