#pragma once
#include "core"
#include "math"

#include "Input/codes.hpp"
#include "Util/list.hpp"
#include "Util/callback.hpp"

namespace evt {

enum struct Type
{
	NONE = 0,
	WINDOW_CLOSE, WINDOW_RESIZED, WINDOW_FOCUS, WINDOW_LOST_FOCUS, WINDOW_MOVED,
	// APP_TICK, APP_UPDATE, APP_RENDER,
	KEY_PRESSED, KEY_RELEASED, KEY_TYPED,
	MOUSE_BUTTON_PRESSED, MOUSE_BUTTON_RELEASED, MOUSE_MOVED, MOUSE_SCROLLED
};

enum Category
{
	CAT_None          = 0,
	CAT_Application   = BIT(0),
	CAT_Input         = BIT(1),
	CAT_Keyboard      = BIT(2),
	CAT_Mouse         = BIT(3),
	CAT_MouseButton   = BIT(4)
};

// Category has been moved out of the event as
// it is tightly coupled with the type anyway
u8 GetCategory(Type);

struct event
{
	bool handled;
	Type type;

	union
	{
		math::uvec2 size;
		math::vec2 position, offset;
		struct
		{
			inp::EKey key;
			inp::EMouseButton button;
			i32 repeatCount;
		};
	};

	inline bool IsInCategory(Category cat) const { return GetCategory(type) & cat; }

#ifdef HOLD_IDS
	void GetString(char *, size_t);
#endif
};

event NewWindowClose();
event NewWindowFocus();
event NewWindowLostFocus();
event NewWindowResized(glm::uvec2 size);
event NewWindowMoved(glm::vec2 position);

event NewKeyPressed(inp::EKey, i32 repeatCount);
event NewKeyReleased(inp::EKey);
event NewKeyTyped(inp::EKey);

event NewMouseMoved(glm::vec2 position);
event NewMouseScrolled(glm::vec2 offset);
event NewMouseButtonPressed(inp::EMouseButton);
event NewMouseButtonReleased(inp::EMouseButton);

using signature = ::clbk::types<void (event&)>;

struct dispatcher
{
	dispatcher(u32 length)
		: _events(length)
	{ }

	inline void SubmitEvent(event e) { _events.push(e); }
	void Dispatch();

	signature::snk
		OnEvent,
		OnWindowClose, OnWindowFocus, OnWindowLostFocus, OnWindowResized, OnWindowMoved,
		OnKeyPressed, OnKeyReleased, OnKeyTyped,
		OnMouseMoved, OnMouseScrolled, OnMouseButtonPressed, OnMouseButtonReleased;

private:
	arr::dync_list<event> _events;
};

void Invoke(const clbk::sink<void (event&)>& snk, event& evt);
};
