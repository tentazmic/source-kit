#!/usr/bin/python
import os
import sys

from reflection.ppprocess import ppp_files_only

# TODO Some sort of string to int conversion

__USAGE = f"""\
python {sys.argv[0]} <TEMPLATE> <DESTINATION> [CONTEXT PAIRS]\

--- CONTEXT PAIRS ---
Either enter a pair like so 'foo=bar' or as two arguments 'foo bar'
"""

def resolve_template(path: str) -> str:
    dirname = os.path.dirname(__file__)
    templates = os.path.join(os.path.abspath(dirname), '../1Templates')
    return os.path.join(templates, path)

def execute(template_path: str, output_path: str, context = dict()):
    source = resolve_template(template_path)
    ppp_files_only(context, source, output_path)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print(__USAGE)
        exit(1)

    args = sys.argv[1:]
    template = args.pop(0)
    output = args.pop(0)
    context = { 'no-lines': 0 }
    key = None
    while len(args) > 0:
        arg = args.pop(0)
        if key != None:
            context[key] = arg
            key = None
        elif '=' in arg:
            k, v, *_ = arg.split('=')
            context[k] = v
        else:
            key = arg

    if key != None:
        print(f'ERROR Key {key} has no value')
        exit(1)

    print(context)
    execute(template, output, context)

