import sys
import os
import glob

import util.settings as stts

'''
Classes that describe the requirements for the different
build units of the code base (e.g. the Modules, Platforms, etc.).

The fields of the classes are particular and used by the builder
to resolve the requirements for the section.

name              The name of this section
folders           The folders that contain the code for this section
install_folders   The folders that contain the code for this section
                    under Installs/0Source
include           The include folder for the compiler inputs
macros            Macros to be used
install_macros    Macros to be used when building an install
lib_name          The library to add to the compiler inputs
libraries         Libraries to link during linking
options           Compiler Options
dependencies      Modules to also build
externals         Externals to include
'''

class External:
	def __init__(self, name: str, lib_names: list,
	             include: str, macros: list, libraries: list):
		self.name = stts.DIR_EXTERNALS + name
		self.lib_names = lib_names
		if include == 'l:':
			self.includes = [ f'{stts.DIR_LIBS}/include_{name}/*' ]
		elif include.startswith('l:'):
			query = include[2:]
			self.includes = glob.glob(f'{stts.DIR_LIBS}/include_{name}/*')
		else:
			self.includes = [ f'{self.name}/{include}' ]
		self.macros = macros
		self.libraries = libraries

# For stuff under Framework/Platform
class Platform:
	def __init__(self, name: str, macros: list, install_macros: list,
	             shared: list, externals: list):
		self.name = name
		self.shared = [ stts.DIR_PLTSHARED + s for s in shared ]
		self.folders = [ stts.DIR_PLATFORM + name ] + self.shared
		self.install_folders = [ stts.DIR_INSTL_PLATFORM + name ] + self.shared
		self.macros = macros
		self.install_macros = macros + install_macros
		self.externals = externals

# For stuff under Framework/Module
class Module:
	def __init__(self, name: str, macros: list, dependencies: list, externals: list):
		self.name = name
		self.folders = [ stts.DIR_MODULES + name ]
		self.macros = macros
		self.dependencies = dependencies
		self.externals = externals

class GraphicsAPI:
	def __init__(self, name: str, macros: list, externals: list):
		self.name = name
		self.folders = [ stts.DIR_GFXAPI + name ]
		self.macros = macros
		self.externals = externals

# Debug, Develop, Release
class Profile:
	def __init__(self, name: str, macros: list, options: list, debug: bool):
		self.name = name
		self.macros = macros
		self.options = options
		self.debug = debug

# Not sure if I need memory mapping
# E_CPP_MMF = External

E_GLAD = External('Glad', [ 'glad' ], 'include', [], [])

if sys.platform == 'win32':
	E_GLFW = External(
		'GLFW', [ 'glfw3' ], 'include',
		[ 'GLFW_INCLUDE_NONE' ],
		[ 'gdi32' ]
	)
elif sys.platform == 'linux' or sys.platform == 'linux2':
	E_GLFW = External(
		'GLFW', [ 'glfw3' ], 'include',
		[ 'GLFW_INCLUDE_NONE' ],
		[ 'X11', 'Xrandr', 'pthread', 'Xi', 'dl', 'Xinerama', 'Xcursor' ]
	)

match sys.platform:
	case 'win32':
		E_GLFW = External(
			'GLFW', [ 'glfw3' ], 'include',
			[ 'GLFW_INCLUDE_NONE' ],
			[ 'gdi32' ]
		)
		E_MSDF =\
		  External('msdf_atlas_gen', [ 'msdfgen-ext', 'msdf-atlas-gen', 'msdfgen-core' ],
		           'l:*', [], [ 'png', 'freetype' ])
	case 'linux' | 'linux2':
		E_GLFW = External(
			'GLFW', [ 'glfw3' ], 'include',
			[ 'GLFW_INCLUDE_NONE' ],
			[ 'X11', 'Xrandr', 'pthread', 'Xi', 'dl', 'Xinerama', 'Xcursor' ]
		)
		E_MSDF =\
		  External('msdf_atlas_gen', [ 'msdf-atlas-gen', 'msdfgen-core', 'msdfgen-ext' ],
		           'l:*', [],
		           [ 'freetype', 'png', 'z', 'bz2', 'skia',
		             'brotlidec', 'brotlicommon' ])



E_GLM = External('glm', [ '' ], '', [], [])

E_IMGUI = External('imgui', [ 'imgui' ], '', [], [])

E_SPDLOG = External('spdlog', [ 'spdlog' ], 'include', [ 'SPDLOG_COMPILED_LIB' ], [])

E_STB = External('stb', [ 'stb' ], '', [], [])

E_SOUNDIO = External('libsoundio', [ 'soundio' ], '', [], [ 'asound' ])

ALL_EXTERNALS = [
	E_GLAD, E_GLFW, E_GLM, E_IMGUI, E_SPDLOG, E_STB, E_MSDF, E_SOUNDIO
]

P_WINDOWS = Platform(
	'Windows',
	[ 'PLT_WIN' ], [ 'INSTALL_API=__declspec(dllexport)' ],
	[ 'GLFW' ], [ E_GLFW ]
)

P_LINUX = Platform(
	'Linux',
	[ 'PLT_LIN' ], [ '"INSTALL_API=__attribute__((visibility(\\"default\\")))"'],
	[ 'GLFW' ], [ E_GLFW, E_SOUNDIO ]
)

# These two are special

COMMON = Module(
	'Common', [ 'GLEW_STATIC' ], [],
	[ E_STB, E_GLM ]
)
COMMON.folders = [ stts.DIR_COMMON, stts.DIR_COMMON + '_Includes' ]

DEBUG = Module('Debug', [], [], [ E_SPDLOG ])
DEBUG.folders = [ stts.DIR_DEBUG ]

def find_modules() -> list:
	modules = [ read_module_file(f) for f in glob.glob(stts.DIR_MODULES + '/**/module') ]
	config = stts.read_project_config()
	for m in modules:
		m.macros = [ macro.format(**config) for macro in m.macros ]

		dep_names = m.dependencies
		m.dependencies = []
		for name in dep_names:
			dep = next(filter(lambda m: m.name == name, modules), None)
			if dep != None:
				m.dependencies.append(dep)

	return modules

def external_filter_func(ext_name: str) -> bool:
	global ALL_EXTERNALS
	for e in ALL_EXTERNALS:
		if e.name == stts.DIR_EXTERNALS + ext_name:
			return True

	print('Unknown external ' + ext_name)
	return False

def read_module_file(location: str) -> Module:
	global ALL_EXTERNALS
	module = Module('', [], [], [])
	module.folders = [ os.path.dirname(location) + '/src' ]

	with open(location, 'r') as conf:
		for l in conf:
			k, v = l.split(':')
			match k:
				case 'name':
					module.name = v.strip()
				case 'macros':
					module.macros = [ i.strip() for i in v.split() ]
				case 'externals':
					exts = [ i.strip() for i in v.split() ]
					for name in exts:
						for e in ALL_EXTERNALS:
							if e.name == stts.DIR_EXTERNALS + name:
								module.externals.append(e)
				case 'dependencies':
					module.dependencies = [ i.strip() for i in v.split() ]
	return module

MODULES = find_modules()

def get_modules(names: list) -> list:
	global MODULES
	modules = []
	for n in names:
		m = next(filter(lambda m: m.name == n, MODULES), None)
		if m != None:
			modules.append(m)
	return modules

# These two are here as they need to be referenced
# by name in the build scripts
M_RENDERER = next(filter(lambda m: m.name == 'renderer', MODULES), None)
M_INSTALLS = next(filter(lambda m: m.name == 'installs', MODULES), None)

G_OPENGL = GraphicsAPI(
	'OpenGL', [ 'GAPI_OPENGL' ], [ E_GLAD ]
)

PR_DEBUG = Profile(
	'Debug',
	[
		'ENABLE_LOG', 'ENABLE_ASSERTS', 'ENABLE_PROFILING', 'ENABLE_DEBUGGER',
		'HOLD_IDS', 'DEV_WINDOW', 'SPDLOG_ACTIVE_LEVEL=0', 'GLIBCXX_DEBUG'
	],
	[ '-ggdb', '-Wall' ],
	debug=True
)

PR_DEVELOP = Profile(
	'Develop',
	[ 'ENABLE_LOG', 'SPDLOG_ACTIVE_LEVEL=3', 'ENABLE_PROFILING' ],
	[ '-ggdb', '-Wall', '-O2' ],
	debug=True
)

PR_RELEASE = Profile(
	'Release',
	[ ],
	[ '-fno-exceptions', '-O3' ],
	debug=False
)

# Settings passed in in anticipation of cross platform
# builds
def get_platform(settings: stts.BuildSettings) -> Platform:
	match sys.platform:
		case 'win32':
			return P_WINDOWS
		case 'linux' | 'linux2':
			return P_LINUX
