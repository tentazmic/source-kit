import sys
import os
import re

DIR_BUILD = '00Build/'
DIR_EXEC = '00Exec/'
DIR_LIBS = '00Libs/'
DIR_SOURCE = '00Source/'
DIR_UTILS = '00Utils/'

DIR_TOOLS = '0Tools/'
DIR_EXTERNALS = '2Externals/'
DIR_TESTS = '4Tests/'

DIR_COMMON = 'Framework/Common/'
DIR_MODULES = 'Framework/Modules/'
DIR_GFXAPI = 'Framework/GraphicsAPI/'
DIR_PLATFORM = 'Framework/Platform/'
DIR_PLTSHARED = 'Framework/Platform/Shared/'

DIR_DEBUG = 'Framework/Debug/'

DIR_ASSETS = 'Assets/'

DIR_INSTALLS = 'Installs/'
DIR_INSTL_SRC = DIR_INSTALLS + '0Source/'
DIR_INSTL_PLATFORM = DIR_INSTL_SRC + 'Platform/'

DIR_INSTALLS_DEST = DIR_EXEC + 'Installs/'

CHIME_ALWAYS = 0
CHIME_FAIL = 1
CHIME_NEVER = 2

class BuildSettings:
	def __init__(self,
	      app_name='Install Framework', build_name='build',
	      target_exe=False, exe_name='out.exe',
				verbosity_level=0, use_timing=False,
				num_processes=10, use_clean_slate=False,
				run_result=False, build_chime=CHIME_ALWAYS):
		self.app_name = app_name
		self.build_name = build_name
		self.target_exe = target_exe
		self.exe_name = exe_name

		self.verbosity_level = verbosity_level
		self.use_timing = use_timing
		self.num_processes = num_processes
		self.use_clean_slate = use_clean_slate
		self.run_result = run_result
		self.build_chime = build_chime

	def copy(self):
		return BuildSettings(
			app_name=self.app_name, build_name=self.build_name,
			target_exe=self.target_exe, exe_name=self.exe_name,
			verbosity_level=self.verbosity_level, use_timing=self.use_timing,
			num_processes=self.num_processes, use_clean_slate=self.use_clean_slate,
			run_result=self.run_result, build_chime=self.build_chime
		)

_HELP_DIALOGUE = f"""\
{sys.argv[0]} [OPTIONS]

-h | ?          Display this help dialogue
-t              Time the execution of this script
-v              Sets a low verbosity level
-vvv            Sets a high verbosity level
-p [int]        Sets the number of precesses to use
-c              Rebuilds everything
-r              Where appropriate, runs the result of the build
--chime         Build chimes on success and failure
--chime-fail    Build only chimes on failure
--chime-never   Build never chimes
"""

def get_ppprocessed_include(include: str) -> str:
	if include == 'pch.h':
		return include

	split = include.split('/')
	replacement = '/'.join(split[:-1])
	if len(split) > 1:
		replacement += '/'
	return replacement + 'p_' + split[-1]

def get_settings(settings=BuildSettings()) -> BuildSettings:
	if len(sys.argv) <= 1:
		return settings

	args = sys.argv[1:]
	while len(args) > 0:
		arg = args.pop(0)

		if arg == '-h' or arg == '?':
			print(_HELP_DIALOGUE)
			exit(0)

		match arg:
			case '-h' | '?':
				print(_HELP_DIALOGUE)
				exit(0)
			case '-t':
				settings.use_timing = True
			case '-v':
				settings.verbosity_level = 1
			case '-vvv':
				settings.verbosity_level = 2
			case '-p':
				n = int(args.pop(0))
				settings.num_processes = n
			case '-c':
				settings.use_clean_slate = True
			case '-r':
				settings.run_result = True
			case '--chime':
				settings.build_chime = CHIME_ALWAYS
			case '--chime-fail':
				settings.build_chime = CHIME_FAIL
			case '--chime-never':
				settings.build_chime = CHIME_NEVER
			case _:
				print(f'Command {arg} not recognised')
				exit(1)

	return settings

def read_project_config() -> dict:
	d = dict()
	with open('project.install', 'r') as config:
		for line in config:
			pivot = line.find('=')
			name = line[:pivot].strip()
			var = line[pivot + 1:].strip()
			d[name] = var

	return d
