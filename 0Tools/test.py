#!/usr/bin/python
import re
import glob
import os

import build.compiler as cmpl
import reflection.ppprocess as pppr
import reflection.reflection as refl
import build.build_unit as bdut
import util.settings as stts

RGX_TEST_FUNC = re.compile(r'(__[0-9]+_[A-Za-z0-9_]+)')

def get_test_funcs(ppp_file: str) -> list:
	test_funcs = []
	with open(ppp_file, 'r') as f:
		funcs = RGX_TEST_FUNC.findall(f.read())
		test_funcs.extend(funcs)

	return test_funcs

def execute(settings: stts.BuildSettings):
	components = []
	for f in glob.glob('Framework/Common/**/*.cmp', recursive=True):
		with open(f, 'rb') as input:
			comps = refl.read_components(input)
		components += comps

	context = { 'COMPONENTS': components, 'root_folder': 'Framework/Common' }

	settings.target_exe=True
	if settings.build_chime == stts.CHIME_ALWAYS:
		settings.build_chime = stts.CHIME_FAIL

	cinputs = cmpl.CompilerInputs()
	cinputs, all_objs, statuses, returncode =\
		cmpl.compile_common(settings, bdut.PR_DEBUG, context, cinputs)

	if returncode != 0:
		return

	test_files =  cmpl.get_files('4Tests', ('.tst',))
	dirty_tests = pppr.execute(test_files, { 'testing': True })

	if settings.use_clean_slate:
		dirty_tests = test_files
	elif len(dirty_tests) > 0:
		settings.use_clean_slate = True

	runner_files = cmpl.get_files('4Tests', ('.[hic]pp',))
	unnecessary_prefix = stts.DIR_SOURCE + stts.DIR_TESTS
	test_exes = []
	for test in dirty_tests:
		ppp_test = pppr.get_processed_file(test)
		test_include = ppp_test.replace('\\', '/')[len(unnecessary_prefix):]
		tests = get_test_funcs(ppp_test)

		context = { 'TEST_INCLUDES': [ test_include ], 'TESTS': tests }
		unit_name = '/'.join(test.split(os.sep)[1:])
		unit_name = '.'.join(unit_name.split('.')[:-1])

		comp = cinputs.copy()
		comp.macros.append(f'UNIT_NAME=\\"' + unit_name + '\\"')
		# settings.use_clean_slate = True
		obj_files, file_statuses, rc = cmpl.prep_and_compile(
			settings.app_name, runner_files, settings, context, comp, statuses)

		if rc != 0:
			print('Failed to compile test ' + test)
			continue

		objs = all_objs + obj_files
		settings.exe_name = f'Tests/{unit_name}.exe'
		output_dir = os.path.dirname(stts.DIR_EXEC + settings.exe_name)
		if not os.path.exists(output_dir):
			os.makedirs(output_dir)

		cmpl.link_executable(settings, objs, comp)
		test_exes.append(settings.exe_name)

	if not settings.run_result:
		return

	for exe in test_exes:
		settings.exe_name = exe
		cmpl.run_output(settings)

if __name__ == '__main__':
	settings = stts.get_settings(
		stts.BuildSettings(app_name='Test Runner')
	)
	execute(settings)
