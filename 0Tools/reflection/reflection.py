import os
import re
import io

import reflection.parser as prsr
import util.settings as stts

#####################
### COMPONENT READER
#####################

'''
Take in a parser, read all the component data to a list

Component Dict Format
	full_name
	name
	pretty_name
	fields [Field]
		name
		type: Type
			name        e.g. glm::mat4
			pretty_name e.g. Mat4
			is_asset
'''

CMP_EXAMPLES = [
	{
		'full_name': '::gaming::world::Transform',
		'name': 'Transform',
		'fields': [
			{
				'name': 'x',
				'type': {
					'name': 'float32',
					'pretty_name': 'Float32',
					'is_asset': False
				}
			},
			{
				'name': 'y',
				'type': {
					'name': 'float32',
					'pretty_name': 'Float32',
					'is_asset': False
				}
			},
			{
				'name': 'z',
				'type': {
					'name': 'float32',
					'pretty_name': 'Float32',
					'is_asset': False
				}
			}
		]
	},
	{
		'full_name': '::arcadia::Mesh3D',
		'name': 'Mesh',
		'fields': [
			{
				'name': 'mesh',
				'type': {
					'name': 'mesh',
					'pretty_name': 'Mesh',
					'is_asset': True
				}
			},
			{
				'name': 'material',
				'type': {
					'name': 'Material',
					'pretty_name': 'Material',
					'is_asset': True
				}
			},
			{
				'name': 'visible',
				'type': {
					'name': 'bool',
					'pretty_name': 'Bool',
					'is_asset': False
				}
			},
		]
	}
]

RGX_COMPONENT_NAME = re.compile(r'COMPONENT ([A-Za-z0-9_]+)')
RGX_TYPE = re.compile(r'([A-Za-z0-9_:]+)')
RGX_VAR = re.compile(r'([A-Za-z0-9_]+|;)\s*=?\s*[A-Za-z0-9_:]*{?')

RGX_FUNCTION = re.compile(r'[A-Za-z0-9_:]+\s+[A-Za-z0-9_]+\s*\([A-Za-z0-9_:,\s=]*\)')

def get_type_data(name: str) -> dict:
	pretty_name = False
	is_asset = False
	match name:
		case 'f32':
			pretty_name = 'Float'
		case 'i8':
			pretty_name = 'Int8'
		case 'i16':
			pretty_name = 'Int16'
		case 'i32':
			pretty_name = 'Int32'
		case 'u8':
			pretty_name = 'UInt8'
		case 'u16':
			pretty_name = 'UInt16'
		case 'u32':
			pretty_name = 'UInt32'
		case 'math::vec2':
			pretty_name = 'Vec2'
		case 'math::vec3':
			pretty_name = 'Vec3'
		case 'math::vec4':
			pretty_name = 'Vec4'
		case 'glm::mat4': # TODO With TEECS, matrixes will not be serialised
			pretty_name = 'Mat4'
		case 'bool':
			pretty_name = 'Bool'
		case 'gfx::hdl_texture':
			pretty_name = 'Texture'
			is_asset = True
		case _:
			print(f'REFLECTION Unknown asset {name}')
			exit(1)

	return {
		'name': name,
		'pretty_name': pretty_name,
		'is_asset': is_asset
	}

def escape_scope(input: prsr.Parser, tokens: str):
	'''
	Steps through the parser until it escapes the current scope
	TOKENS: The delimiters of a scope
		TOKENS[0]: The character that marks the beginning of a new scope
		TOKENS[1]: The character that marks the end of the current scope
	'''
	# TODO Account for comments
	depth = 1
	while depth > 0:
		c = input.read_char().decode()
		if c == '':
			break

		if c == tokens[0]:
			depth += 1
		elif c == tokens[1]:
			depth -= 1

def read_field(input: prsr.Parser) -> (str, str):
	input.skip_until('[A-Za-z_]')
	input.step_back()
	c = input.read_char().decode()
	name = ''
	while c.isalnum() or c == '_':
		name += c
		c = input.read_char().decode()

	input.step_back()

	while True:
		c = input.read_char().decode()
		if c == ',' or c == ';':
			break

		if c == '{':
			escape_scope(input, '{}')
		elif c == '(':
			escape_scope(input, '()')

	return name, c

def read_fields(input: prsr.Parser) -> list:
	pos = input.stream.tell()
	text = input.stream.read().decode()
	match = RGX_TYPE.search(text)
	if match == None:
		return None

	type_name = match.group(1)
	type_data = get_type_data(type_name)
	start = match.end()
	input.seek(pos + start)

	var_names = []

	while True:
		name, next = read_field(input)
		var_names.append(name)
		if next == ';':
			break

	fields = []
	for name in var_names:
		fields.append({
			'name': name,
			'type': type_data
		})

	return fields

def read_component(input: prsr.Parser, line: str) -> dict:
	'''
	Reads all the fields in a dict
	'''
	match = RGX_COMPONENT_NAME.search(line)
	if match == None:
		return None

	component = {
		'name': match.group(1),
		'full_name': '::ecs::' + match.group(1)
	}

	input.skip_until('{')

	fields = []
	while True:
		input.skip_until('[A-Za-z_}]')
		input.step_back()

		# Detect the end of a C/++ struct '};'
		if input.peek().decode() == '}':
			pos = input.stream.tell()
			input.skip_until(r'\S')
			if input.peek().decode() == ';':
				input.read_char()
				break
			else:
				input.seek(pos)

		# Skip over a function
		pos = input.stream.tell()
		text = input.stream.read().decode()
		match = RGX_FUNCTION.match(text)
		input.seek(pos)
		if match != None:
			input.skip_until('{')
			escape_scope(input, '{}')
			continue

		fields += read_fields(input)


	component['fields'] = fields
	return component

def read_components(input: io.IOBase) -> list:
	'''
	Produces a list of components from the provided input stream
	'''
	parser = prsr.Parser(input)

	components = []
	while True:
		line = parser.readline().decode()
		if line == '':
			break

		res = read_component(parser, line)
		if res != None:
			components.append(res)

	return components

#################
# FILE REFLECTION
#################

'''
Find the dependencies of the files in a folder
Find dirty files
'''

COMPILE_DEST = '00Build/'

RGX_IFDEF = re.compile(r'#if(n?)def\s+([A-Za-z0-9_]+)')
RGX_ENDIFDEF = re.compile(r'#endif')
RGX_INCLUDE = re.compile(r"^\s*#include\s*\"([0-9A-Za-z_\.\-\/]+)\"", re.MULTILINE)

def find_dependencies(file: str, includes: list, macros: list) -> list:
	'''
	Finds the dependencies of the provided file, returning their full names
	in a list

	TODO: Handle comments around the includes
	'''
	raw_deps = []
	with open(file, 'r') as f:
		parser = prsr.Parser(f)
		goto_endif = False

		while True:
			line = parser.readline()
			if line == '':
				break

			line = line.strip()
			match = RGX_IFDEF.search(line)
			if match != None:
				defined = match.group(1) == ''
				macro = match.group(2)
				goto_endif = not (macro in macros == defined)
				if goto_endif:
					prsr.c_go_to_endif(parser.stream)
				continue

			match = RGX_INCLUDE.search(line)
			if match != None:
				res = match.group(1)
				if res.split('.')[-1] == 'cpp':
					print(f"{file} has a cpp file include '{res}'")
					exit(1)
				raw_deps.append(match.group(1))

	deps = []
	inc_folders = [ os.path.split(file)[0] ] + includes
	for d in raw_deps:
		for inc in inc_folders:
			full_path = os.path.join(inc, d)
			if os.path.exists(full_path):
				deps.append(os.path.normpath(full_path))
				break
		else:
			print(f"ERROR Could not find file {d} in {file} at with" +\
			      f"\n  Includes\n{inc_folders}\n  Macros\n{macros}")
			exit(1)

	return deps

def get_dirtiness(file: str, deps: dict, files: dict) -> bool:
	'''
	A recursive function that whether a file is dirty by scanning its
	dependencies
	The FILES dictionary is updated as we scan, making subsequent calls
	faster (if the file is dirty)
	RETURN is this file dirty
	'''
	if files[file]:
		return True

	for dep in deps[file]:
		if dep in deps and get_dirtiness(dep, deps, files):
			return True

	return False

def propogate_dirtiness(files: dict, includes: list, macros: list) -> dict:
	'''
	Scans the provided FILES. Find their dependencies and marking a file as
	dirty if any ancestors in its dependencies in the dependency tree are dirty
	RETURN a new, updated map of the files and whether they are dirty
	'''
	normalised = dict([ (os.path.normpath(f), files[f]) for f in files ])

	deps = {}
	for file in normalised:
		deps[file] = find_dependencies(file, includes, macros)

	results = {}
	for file in normalised:
		results[file] = get_dirtiness(file, deps, normalised)

	return results
