import os
import re
import io
import glob
import traceback

from reflection.parser import Parser
import util.settings as stts

'''
Pre preprocesses files and outputs them in the 00Source folder

PPR Syntax:
$                Marks the line it starts as for the processor
$IDENTIFIER$     Paste the value of the identifier
if               If branch
for x in Y       Iterates over Y with x
' FILE_GLOB '    Finds a list of files that satisfy FILE_GLOB
@IDENTIFIER      A reference to a field in a variable
ifchain          Creates an if ... else if ... else if ... chain
                   in the scope of its enclosing for block

bp               Triggers a python breakpoint
quit             Ends the processing of a block early (mainly for debugging)
'''

ID = r'[A-Za-z0-9\._\-]+'
___COMP_PARAM = r'[A-Za-z0-9._\-,()\'$@ ]+'

RGX_VAR = re.compile(r'\$([A-Za-z0-9\._\-]+)\$')

RGX_ID = re.compile(ID)
RGX_IF = re.compile(r"if\s+([A-Za-z0-9\t !<=>\._\-'@$,\(\)]+)")
RGX_ELSE = re.compile("else")
RGX_EIF = re.compile("endif")

RGX_FOR = re.compile(fr"for\s+({ID})\s+in\s+([/\*'@ A-Za-z0-9\._\(\),]+)")
RGX_EFOR = re.compile("endfor")

RGX_IF_CHAIN = re.compile(r"^\$(\s*)ifchain\s+([A-Za-z0-9\._]+)\s+in\s+([/\*'@ A-Za-z0-9\._\(\),]+)\s+(\([\(\)A-Za-z0-9\t !<=>\.\,\"'@_\$]+\))")
RGX_ECHAIN = re.compile("endchain")

# RGX_COMPARISON = re.compile(r'([A-Za-z0-9\._\,\(\)\'\$ ]+)\s*([!<=>]+)\s*([A-Za-z0-9\._\,\(\)\'\$ ]+)')
RGX_COMPARISON = re.compile(fr'({___COMP_PARAM})\s*([!<=>]+)\s*({___COMP_PARAM})')
RGX_FILE_SEARCH = re.compile(r"'\s*([A-Za-z0-9/\*\._]+)\s*'")

RGX_LOCAL_INCLUDE = re.compile(r'^(\s*)#include\s+"([A-Za-z0-9_ \/\.]+)"')

RGX_MULTILINE_COMMENT = re.compile(r'/\*|\*/')

RGX_TEST = re.compile(r"test\s+([A-Za-z0-9\s]+)")

# Context Keys
cBLOCKS = 'blocks'
cSRC = 'source_file'
cPPP = 'out_file'
cSRC_LINE_NUM = 'src_line_num'
cPPP_LINE_NUM = 'ppp_line_num'
cROOT = 'root_folder'
cIN_COMMENT = 'in_comment'
cTESTING = 'testing'
cTEST_COUNTER = 'test_counter'

def get_processed_file(source: str) -> str:
	split = os.path.split(source)
	dirname = stts.DIR_SOURCE + split[0]
	file_name = 'p_' + split[1]
	return dirname + os.sep + file_name

def error(context: dict, msg: str):
	id = ''
	file_name = ''
	line_num = ''
	if cBLOCKS in context and len(context[cBLOCKS]) > 0:
		id = context[cBLOCKS][-1]['line']

	if cSRC in context: file_name = context[cSRC]
	if cSRC_LINE_NUM in context: line_num = str(context[cSRC_LINE_NUM])

	if file_name == line_num == '':
		print(msg)
	else:
		print(f'{file_name}:{line_num}   {id}\n{msg}')
	traceback.print_stack()
	exit(1)

def check_if_sublock(input: io.IOBase, line: str) -> str:
	if RGX_IF.search(line) == None:
		return ''

	sub_block, ender = read_block(input, (RGX_EIF, ))
	sub_block.seek(0)
	return line + sub_block.read() + '$ endif\n'

def check_for_sublock(input: io.IOBase, line: str) -> str:
	if RGX_FOR.search(line) == None:
		return ''

	sub_block, ender = read_block(input, (RGX_EFOR, ))
	sub_block.seek(0)
	return line + sub_block.read() + '$ endfor\n'


def skip_until(context: dict, input: io.IOBase, enders: tuple) -> re.Pattern:
	while True:
		line = input.readline()
		if line == '':
			return None

		if not line.startswith('$'):
			continue

		for ender in enders:
			m = ender.search(line)
			if m != None:
				return ender

		check_if_sublock(input, line)
		check_for_sublock(input, line)

	return None

def write_line_directive(file: str, context: dict, line: int, output: io.IOBase):
	if 'no-lines' in context:
		return

	output.write(f'#line {line} "{file}"\n')
	context[cPPP_LINE_NUM] += 1

def read_block(input: io.IOBase, enders: tuple) -> (io.StringIO, re.Pattern):
	block = io.StringIO()
	while True:
		line = input.readline()
		if line == '':
			return (block, None)

		if not line.startswith('$'):
			block.write(line)
			continue

		for ender in enders:
			m = ender.search(line)
			if m != None:
				return (block, ender)

		res = check_if_sublock(input, line)
		if res != '':
			block.write(res)
			continue

		res = check_for_sublock(input, line)
		if res != '':
			block.write(res)
			continue

		block.write(line)

	return (block, None)

def get_id(context: dict, id: str):
	# This is a reference to a property of a variable
	if id[0] == "@":
		return id

	parts = id.split('.')
	scope = context
	for i in range(len(parts) - 1):
		p = parts[i]
		if p in scope:
			scope = scope[p]
		else:
			error(context, f'Could not find {id} at {p} in\n{context}')

	if parts[-1] in scope:
		return scope[parts[-1]]
	else:
		return id

def get_bool(context: dict, id: str) -> bool:
	res = get_id(context, id)
	if not isinstance(res, bool):
		error(context, f'Expected BOOL got {res}')

	return bool(res)

def get_string(context: dict, id: str) -> str:
	return str(get_id(context, id))

def try_calc_condition(context: dict, string: str) -> int:
	match = RGX_COMPARISON.search(string)
	if not match:
		return -1

	l = evaluate(context, match.group(1))
	r = evaluate(context, match.group(3))
	boolean = { True: 1, False: 0 }
	match match.group(2):
		case '==':
			return boolean[l == r]
		case '>':
			return boolean[int(l) > int(r)]
		case '<':
			return boolean[int(l) < int(r)]
		case '>=':
			return boolean[int(l) >= int(r)]
		case '<=':
			return boolean[int(l) <= int(r)]

	return -1

def get_iterable(context: dict, iterable_id: str) -> list:
	match = RGX_FILE_SEARCH.search(iterable_id)
	if match == None:
		# It's a variable
		iterable = evaluate(context, iterable_id)
	else:
		# File search
		search_term = match.group(1)
		has_root = cROOT in context
		if has_root:
			search_term = context[cROOT] + os.sep + search_term
		iterable = glob.glob(search_term, recursive=True)
		iterable = [ i.replace('\\', '/') for i in iterable ]
		if has_root:
			iterable = [ i.replace(context[cROOT] + '/', '') for i in iterable ]
			iterable = [ stts.get_ppprocessed_include(i) for i in iterable ]

	return iterable

def evaluate_condition(context: dict, condition: str) -> bool:
	tcc = try_calc_condition(context, condition)
	if tcc == -1:
		return get_bool(context, condition)

	# If 0 return False
	return tcc != 0

def evaluate(context: dict, id: str):
	id = id.strip()

	token = ''
	param = ''
	read_function_params = False
	depth = 0
	params = []
	for c in id:
		if c.isspace():
			continue

		if read_function_params:
			if c.isalnum() or c == '.' or c == "'" or c == '_' or c == '@':
				param += c
			elif c == '(':
				param += c
				depth += 1
			elif c == ')':
				depth -= 1
				if depth == 0:
					params.append(evaluate(context, param))
					param = ''
				else:
					param += c
			elif c == ',':
				if depth == 1:
					params.append(evaluate(context, param))
					param = ''
				else:
					param += c
			else:
				error(context, 'Unrecognised character ' + c + '.')
		else:
			if c.isalnum() or c == '.' or c == "'" or c == '_' or c == '@':
				token += c
			elif c == '(':
				read_function_params = True
				depth = 1
			else:
				error(context, 'Unrecognised character ' + c)

	if depth != 0:
		error(context, f'Missing closed paranthesis in {id}')

	if read_function_params:
		match token:
			case 'len':
				return len(params[0])
			case 'where':
				res = []
				condition = params[1][1:]
				# print(params)
				for i in params[0]:
					if evaluate_condition(i, condition):
						res.append(i)
				# print(res)
				return res
			case _:
				error(context, f'Unknown function {token}')

	return get_id(context, token)


def process_line(context: dict, output: io.IOBase, line: str, input_line_num: int):
	'''
	Print a line replacing any tokens with the appropriate output
	'''

	# 0 - do nothing, 1 - prepend /*, 2 - append */
	make_comment = 0
	for match in RGX_MULTILINE_COMMENT.findall(line):
		if match == '/*':
			if make_comment == 0: make_comment = 2
		elif match == '*/':
			if make_comment == 0: make_comment = 1
			elif make_comment == 2: make_comment = 0

	if make_comment == 1: # Escaped the comment block
		line = '/* ' + line
		context[cIN_COMMENT] = False
	elif make_comment == 2:
		context[cIN_COMMENT] = True
		line = line[:-1] + ' */\n'
	elif context[cIN_COMMENT]:
		line = '/* ' + line[:-1] + ' */\n'

	if len(context[cBLOCKS]) > 0:
		line = f'/* {input_line_num} */ ' + line

	line_index = 0
	for match in RGX_VAR.finditer(line):
		output.write(line[line_index:match.start()])
		output.write(get_string(context, match.group(1)))
		line_index = match.end()

	output.write(line[line_index:])
	context[cPPP_LINE_NUM] += 1

def process_include(line: str, context: dict, output: io.IOBase) -> bool:
	match = RGX_LOCAL_INCLUDE.search(line)
	if match == None:
		return False

	include = match.group(2)
	replacement = stts.get_ppprocessed_include(include)
	output.write(f'{match.group(1)}#include "{replacement}"\n')
	context[cPPP_LINE_NUM] += 1
	return True

def process_if(context: dict, input: Parser, output: io.IOBase, depth: int, line: str) -> bool:
	match = RGX_IF.search(line)
	if match == None:
		return False

	condition = match.group(1)
	tcc = try_calc_condition(context, condition)
	if tcc == -1:
		if_res = get_bool(context, condition)
	else:
		if_res = False if tcc == 0 else True

	start_line_num = context[cSRC_LINE_NUM]
	if not if_res:
		ender = skip_until(context, input, (RGX_ELSE, RGX_EIF))
		start_line_num = input.line_num
		if ender == RGX_EIF:
			return True
		if ender == None:
			error(context, 'IF statement not closed')

	block, ender = read_block(input, (RGX_ELSE, RGX_EIF))
	context[cBLOCKS].append({
		 'id': input.get_line_num(), 'type': 'if', 'line': line
		})

	if ender == RGX_ELSE:
		ender = skip_until(context, input, (RGX_EIF, ))
		if ender == None:
			error(context, 'IF statement not closed')

	is_not_deep = len(context[cBLOCKS]) <= 1

	if is_not_deep:
		write_line_directive(context[cPPP], context, context[cPPP_LINE_NUM] + 1, output)
	pre_preprocess(context, Parser(block, start_line_num), output, depth + 1)
	context[cBLOCKS].pop()
	if is_not_deep:
		write_line_directive(context[cSRC], context, context[cSRC_LINE_NUM], output)

	return True

def process_for(context: dict, input: Parser, output: io.IOBase, depth: int, line: str) -> bool:
	match = RGX_FOR.search(line)
	if match == None:
		return False

	iterator = match.group(1)
	iterable_id = match.group(2)
	iterable = get_iterable(context, iterable_id)

	# Variable shadowing
	shadowing = iterator in context
	if shadowing:
		old = context[iterator]

	start_line_num = context[cSRC_LINE_NUM]
	block, ender = read_block(input, (RGX_EFOR, ))
	if ender == None:
		error(context, 'FOR block not closed')

	context[cBLOCKS].append({
		 'id': input.get_line_num(), 'type': 'for', 'line': line
		})

	is_not_deep = len(context[cBLOCKS]) <= 1

	if is_not_deep:
		write_line_directive(context[cPPP], context, context[cPPP_LINE_NUM] + 1, output)
	for i in iterable:
		context[iterator] = i
		context[cSRC_LINE_NUM] = start_line_num
		pre_preprocess(context, Parser(block, start_line_num), output, depth + 1)

	if iterator in context:
		context.pop(iterator)
	context[cBLOCKS].pop()
	context[cSRC_LINE_NUM] = input.line_num

	if is_not_deep:
		write_line_directive(context[cSRC], context, context[cSRC_LINE_NUM] + 1, output)

	if shadowing:
		context[iterator] = old

	return True

def process_ifchain(context: dict, input: Parser, output: io.IOBase, depth: int, line: str) -> bool:
	match = RGX_IF_CHAIN.search(line)
	if match == None:
		return False

	indentation = match.group(1)
	iterator = match.group(2)
	iterable_id = match.group(3)
	condition = match.group(4)

	iterable = get_iterable(context, iterable_id)
	# Variable shadowing
	shadowing = iterator in context
	if shadowing:
		old = context[iterator]

	start_line_num = context[cSRC_LINE_NUM]
	block, ender = read_block(input, (RGX_ECHAIN, ))
	if ender == None:
		error(context, f'IFCHAIN block not closed')

	context[cBLOCKS].append({
		 'id': input.get_line_num(), 'type': 'ifchain', 'line': line
		})

	is_not_deep = len(context[cBLOCKS]) <= 1

	if is_not_deep:
		write_line_directive(context[cPPP], context, context[cPPP_LINE_NUM] + 1, output)
	first = True
	for i in iterable:
		if_stmt = 'if' if first else 'else if'
		first = False
		context[iterator] = i
		context[cSRC_LINE_NUM] = start_line_num
		process_line(context, output, f'{indentation}{if_stmt} {condition}\n', start_line_num)
		pre_preprocess(context, Parser(block, start_line_num), output, depth + 1)

	if iterator in context:
		context.pop(iterator)
	context[cBLOCKS].pop()

	if is_not_deep:
		write_line_directive(context[cSRC], context, context[cSRC_LINE_NUM] + 1, output)

	if shadowing:
		context[iterator] = old

	return True

def process_test(context: dict, input: Parser, output: io.IOBase, depth: int, line: str) -> bool:
	if cTESTING not in context:
		return False

	match = RGX_TEST.search(line)
	if match == None:
		return False

	if cTEST_COUNTER in context:
		number = context[cTEST_COUNTER]
	else:
		number = 0

	test_name = match.group(1)
	test_name = test_name.strip()
	func_name = f'__{number:03}_' + test_name.replace(' ', '_')

	output.write(f'void {func_name}(bool *success)\n')

	context[cTEST_COUNTER] = number + 1

	return True

def pre_preprocess(context: dict, input: Parser, output: io.IOBase, depth=0):
	if not cBLOCKS in context:
		context[cBLOCKS] = []
	if not cIN_COMMENT in context:
		context[cIN_COMMENT] = False
	if not cPPP_LINE_NUM in context:
		context[cPPP_LINE_NUM] = 0

	any_edits = False
	input.seek(0)
	context[cSRC_LINE_NUM] = input.line_num
	while True:
		line = input.readline()
		context[cSRC_LINE_NUM] += 1
		if line == '':
			break

		if process_include(line, context, output):
			continue

		if not line.startswith('$'):
			process_line(context, output, line, input.line_num)
			continue

		if process_if(context, input, output, depth, line) \
			or process_for(context, input, output, depth, line) \
			or process_ifchain(context, input, output, depth, line) \
			or process_test(context, input, output, depth, line):
			any_edits = True
			continue

		match line[1:].strip():
			case 'quit':
				return
			case 'bp':
				output.flush()
				breakpoint()
			case _:
				error(context, f'Do not recognise statement\n\t{line}')

def ppp_files_only(context: dict, src: str, ppr: str):
	context[cSRC] = os.path.abspath(src).replace('\\', '/')
	context[cPPP] = os.path.abspath(ppr).replace('\\', '/')
	with open(context[cSRC], 'r') as s:
		with open(context[cPPP], 'w') as o:
			context[cPPP_LINE_NUM] = 0
			write_line_directive(context[cSRC], context, 1, o)
			pre_preprocess(context, Parser(s), o)

def execute(files: list, context = dict(), redo_all=False) -> list:
	'''
	Pre-preprocesses all the files provided
	RETURNS a list of the provided files that had to be ppprocessed
	'''
	dirty_srcs = []
	if redo_all:
		dirty_srcs = [ (f, get_processed_file(f)) for f in files ]
	else:
		for src in files:
			src_time = os.path.getmtime(src)
			ppr_dest = get_processed_file(src)
			dirname = os.path.dirname(ppr_dest)
			if not os.path.exists(dirname):
				os.makedirs(dirname)

			if not os.path.exists(ppr_dest):
				dirty_srcs.append((src, ppr_dest))
				continue

			ppr_time = os.path.getmtime(ppr_dest)
			if src_time > ppr_time:
				dirty_srcs.append((src, ppr_dest))

	for src, ppr in dirty_srcs:
		dirname = os.path.dirname(ppr)
		if not os.path.exists(dirname):
			os.makedirs(dirname)

		ppp_files_only(context.copy(), src, ppr)

	return [ s[0] for s in dirty_srcs ]
