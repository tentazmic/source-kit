#!/usr/bin/python
import build.compiler as cmpl
import build.build_unit as bdut
import util.settings as stts

from util.timer import Timer

def execute(settings: stts.BuildSettings):
	if settings.use_timing:
		timer = Timer()
		timer.start()

	module_names = [
		'renderer', 'install-driver', 'application', 'asset-filesystem', 'imgui'
	]
	modules = bdut.get_modules(module_names)
	graphics = [ bdut.G_OPENGL ]

	settings.build_name = 'Base'
	unit_settings = settings.copy()

	comp = cmpl.CompilerInputs(
		macros=[ 'PRJ_ROOT=\\"Assets\\"', 'LOCAL_PRJ_ROOT=\\"Assets\\"' ]
	)
	comp, all_objs, context, statuses, returncode =\
		cmpl.compile_exe(comp, unit_settings, bdut.PR_DEBUG,
		                 bdut.get_platform(settings), modules + graphics)

	# Target
	if settings.verbosity_level >= 1:
		print('Build Target')

	comp.includes.append(stts.DIR_SOURCE + 'Base')
	deps = cmpl.CompilerInputs()
	for dep in modules:
		deps.add(cmpl.get_compiler_inputs(settings, dep))

	comp.includes.extend(deps.includes)

	files = cmpl.get_files('Base', ('.[hic]pp', ))

	obj_files, file_statuses, rc = cmpl.prep_and_compile(
		'Target', files, unit_settings, context, comp, statuses
	)

	returncode += rc
	if returncode != 0:
		return

	all_objs += obj_files

	cmpl.link_executable(settings, all_objs, comp)

	if settings.use_timing:
		timer.stop()
		print(f'Base Build Time {timer.print()}')

	if settings.run_result:
		cmpl.run_output(settings)

if __name__ == '__main__':
	config = stts.read_project_config()
	settings = stts.get_settings(
		stts.BuildSettings(app_name=config['name'], exe_name=config['executable'])
	)
	execute(settings)
