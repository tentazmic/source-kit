#!/usr/bin/python
import os
import glob

import build.compiler as cmpl
import build.build_unit as bdut
import reflection.reflection as refl
import util.settings as stts

from util.timer import Timer

class Install:
	def __init__(self, name: str, folder: str, dependencies: list):
		self.name = name
		self.folder = folder
		self.dependencies = dependencies

def get_installs() -> list:
	installs = []
	for file in glob.glob(stts.DIR_INSTALLS + '**/install', recursive=True):
		folder, _ = os.path.split(file)
		_, name = os.path.split(folder)

		dependencies = bdut.get_modules([ 'installs', 'asset-filesystem' ])
		with open(file, 'r') as config:
			for line in config:
				k, v = line.split(':')
				match k:
					case 'dependencies':
						deps = [ i.strip() for i in v.split() ]
						dependencies.append(bdut.get_modules(deps))

		installs.append(
			Install(name=name, folder=folder, dependencies=dependencies)
		)

	return installs

def build_install(install: Install, profile: bdut.Profile,
                  cinputs: cmpl.CompilerInputs, settings: stts.BuildSettings,
                  context: dict, objs: list, file_statuses: dict)\
                   -> int:
	if settings.use_timing:
		timer = Timer()
		timer.start()

	returncode = 0

	# Skip the first one, we know it is M_INSTALLS and it is built in execute
	# If a dependency appears in multiple installs, it will be be built the first
	# time then all of its files will be recognised as not dirty and not built again
	# Unless -c is passed
	for dep in install.dependencies[1:]:
			c, o, statuses, ret =\
			  cmpl.compile_build_unit(settings, dep, context, cinputs, file_statuses)
			cinputs.add(c)
			objs += o
			file_statuses.update(statuses)
			returncode += ret

	if returncode != 0:
		return 1

	print('Build Install ' + install.name)

	if settings.verbosity_level >= 1:
		print('Compiling Install ' + install.name)

	files = cmpl.get_files(install.folder, ('.[hic]pp'))

	cinputs.macros.append('PRJ_ROOT=\\"Assets\\"')
	local_assets = install.folder.replace('\\', '/') + '/Assets'
	cinputs.macros.append(f'LOCAL_PRJ_ROOT=\\"{local_assets}\\"')

	settings.build_name = 'Install/' + install.name

	chime_reset = False
	if settings.build_chime == stts.CHIME_ALWAYS:
		settings.build_chime = stts.CHIME_FAIL
		chime_reset = True
	install_objs, statuses, ret = cmpl.prep_and_compile(
		install.name, files, settings, context, cinputs, file_statuses
	)

	returncode += ret
	if returncode != 0:
		return returncode

	objs += install_objs
	file_statuses.update(statuses)

	settings.exe_name = stts.DIR_INSTALLS_DEST + install.name + '.instl'
	if chime_reset:
		settings.build_chime = stts.CHIME_ALWAYS
	returncode = cmpl.link_shared_library(settings, objs, cinputs)

	if settings.use_timing:
		timer.stop()
		print(f'Install Build Time {timer.print()}')

	return returncode

def execute(settings: stts.BuildSettings):
	if settings.use_timing:
		timer = Timer()
		timer.start()

	components = []
	for f in glob.glob('Framework/Common/**/*.cmp', recursive=True):
		with open(f, 'rb') as input:
			comps = refl.read_components(input)
		components += comps

	context = { 'COMPONENTS': components, 'root_folder': 'Framework/Common' }

	settings.build_name = 'Install'
	settings.target_exe=False
	unit_settings = settings.copy()
	if settings.build_chime == stts.CHIME_ALWAYS:
		unit_settings.build_chime = stts.CHIME_FAIL

	comp, all_objs, statuses, returncode =\
		cmpl.compile_common(unit_settings, bdut.PR_DEBUG, context,
		                    cmpl.CompilerInputs(macros=[ 'BLD_INSTALL' ]))

	comp.macros.append(f'"PRJ_NAME=\\"{settings.app_name}\\""')
	cinstalls, objs, stats, code =\
		cmpl.compile_build_unit(unit_settings, bdut.M_INSTALLS,
		                        context, comp, statuses)
	all_objs += objs
	statuses.update(stats)
	returncode += code

	cplt, objs, stats, code =\
		cmpl.compile_build_unit(unit_settings, bdut.get_platform(settings),
		                        context, comp, statuses)
	all_objs += objs
	statuses.update(stats)
	returncode += code

	if returncode != 0:
		return

	comp.add(cinstalls)
	comp.add(cplt)

	if not os.path.exists(stts.DIR_INSTALLS_DEST):
		os.makedirs(stts.DIR_INSTALLS_DEST)

	if settings.verbosity_level >= 2:
		print()
		print()

	if settings.verbosity_level >= 1:
		print('Building Installs')

	installs = get_installs()
	if settings.verbosity_level >= 2:
		for install in installs:
			print(install.name)

	for install in installs:
		returncode += build_install(install, bdut.PR_DEBUG, comp.copy(),
		                            settings.copy(), context.copy(), all_objs.copy(), statuses.copy())

	if settings.use_timing:
		timer.stop()
		print(f'Total Install Build Time {timer.print()}')

if __name__ == '__main__':
	config = stts.read_project_config()
	settings = stts.get_settings(stts.BuildSettings(app_name=config['name']))
	execute(settings)
