#pragma once
#include "Application/tick.hpp"
#include "rdr/renderer.hpp"

namespace dbg::dwin {

void Initialise();
void Update(tik::frame_stamp&);
void Draw(const rdr::statistics&);
void Shutdown();

};
