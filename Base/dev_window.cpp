#include "pch.h"

#include "dev_window.hpp"
#include <imgui.h>

#include "Application/events.hpp"
#include "gui/imgui.hpp"

#include "Util/list.hpp"

namespace dbg::dwin {

namespace {
	evt::dispatcher s_Dispatcher(512);

	struct {
		f64 previous = 0;
		f64 sum = 0;
		u32 count = 0;

		u32 current = 0;
		f64 avgFrameTime = 0;
	} s_FrameStep;

	f32 s_FrameStepRefreshRate = 2.f;
};

void Initialise()
{
  arr::sttc_list<u32, 2> flags = {
    ImGuiConfigFlags_ViewportsEnable
  };
  gui::Initialise(s_Dispatcher, flags.get());
}

void Shutdown()
{
  gui::Shutdown();
}

void Update(tik::frame_stamp& fs)
{
	s_FrameStep.sum += fs.deltaTime;
	s_FrameStep.count++;

	if (fs.timeSinceStart - s_FrameStep.previous > s_FrameStepRefreshRate)
	{
		s_FrameStep.current = fs.frame;
		s_FrameStep.avgFrameTime = s_FrameStep.sum / s_FrameStep.count;

		s_FrameStep.previous = fs.timeSinceStart;
		s_FrameStep.sum = 0;
		s_FrameStep.count = 0;
	}
}

void Draw(const rdr::statistics &stats)
{
	gui::BeginFrame();

	const ImGuiViewport *vp = ImGui::GetMainViewport();
	ImGui::SetNextWindowPos(
		ImVec2(vp->WorkPos.x + 650, vp->WorkPos.y + 30),
		ImGuiCond_Appearing
	);
	ImGui::SetNextWindowSize(ImVec2(500, 700), ImGuiCond_Appearing);

	const ImGuiWindowFlags flags = ImGuiWindowFlags_MenuBar;
	if (!ImGui::Begin("Dev Window", NULL, flags))
		goto __END;

	ImGui::PushItemWidth(ImGui::GetFontSize() * -12);

	static bool s_ShowDemoWindow = false;
	if (ImGui::BeginMenuBar())
	{
		ImGui::MenuItem("Show Demo Window", NULL, &s_ShowDemoWindow);
		ImGui::EndMenuBar();
	}

	ImGui::DragFloat("Frame Step Refresh Rate", &s_FrameStepRefreshRate, 0.05f, 0.1f, 10.0f);

	ImGui::Text("Frame No.:  %d", s_FrameStep.current);
	ImGui::Text("Frame Time: %.3f", s_FrameStep.avgFrameTime * 1000.0f);
	ImGui::Text("FPS:        %.1f", 1.0f / s_FrameStep.avgFrameTime);

	{
		const ImGuiTreeNodeFlags fRenderStats = ImGuiTreeNodeFlags_DefaultOpen;
		if (ImGui::CollapsingHeader("Render Stats", fRenderStats))
		{
			ImGui::Text("Draw Calls: " SSIZET_FMT, stats.drawCalls);
			ImGui::Text("Tris: " SSIZET_FMT, stats.trisDrawn);
			ImGui::Text("Vertices: " SSIZET_FMT, stats.vertexCount);
			ImGui::Text("Indices: " SSIZET_FMT, stats.indexCount);
		}
	}

	if (s_ShowDemoWindow)
		ImGui::ShowDemoWindow(&s_ShowDemoWindow);

__END:
	ImGui::End();
	gui::EndFrame();
}

};
