# Editor

I've decided to shelve development of the editor. It's already decided that for
the viewport I will just be reimplementing Blender's viewport controls as well
as its 3D cursor.  
So for the time being focus will be switched to making Blender extensions to have
Blender serve as the editor. I may return to developing a standalone editor in
the future.

Below is the editor roadmap.

## Editor Tasks

Serialisation of entities and scenes  
Editing and saving a scene  
Create and delete scenes  

Editive Install Scenes:  
This is going to need to trigger an event whenever a scene in Base is loaded,
look for the editive scene for that scene and if found make the additions and/or replacements

Start up playing a scene: making a copy of the scene and allowing it to run  
Individual compilation of systems into shared libraries so they can be used in the Editor  

Scene Viewport navigation in the Editor and some simple controls  
- click+drag: orbit about a point
- control+click+drag: pan viewport
- shift+click+drag: look around, rotating the camera on its transform and moving the origin as if it were a child of it
- mouse picking
- scroll: zoom
- control+right click: place viewport cursor
- A: add entity dialogue

Keymap file  

Integrate the installs into the editor  
- Create install scenes (new and editive)  
- Editor mode for installs, giving access to the Base file system and the local install one  
- View scenes with multiple installs loaded
- Play scenes with multiple installs loaded

Live shader editor panel



## Plan

Editor shall be completely navigable by keyboard  
Blender-esc scene view navigation and keyboard shortcuts  
Layout Tabs for specific panel arrangements  
No Inspector + Scene Tabs  
- You have multiple inspector tabs, numbered 1, 2, 3
- They represent the depth level of inspection, e.g. scene => entity => component's subcomponent view

Entity paging, allowing you to group sets of an entity's components on specific pages
for a clearer view, e.g.

```
|All|Gameplay|Graphics|
|:--------:|:-------:|:---------:|
|Transform|Transform|Transform|
|StateMachine|StateMachine|SkinnedMeshRenderer|
|Runner|Runner|Animator|
|SkinnedMeshRenderer|BulletSpawner|PlayerAnimations|
|BulletSpawner|BulletManager|ParticleManager|
|BulletManager|Rigidbody||
|Animator|CapsuleCollider||
|PlayerAnimations|||
|Rigidbody|||
|CapsuleCollider|||
|ParticleManager|||
```

Viewport Cursor:  
Like Blender's 3D cursor, it is a point in space that acts as a visual guide
Newly created objects will be placed at it, you can scale and rotate a scene relative to
it, you can use it to place the orbit point of the scene view camera

Viewport Scene:  
All the things in the viewport (the camera, scene cursor, etc) are entities
Allowing you to manipulate them as you would anyother scene

Entity Schemas:
Generated from the C++ components. Tells the editor what properties it can
display, what are serialised and editor widgets.
e.g. range, textarea, hidden, nonserialised

```
Cjump
{
	force float: range(0, 10)
	jumpPoints [CPosition*]: max(10)

	lastJumpPoint CPosition*: hidden
}
```
