#include "pch.h"
#include "inspect.hpp"

#include "imgui"

$ for cmp in ' ECS/*.cmp '
#include "$cmp$"
$ endfor

namespace edt::inspect {

namespace {
$ for cmp in COMPONENTS

	void $cmp.name$($cmp.full_name$ *c)
	{
$ 	for field in cmp.fields
$ 		if field.type.is_asset
		Texture("$field.name$", gfx::hdl_texture{}, nullptr);
$ 		else
		$field.type.pretty_name$("$field.name$", &c->$field.name$);
$ 		endif
$ 	endfor
	}
$ endfor
};

void InspectComponent(ecs::Type type, void *component)
{
$	ifchain cmp in COMPONENTS (SAME_TYPE(ECS_TYPE($cmp.full_name$), type))
	{
		$cmp.name$(RCAST($cmp.full_name$ *)(component));
	}
$ endchain
}

void Bool(const char *name, bool *b)
{
	ImGui::Checkbox(name, b);
}

void Int(const char *name, i32 *i)
{
	ImGui::DragInt(name, i);
}

void Float(const char *name, f32 *f)
{
	ImGui::DragFloat(name, f);
}
void Vec2(const char *name, math::vec2 *v)
{
	ImGui::DragFloat2(name, &v->x);
}
void Vec3(const char *name, math::vec3 *v)
{
	ImGui::DragFloat3(name, &v->x);
}
void Vec4(const char *name, math::vec4 *v)
{
	ImGui::DragFloat4(name, &v->x);
}
void Mat4(const char *name, glm::mat4 *v) { ImGui::Text(name); }

void Texture(const char *name, gfx::hdl_texture tex, gfx::_TextureLib *library) { ImGui::LabelText("Texture", name); }
};
