#include "pch.h"
#include "file_explorer.hpp"

#include <imgui.h>
#include "Util/string.hpp"

namespace edt::gui {

namespace {
	const ImGuiTreeNodeFlags kTreeNodeDirectory =
		ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick
		| ImGuiTreeNodeFlags_SpanAvailWidth;

	// Passing ACTIVEENTRIES by reference as this is a recursive function
	enum struct EFilterReason { NONE, EXTENSION, NOT_A_DIR, BAD_DIR };
	EFilterReason FilterEntries(const edt::asset::directory_tree& tree, const explorer_filter& filter, arr::bitset& activeEntries, size_t index);
	void _DrawFileExplorer(size_t index, explorer_selection& selection, const edt::asset::directory_tree& tree, size_t currentSelected, const arr::bitset& activeEntries, arr::bitset& openDirectories, const explorer_filter& filter);
};

explorer_selection DrawFileExplorer(const edt::asset::directory_tree& tree, size_t currentSelected, arr::bitset openDirectories, const explorer_filter& filter)
{
	if (openDirectories.length() < tree.count())
	{
		LG_ERR("Passed openDirectories {} is smaller than tree {}", openDirectories.length(), tree.count());
		return explorer_selection{};
	}

	arr::dync_bitset actives = arr::HeapBitset(openDirectories.length());
	arr::bitset got = actives.get();
	FilterEntries(tree, filter, got, 0);

	// LG_TRC(got.data()[0]);
	explorer_selection selection{ .index = tree.count() };
	_DrawFileExplorer(0, selection, tree, currentSelected, got, openDirectories, filter);
	return selection;
}

explorer_selection DrawDirectoryContents(const edt::asset::directory_tree& tree, size_t idxDirectory, size_t currentSelected, const contents_config& config, const explorer_filter& filter)
{
	explorer_selection selection{ .index = tree.count() };

	for (size_t i = idxDirectory + 1; i < tree.count(); i++)
	{
		if (tree[i].parent != idxDirectory)
			continue;
		if (!tree[i].isDirectory && filter.onlyDirectories)
			continue;
		// Filter by extension
		if (filter.extension && !str::EndsWith(tree[i].name, filter.extension))
			continue;

		if (!ImGui::Selectable(tree[i].name, i == currentSelected, 0))
			continue;

		selection.index = i;
		selection.byDoubleSelection = i == currentSelected;
	}

	return selection;
}

namespace {

	EFilterReason FilterEntries(const edt::asset::directory_tree& tree, const explorer_filter& filter, arr::bitset& activeEntries, size_t index)
	{
		const edt::asset::file_entry& entry = tree[index];
		if (!entry.isDirectory)
		{
			activeEntries[index] = false;
			if (filter.onlyDirectories)
				return EFilterReason::NOT_A_DIR;
			if (filter.extension && !str::EndsWith(entry.name, filter.extension))
				return EFilterReason::EXTENSION;

			activeEntries[index] = true;
			return EFilterReason::NONE;
		}

		// Filter by folder name

		bool valid = false;
		bool hasChildren = false;
		for (size_t i = index + 1; i < tree.count(); i++)
		{
			if (tree[i].parent != index) continue;

			hasChildren = true;
			EFilterReason reason = FilterEntries(tree, filter, activeEntries, i);
			switch (reason)
			{
				case EFilterReason::EXTENSION:
				{
					valid |= false;
					break;
				}
				default:
				{
					valid |= true;
				}
			}
		}

		// Allow empty directories through, assuming they haven't
		// been filtered out
		if (hasChildren && !valid)
		{
			activeEntries[index] = false;
			return EFilterReason::BAD_DIR;
		}

		// bool res = valid | !hasChildren;
		activeEntries[index] = true;
		return EFilterReason::NONE;
	}

	void _DrawFileExplorer(size_t index, explorer_selection& selection, const edt::asset::directory_tree& tree, size_t currentSelected, const arr::bitset& activeEntries, arr::bitset& openDirectories, const explorer_filter& filter)
	{
		const edt::asset::file_entry& entry = tree[index];
		if (!activeEntries[index])
			return;

		if (!entry.isDirectory ||(filter.onlyDirectories ? !edt::asset::DirectoryHasChildDirectories(tree, index) : !edt::asset::DirectoryHasChildren(tree, index)))
		{
			if (!ImGui::Selectable(entry.name, index == currentSelected, 0))
				return;

			selection.index = index;
			selection.byDoubleSelection = index == currentSelected;
			return;
		}

		// Only directories with children
		ImGuiTreeNodeFlags flags = kTreeNodeDirectory;
		if (index == currentSelected)
			flags |= ImGuiTreeNodeFlags_Selected;

		ImGui::SetNextItemOpen(openDirectories[index]);
		bool success = ImGui::TreeNodeEx(RCAST(void *)(RCAST(uintptr_t)(index)), flags, entry.name);
		openDirectories[index] = success;
		if (!success)
			return;

		if (ImGui::IsItemClicked())
		{
			selection.index = index;
			selection.byDoubleSelection = index == currentSelected;
		}

		for (size_t i = index + 1; i < tree.count(); i++)
		{
			if (tree[i].parent != index)
				continue;

			_DrawFileExplorer(i, selection, tree, currentSelected, activeEntries, openDirectories, filter);
		}

		ImGui::TreePop();
	}
};

};
