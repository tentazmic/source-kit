#pragma once
#include "core"
#include "Util/list.hpp"

namespace edt::asset {

enum struct EFileStatus : u8 { NONE, MODIFIED, CREATED, DELETED };

struct file_entry
{
	const static u16 NONE = MAX(u16);

	u16 parent = NONE;
	EFileStatus status = EFileStatus::NONE;
	bool isDirectory;
	fs::file_time_type lastWriteTime;
	char *name;
};

struct file
{
	EFileStatus status = EFileStatus::NONE;
	fs::file_time_type lastWriteTime;
	fs::path path;
};

using directory_tree = arr::list<file_entry>;

// TODO Filtration at watcher level

struct watcher
{
	watcher() : _root(), _isWatchingDirectory{false}, _files() { }
	watcher(const fs::path& root, size_t capacity);

	watcher& operator =(const watcher& other) = delete;
	watcher& operator =(watcher&& other)
	{
		MOVE_CHECK
		_root = std::move(other._root);
		_isWatchingDirectory = other._isWatchingDirectory;
		_files = std::move(other._files);
		other._isWatchingDirectory = false;
		return *this;
	}

	// void Tick();
	size_t GetIndexFromFilePath(const fs::path& path) const;
	fs::path GetFullPath(size_t idxEntry);

	inline const directory_tree tree() const { return _files.get(); }

private:
	fs::path _root;
	bool _isWatchingDirectory;
	arr::dync_list<file_entry> _files;
};

void FillDirectoryItems(const fs::path& root, directory_tree& tree, bool trimBeginning = false, u16 parent = MAX(u16));
bool DirectoryHasChildren(const directory_tree& tree, size_t iDirectory);
bool DirectoryHasChildDirectories(const directory_tree& tree, size_t iDirectory);
};
