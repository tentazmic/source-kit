#pragma once
#include "core"
#include "ecs"
#include "Memory/ref.hpp"
#include "Util/list.hpp"

#include "rdr/renderer.hpp"

namespace edt::asset {

struct scene
{
	ecs::World *world;
	rdr::twod::render_scene scene;
	rdr::twod::default_data defaults;
};

void SceneInit();
void Shutdown();
scene * LoadScene(const fs::path& filepath);

std::map<const char *, scene>& GetActiveScenes();
};

