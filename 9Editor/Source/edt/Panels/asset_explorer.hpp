#pragma once
#include "panel.hpp"
#include "edt/Assets/watcher.hpp"
#include "Util/bitset.hpp"

namespace edt::panel::asset_explorer {

class PnlAssetExplorer : public _Panel
{
public:
	const static u16 MAX_NUM = 0;

	PnlAssetExplorer(const fs::path& directory);
	~PnlAssetExplorer() { }

	// void Update() override;
	// void OnEvent(evt::event& e) override;
	void GUI() override;

private:
	edt::asset::watcher _watcher;
	arr::dync_bitset _activeEntries;
	size_t _selection;
	size_t _directoryContentsSelection;
};

};
