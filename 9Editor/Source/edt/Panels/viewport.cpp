#include "pch.h"
#include "viewport.hpp"

#include "core"
#include "events"
#include "events"
#include "input"

#include "edt/editor.hpp"

#include "rdr/renderer.hpp"
#include "Graphics/frame_buffer.hpp"

#include "Memory/memory.hpp"

#include "res/assets.hpp"
#include "res/scene.hpp"
#include "edt/Assets/scene.hpp"

#include "edt/editor.hpp"
#include "edt/GUI/file_explorer.hpp"

#include "ECS/ecs.hpp"

namespace {

u32 s_ViewportIndex = 0;
char * GetViewportName()
{
	char *name = new char[strlen("Viewport XXX")];
	sprintf(name, "Viewport %d", s_ViewportIndex++);
	s_ViewportIndex %= 1000;
	return name;
}

ecs::sys::machine s_machine;
bool s_machineSetup = false;
void MachineSetup()
{
	if (s_machineSetup) return;
	s_machineSetup = true;
	ecs::sys::machine::FillWithAllSystems(&s_machine);
}
};

namespace edt::panel::viewport {

math::rect window::GetWindowRect(const ImVec2& viewportPos, const ImVec2& viewportSize) const
{
	math::rect rect;
	rect.min = { viewportPos.x + (viewportSize.x * rect.xMin), viewportPos.y + (viewportSize.y * rect.yMin) };
	rect.max = { viewportPos.x + (viewportSize.x * rect.xMax), viewportPos.y + (viewportSize.y * rect.yMax) };
	return rect;
}

PnlViewport::PnlViewport(rdr::_RenderAPI *api)
	: _Panel(GetViewportName()), _renderer(api), _scene{nullptr}, _windows{},
	  _arTools(mem::heap_area(sizeof(ecs::World) * 4, sizeof(ecs::World))),
	  _frameBuffers{nullptr},
	  _watcher("Assets", 30), _activeEntries(arr::HeapBitset(30)), _selection(0)
{
	_fWindow = ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse;
	MachineSetup();

	_frameBuffers = editor::CurrentAPI->CreateFrameBufferLibrary(4);
	editor::e_OpenScene.AddDelegate(clbk::delegate<void (const edt::asset::scene&)>::Create<PnlViewport, &PnlViewport::OpenScene>(this));
}

PnlViewport::PnlViewport(rdr::_RenderAPI *api, edt::asset::scene *scene)
	: _Panel(GetViewportName()), _renderer(api), _scene{scene}, _windows{},
	  _arTools(mem::heap_area(sizeof(ecs::World) * 4, sizeof(ecs::World))),
	  _frameBuffers{nullptr}
{
	_fWindow = ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse;
	MachineSetup();

	_frameBuffers = editor::CurrentAPI->CreateFrameBufferLibrary(4);
}

PnlViewport::~PnlViewport()
{
	delete _frameBuffers;
	delete[] _name;
}

void PnlViewport::Update(f32 deltaTime)
{
	if (!HasWorld()) return;

	_renderer.ResetStats();

	// TODO Change this so that the render targets are gotten from the windows

	FOR (window, _windows)
	{
		window->screen->Bind();
		s_machine.world = _scene->world;

		arr::sttc_list<rdr::twod::sprite_data, 32> sceneSprites;
		arr::sttc_list<rdr::twod::render_target, 32> targets;

		s_machine.Draw(targets.get(), sceneSprites.get(), _scene->defaults);

		s_machine.world = window->tools;
		s_machine.Update(deltaTime);

		targets.clear();
		arr::sttc_list<rdr::twod::sprite_data, 32> sprites2;
		s_machine.Draw(targets.get(), sprites2.get(), _scene->defaults);
		_renderer.RenderScene(_scene->scene, targets.get(), sceneSprites.get());

		window->screen->UnBind();
	}
}

void PnlViewport::PreGUI()
{
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
}

void PnlViewport::PostGUI()
{
	ImGui::PopStyleVar();
}

void PnlViewport::GUI()
{
	if (HasWorld())
	{
		if (_resized)
		{
			FOR (window, _windows)
			{
				math::uvec2 size{
					(u32)(_size.x * (window->region.xMax - window->region.xMin)),
					(u32)(_size.y * (window->region.yMax - window->region.yMin))
				};
				window->screen->Resize(size.x, size.y);
				evt::event e = evt::NewWindowResized(glm::uvec2(size.x, size.y));
				s_machine.world = window->tools;
				s_machine.WindowResized(e);
			}
		}

		FOR (window, _windows)
		{
			ImVec2 size(
				_size.x * (window->region.xMax - window->region.xMin),
				_size.y * (window->region.yMax - window->region.yMin)
			);

			// Top Left is 0, 1 | Top Right is 1, 0
			ImGui::Image(
				RCAST(void*)(window->screen->GetColourAttachment()), size,
				ImVec2(window->region.xMin, window->region.yMin),
				ImVec2(window->region.xMax, window->region.yMax)
			);
		}
	}
	else
	{
		bool wider = _size.x > _size.y;
		ImVec2 ratRecents(1, 1);
		if (wider)
		{
			ratRecents.x = 0.35f;
		}
		else
		{
			ratRecents.y = 0.35f;
		}

		if (ImGui::BeginChild("Recents", ImVec2(_size.x * ratRecents.x, _size.y * ratRecents.y), true))
		{
			ImGui::TextWrapped("This is where the recently opened but not currently opened scenes will go");
			// if (ImGui::Button("Open Test"))
			// {
			// 	_world = edt::asset::LoadScene(AssetPath(test.scn));
			// 	CreateWindow(math::rect());
			// }
		} ImGui::EndChild();

		ratRecents.x = 1.0f - ratRecents.x;
		ratRecents.y = 1.0f - ratRecents.y;
		if (wider) ImGui::SameLine();

		if (ImGui::BeginChild("Filesystem", ImVec2(_size.x * ratRecents.x, _size.y * ratRecents.y), true))
		{
			// ImGui::TextWrapped("This is where a file system showing only scenes and prefabs will go");
			edt::asset::directory_tree tree = _watcher.tree();
			edt::gui::explorer_selection selection = edt::gui::DrawFileExplorer(tree, _selection, _activeEntries.get(),  edt::gui::explorer_filter{ .extension = ".scn" });
			if (selection.index != tree.count())
			{
				_selection = selection.index;

				if (selection.byDoubleSelection && !tree[selection.index].isDirectory)
				{
					fs::path fullpath = _watcher.GetFullPath(selection.index);
					const edt::asset::scene& scene = *edt::asset::LoadScene(fullpath);
					if (scene.world)
						clbk::Invoke<void, const edt::asset::scene&>(editor::e_OpenScene, scene);
				}
			}
		} ImGui::EndChild();
	}
}

void PnlViewport::OnEvent(evt::event& e)
{
	if (e.IsInCategory(evt::CAT_Input))
	{
		if (block())
		{
			ImGuiIO& io = ImGui::GetIO();
			e.handled |= io.WantCaptureMouse || io.WantCaptureKeyboard;
		}
		else
			return;
	}

	switch (e.type)
	{
	case evt::Type::MouseScrolled:
		FOR(window, _windows)
		{
			if (!IsWindowHovered(*window))
				continue;

			s_machine.world = window->tools;
			s_machine.MouseScrolled(e);
		}
		break;
	default:
		break;
	}
}

void PnlViewport::OnFocusGained()
{
	editor::Renderer = &_renderer;
}
void PnlViewport::OnFocusLost()
{
	if (editor::Renderer == &_renderer)
		editor::Renderer = nullptr;
}

void PnlViewport::OpenScene(const edt::asset::scene& scene)
{
	if (_scene && scene.world == _scene->world)
		return;

	// TODO After creating TEECS / new object model
	_scene = CCAST(edt::asset::scene*)(&scene);

	CreateWindow(math::rect());
	editor::e_OpenScene.RemoveDelegate(clbk::delegate<void (const edt::asset::scene&)>::Create<PnlViewport, &PnlViewport::OpenScene>(this));
}

void PnlViewport::CreateWindow(const math::rect& rect)
{
	char name[16];
	sprintf(name, "Window " SSIZET_FMT, _windows.count() + 1);
	gfx::frame_buffer fb = { .width = SCAST(u32)(_size.x), .height = SCAST(u32)(_size.y) };
	window win{
		rect,
		NEW((&_arTools), ecs::World, 0)(8, 8, name),
		_frameBuffers->CreateImmediate(fb)
	};

	::res::scene_context ctx{
		.roots = ::res::GetRoots(),
		.texture = _scene->scene.textures
	};
	::res::LoadSceneIntoWorld(EditorAssetPath(Viewport/camera.prfb), *win.tools, ctx);

	_windows.push(win);
}

bool PnlViewport::IsWindowHovered(const window& w)
{
	ImVec2 mousePos = ImGui::GetMousePos();
	math::rect windowRect = w.GetWindowRect(_position, _size);

	return mousePos.x >= windowRect.xMin && mousePos.x <= windowRect.xMax
		&& mousePos.y >= windowRect.yMin && mousePos.y <= windowRect.yMax;
}
};
