#include "pch.h"
#include "ECS/ecs.hpp"
#include "ECS/camera.sys"
#include "ECS/sprite.sys"

namespace ecs::sys {

system_shared *g_data;

void Init()
{
	g_data = new system_shared;
	g_data->registry = arr::dync_list<System>(8);

	g_data->registry.push(&ortho_cam_target::data);
	g_data->registry.push(&ortho_camera_controller::data);
	g_data->registry.push(&sprite::data);

	g_data->mchMain = machine{};
}
};
